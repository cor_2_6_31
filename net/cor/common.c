/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/mutex.h>

#include "cor.h"

DEFINE_SPINLOCK(cor_bindnodes);
DEFINE_SPINLOCK(conn_free);
DEFINE_SPINLOCK(connid_gen);

LIST_HEAD(openports);


struct cell_hdr{
	spinlock_t lock;
};


struct kmem_cache *conn_slab;
struct kmem_cache *connid_reuse_slab;


struct htable connid_table;
struct htable reverse_connid_table;
struct htable connid_reuse_table;

atomic_t num_conns;


/* see cor.h/KP_ACK_CONN */
static const __u32 log_64_11_table[] = {0,
		64, 68, 73, 77, 82, 88, 93, 99, 106, 113, 120,
		128, 136, 145, 155, 165, 175, 187, 199, 212, 226, 240,
		256, 273, 290, 309, 329, 351, 374, 398, 424, 451, 481,
		512, 545, 581, 619, 659, 702, 747, 796, 848, 903, 961,
		1024, 1091, 1162, 1237, 1318, 1403, 1495, 1592, 1695, 1805,
				1923,
		2048, 2181, 2323, 2474, 2635, 2806, 2989, 3183, 3390, 3611,
				3846,
		4096, 4362, 4646, 4948, 5270, 5613, 5978, 6367, 6781, 7222,
				7692,
		8192, 8725, 9292, 9897, 10540, 11226, 11956, 12734, 13562,
				14444, 15383,
		16384, 17450, 18585, 19793, 21081, 22452, 23912, 25467, 27124,
				28888, 30767,
		32768, 34899, 37169, 39587, 42161, 44904, 47824, 50935, 54248,
				57776, 61534,
		65536, 69799, 74338, 79173, 84323, 89807, 95648, 101870, 108495,
				115552, 123068,
		131072, 139597, 148677, 158347, 168646, 179615, 191297, 203739,
				216991, 231104, 246135,
		262144, 279194, 297353, 316693, 337291, 359229, 382594, 407478,
				433981, 462208, 492270,
		524288, 558388, 594706, 633387, 674583, 718459, 765188, 814957,
				867962, 924415, 984540,
		1048576, 1116777, 1189413, 1266774, 1349166, 1436917, 1530376,
				1629913, 1735924, 1848831, 1969081,
		2097152, 2233553, 2378826, 2533547, 2698332, 2873834, 3060752,
				3259826, 3471849, 3697662, 3938162,
		4194304, 4467106, 4757652, 5067094, 5396664, 5747669, 6121503,
				6519652, 6943698, 7395323, 7876323,
		8388608, 8934212, 9515303, 10134189, 10793327, 11495337,
				12243006, 13039305, 13887396, 14790647,
				15752647,
		16777216, 17868424, 19030606, 20268378, 21586655, 22990674,
				24486013, 26078610, 27774791, 29581294,
				31505293,
		33554432, 35736849, 38061212, 40536755, 43173310, 45981349,
				48972026, 52157220, 55549582, 59162588,
				63010587,
		67108864, 71473698, 76122425, 81073510, 86346620, 91962698,
				97944052, 104314440, 111099165, 118325175,
				126021174,
		134217728, 142947395, 152244850, 162147020, 172693239,
				183925396, 195888104, 208628880, 222198329,
				236650351, 252042347,
		268435456, 285894791, 304489699, 324294041, 345386479,
				367850791, 391776208, 417257759, 444396658,
				473300701, 504084694,
		536870912, 571789581};

static const __u64 log_300_24_table[] = {0LL,
		300LL, 308LL, 317LL, 327LL, 336LL, 346LL, 356LL, 367LL, 377LL,
			389LL, 400LL, 412LL, 424LL, 436LL, 449LL, 462LL, 476LL,
			490LL, 504LL, 519LL, 534LL, 550LL, 566LL, 582LL,
		600LL, 617LL, 635LL, 654LL, 673LL, 693LL, 713LL, 734LL, 755LL,
			778LL, 800LL, 824LL, 848LL, 873LL, 898LL, 925LL, 952LL,
			980LL, 1009LL, 1038LL, 1069LL, 1100LL, 1132LL, 1165LL,
		1200LL, 1235LL, 1271LL, 1308LL, 1346LL, 1386LL, 1427LL, 1468LL,
			1511LL, 1556LL, 1601LL, 1648LL, 1697LL, 1746LL, 1797LL,
			1850LL, 1904LL, 1960LL, 2018LL, 2077LL, 2138LL, 2200LL,
			2265LL, 2331LL,
		2400LL, 2470LL, 2542LL, 2617LL, 2693LL, 2772LL, 2854LL, 2937LL,
			3023LL, 3112LL, 3203LL, 3297LL, 3394LL, 3493LL, 3595LL,
			3701LL, 3809LL, 3921LL, 4036LL, 4154LL, 4276LL, 4401LL,
			4530LL, 4663LL,
		4800LL, 4940LL, 5085LL, 5234LL, 5387LL, 5545LL, 5708LL, 5875LL,
			6047LL, 6224LL, 6407LL, 6594LL, 6788LL, 6987LL, 7191LL,
			7402LL, 7619LL, 7842LL, 8072LL, 8309LL, 8552LL, 8803LL,
			9061LL, 9326LL,
		9600LL, 9881LL, 10170LL, 10468LL, 10775LL, 11091LL, 11416LL,
			11750LL, 12095LL, 12449LL, 12814LL, 13189LL, 13576LL,
			13974LL, 14383LL, 14805LL, 15239LL, 15685LL, 16145LL,
			16618LL, 17105LL, 17606LL, 18122LL, 18653LL,
		19200LL, 19762LL, 20341LL, 20937LL, 21551LL, 22182LL, 22832LL,
			23501LL, 24190LL, 24899LL, 25628LL, 26379LL, 27152LL,
			27948LL, 28767LL, 29610LL, 30478LL, 31371LL, 32290LL,
			33236LL, 34210LL, 35212LL, 36244LL, 37306LL,
		38400LL, 39525LL, 40683LL, 41875LL, 43102LL, 44365LL, 45665LL,
			47003LL, 48380LL, 49798LL, 51257LL, 52759LL, 54305LL,
			55897LL, 57534LL, 59220LL, 60956LL, 62742LL, 64580LL,
			66473LL, 68421LL, 70425LL, 72489LL, 74613LL,
		76800LL, 79050LL, 81366LL, 83750LL, 86205LL, 88731LL, 91331LL,
			94007LL, 96761LL, 99597LL, 102515LL, 105519LL, 108611LL,
			111794LL, 115069LL, 118441LL, 121912LL, 125484LL,
			129161LL, 132946LL, 136842LL, 140851LL, 144979LL,
			149227LL,
		153600LL, 158100LL, 162733LL, 167501LL, 172410LL, 177462LL,
			182662LL, 188014LL, 193523LL, 199194LL, 205031LL,
			211039LL, 217223LL, 223588LL, 230139LL, 236883LL,
			243824LL, 250969LL, 258323LL, 265892LL, 273684LL,
			281703LL, 289958LL, 298454LL,
		307200LL, 316201LL, 325467LL, 335003LL, 344820LL, 354924LL,
			365324LL, 376029LL, 387047LL, 398389LL, 410062LL,
			422078LL, 434446LL, 447176LL, 460279LL, 473767LL,
			487649LL, 501938LL, 516646LL, 531785LL, 547368LL,
			563407LL, 579916LL, 596909LL,
		614400LL, 632403LL, 650934LL, 670007LL, 689640LL, 709848LL,
			730648LL, 752058LL, 774095LL, 796778LL, 820125LL,
			844157LL, 868892LL, 894353LL, 920559LL, 947534LL,
			975299LL, 1003877LL, 1033293LL, 1063571LL, 1094736LL,
			1126814LL, 1159832LL, 1193818LL,
		1228800LL, 1264806LL, 1301868LL, 1340015LL, 1379281LL,
			1419697LL, 1461297LL, 1504116LL, 1548190LL, 1593556LL,
			1640251LL, 1688314LL, 1737785LL, 1788706LL, 1841119LL,
			1895068LL, 1950598LL, 2007755LL, 2066587LL, 2127142LL,
			2189472LL, 2253629LL, 2319665LL, 2387636LL,
		2457600LL, 2529613LL, 2603736LL, 2680031LL, 2758562LL,
			2839394LL, 2922595LL, 3008233LL, 3096381LL, 3187112LL,
			3280502LL, 3376628LL, 3475571LL, 3577413LL, 3682239LL,
			3790137LL, 3901196LL, 4015510LL, 4133174LL, 4254285LL,
			4378945LL, 4507258LL, 4639331LL, 4775273LL,
		4915200LL, 5059226LL, 5207473LL, 5360063LL, 5517125LL,
			5678789LL, 5845190LL, 6016467LL, 6192763LL, 6374225LL,
			6561004LL, 6753256LL, 6951142LL, 7154826LL, 7364478LL,
			7580274LL, 7802393LL, 8031021LL, 8266348LL, 8508570LL,
			8757890LL, 9014516LL, 9278662LL, 9550547LL,
		9830400LL, 10118452LL, 10414946LL, 10720127LL, 11034250LL,
			11357579LL, 11690381LL, 12032935LL, 12385527LL,
			12748451LL, 13122009LL, 13506513LL, 13902285LL,
			14309653LL, 14728957LL, 15160549LL, 15604787LL,
			16062042LL, 16532696LL, 17017141LL, 17515781LL,
			18029033LL, 18557324LL, 19101095LL,
		19660800LL, 20236905LL, 20829892LL, 21440254LL, 22068501LL,
			22715158LL, 23380763LL, 24065871LL, 24771055LL,
			25496903LL, 26244019LL, 27013027LL, 27804570LL,
			28619306LL, 29457915LL, 30321098LL, 31209574LL,
			32124084LL, 33065392LL, 34034282LL, 35031563LL,
			36058066LL, 37114648LL, 38202190LL,
		39321600LL, 40473810LL, 41659784LL, 42880508LL, 44137003LL,
			45430316LL, 46761526LL, 48131743LL, 49542111LL,
			50993806LL, 52488038LL, 54026055LL, 55609140LL,
			57238612LL, 58915831LL, 60642197LL, 62419149LL,
			64248169LL, 66130784LL, 68068564LL, 70063126LL,
			72116132LL, 74229296LL, 76404380LL,
		78643200LL, 80947621LL, 83319568LL, 85761017LL, 88274007LL,
			90860633LL, 93523052LL, 96263487LL, 99084223LL,
			101987612LL, 104976077LL, 108052111LL, 111218280LL,
			114477224LL, 117831663LL, 121284394LL, 124838298LL,
			128496339LL, 132261569LL, 136137129LL, 140126252LL,
			144232264LL, 148458592LL, 152808761LL,
		157286400LL, 161895243LL, 166639136LL, 171522035LL, 176548014LL,
			181721266LL, 187046105LL, 192526975LL, 198168446LL,
			203975224LL, 209952155LL, 216104222LL, 222436560LL,
			228954448LL, 235663326LL, 242568788LL, 249676596LL,
			256992679LL, 264523139LL, 272274259LL, 280252504LL,
			288464529LL, 296917185LL, 305617523LL,
		314572800LL, 323790486LL, 333278272LL, 343044070LL, 353096029LL,
			363442532LL, 374092211LL, 385053950LL, 396336892LL,
			407950449LL, 419904310LL, 432208445LL, 444873120LL,
			457908897LL, 471326652LL, 485137577LL, 499353193LL,
			513985359LL, 529046279LL, 544548518LL, 560505008LL,
			576929058LL, 593834370LL, 611235046LL,
		629145600LL, 647580973LL, 666556544LL, 686088141LL,
			706192058LL, 726885065LL, 748184423LL, 770107900LL,
			792673784LL, 815900899LL, 839808620LL, 864416891LL,
			889746240LL, 915817795LL, 942653304LL, 970275155LL,
			998706387LL, 1027970718LL, 1058092559LL, 1089097037LL,
			1121010017LL, 1153858117LL, 1187668741LL, 1222470092LL,
		1258291200LL, 1295161946LL, 1333113088LL, 1372176283LL,
			1412384117LL, 1453770131LL, 1496368847LL, 1540215801LL,
			1585347569LL, 1631801799LL, 1679617241LL, 1728833783LL,
			1779492480LL, 1831635590LL, 1885306609LL, 1940550310LL,
			1997412774LL, 2055941436LL, 2116185118LL, 2178194075LL,
			2242020034LL, 2307716235LL, 2375337483LL, 2444940184LL,
		2516582400LL, 2590323893LL, 2666226176LL, 2744352567LL,
			2824768235LL, 2907540262LL, 2992737695LL, 3080431603LL,
			3170695139LL, 3263603598LL, 3359234483LL, 3457667567LL,
			3558984960LL, 3663271180LL, 3770613219LL, 3881100620LL,
			3994825549LL, 4111882872LL, 4232370237LL, 4356388151LL,
			4484040068LL, 4615432471LL, 4750674966LL, 4889880368LL,
		5033164800LL, 5180647786LL, 5332452353LL, 5488705134LL,
			5649536470LL, 5815080525LL, 5985475391LL, 6160863207LL,
			6341390279LL, 6527207197LL, 6718468967LL, 6915335135LL,
			7117969921LL, 7326542360LL, 7541226438LL, 7762201240LL,
			7989651098LL, 8223765745LL, 8464740475LL, 8712776303LL,
			8968080137LL, 9230864943LL, 9501349932LL, 9779760736LL,
		10066329600LL, 10361295572LL, 10664904706LL, 10977410268LL,
			11299072941LL, 11630161050LL, 11970950782LL,
			12321726414LL, 12682780558LL, 13054414395LL,
			13436937935LL, 13830670270LL, 14235939843LL,
			14653084721LL, 15082452877LL, 15524402481LL,
			15979302196LL, 16447531490LL, 16929480950LL,
			17425552607LL, 17936160274LL, 18461729886LL,
			19002699864LL, 19559521473LL,
		20132659200LL, 20722591144LL, 21329809413LL, 21954820536LL,
			22598145883LL, 23260322101LL, 23941901564LL,
			24643452829LL, 25365561116LL, 26108828790LL,
			26873875870LL, 27661340540LL, 28471879687LL,
			29306169443LL, 30164905755LL, 31048804962LL,
			31958604392LL, 32895062981LL, 33858961901LL,
			34851105215LL, 35872320548LL, 36923459773LL,
			38005399729LL, 39119042946LL,
		40265318400LL, 41445182288LL,42659618827LL,43909641073LL,
			45196291767LL, 46520644203LL, 47883803129LL,
			49286905659LL, 50731122232LL, 52217657581LL,
			53747751741LL, 55322681081LL, 56943759374LL,
			58612338887LL, 60329811511LL, 62097609924LL,
			63917208785LL, 65790125963LL, 67717923803LL,
			69702210431LL, 71744641096LL, 73846919547LL,
			76010799459LL, 78238085892LL,
		80530636800LL, 82890364576LL, 85319237654LL, 87819282146LL,
			90392583534LL, 93041288407LL, 95767606258LL,
			98573811319LL, 101462244465LL, 104435315163LL,
			107495503482LL, 110645362163LL, 113887518749LL,
			117224677774LL, 120659623022LL, 124195219849LL,
			127834417571LL, 131580251926LL, 135435847606LL,
			139404420863LL, 143489282192LL, 147693839094LL,
			152021598918LL, 156476171785LL,
		161061273600LL, 165780729153LL, 170638475309LL, 175638564293LL,
			180785167068LL, 186082576815LL, 191535212516LL,
			197147622639LL, 202924488931LL, 208870630326LL,
			214991006964LL, 221290724326LL, 227775037498LL,
			234449355548LL, 241319246045LL, 248390439699LL,
			255668835143LL, 263160503853LL, 270871695212LL,
			278808841726LL, 286978564384LL, 295387678189LL,
			304043197837LL, 312952343570LL,
		322122547200LL, 331561458306LL, 341276950619LL, 351277128587LL,
			361570334136LL, 372165153630LL, 383070425033LL,
			394295245278LL, 405848977863LL, 417741260653LL,
			429982013929LL, 442581448653LL, 455550074996LL,
			468898711096LL, 482638492091LL, 496780879399LL,
			511337670287LL, 526321007707LL, 541743390425LL,
			557617683452LL, 573957128769LL, 590775356379LL,
			608086395675LL, 625904687141LL,
		644245094400LL, 663122916612LL, 682553901238LL, 702554257174LL,
			723140668273LL, 744330307261LL, 766140850066LL,
			788590490556LL, 811697955726LL, 835482521307LL,
			859964027858LL, 885162897307LL, 911100149992LL,
			937797422193LL, 965276984182LL, 993561758799LL,
			1022675340575LL, 1052642015414LL, 1083486780851LL,
			1115235366904LL, 1147914257538LL, 1181550712759LL,
			1216172791350LL, 1251809374282LL,
		1288490188800LL, 1326245833225LL, 1365107802477LL,
			1405108514349LL, 1446281336546LL, 1488660614523LL,
			1532281700132LL, 1577180981113LL, 1623395911452LL,
			1670965042615LL, 1719928055717LL, 1770325794615LL,
			1822200299985LL, 1875594844387LL, 1930553968365LL,
			1987123517599LL, 2045350681151LL, 2105284030829LL,
			2166973561703LL, 2230470733808LL, 2295828515076LL,
			2363101425518LL, 2432345582701LL, 2503618748564LL,
		2576980377600LL, 2652491666450LL, 2730215604955LL,
			2810217028699LL, 2892562673093LL, 2977321229047LL,
			3064563400264LL, 3154361962227LL, 3246791822904LL,
			3341930085231LL, 3439856111434LL, 3540651589231LL,
			3644400599971LL, 3751189688775LL, 3861107936730LL,
			3974247035198LL, 4090701362303LL, 4210568061659LL,
			4333947123406LL, 4460941467616LL, 4591657030153LL,
			4726202851036LL, 4864691165402LL, 5007237497128LL,
		5153960755200LL, 5304983332900LL, 5460431209910LL,
			5620434057398LL, 5785125346187LL, 5954642458094LL,
			6129126800528LL, 6308723924455LL, 6493583645808LL,
			6683860170462LL, 6879712222869LL, 7081303178462LL,
			7288801199942LL, 7502379377550LL, 7722215873460LL,
			7948494070397LL, 8181402724607LL, 8421136123319LL,
			8667894246812LL, 8921882935232LL, 9183314060306LL,
			9452405702073LL, 9729382330805LL, 10014474994257LL,
		10307921510400LL, 10609966665800LL, 10920862419821LL,
			11240868114797LL, 11570250692375LL, 11909284916188LL,
			12258253601057LL, 12617447848911LL, 12987167291617LL,
			13367720340924LL, 13759424445738LL, 14162606356925LL,
			14577602399885LL, 15004758755101LL, 15444431746921LL,
			15896988140794LL, 16362805449214LL, 16842272246638LL,
			17335788493624LL, 17843765870465LL, 18366628120613LL,
			18904811404146LL, 19458764661611LL, 20028949988515LL,
		20615843020800LL, 21219933331600LL, 21841724839642LL,
			22481736229595LL, 23140501384751LL, 23818569832376LL,
			24516507202114LL, 25234895697822LL, 25974334583234LL,
			26735440681849LL, 27518848891476LL, 28325212713851LL,
			29155204799770LL, 30009517510202LL, 30888863493843LL,
			31793976281588LL, 32725610898429LL, 33684544493277LL,
			34671576987248LL, 35687531740931LL, 36733256241226LL,
			37809622808292LL, 38917529323222LL, 40057899977031LL,
		41231686041600LL, 42439866663201LL, 43683449679284LL,
			44963472459190LL, 46281002769503LL, 47637139664753LL,
			49033014404229LL, 50469791395645LL, 51948669166468LL,
			53470881363698LL, 55037697782953LL, 56650425427703LL,
			58310409599540LL, 60019035020404LL, 61777726987686LL,
			63587952563177LL, 65451221796858LL, 67369088986555LL,
			69343153974496LL, 71375063481862LL, 73466512482452LL,
			75619245616585LL, 77835058646445LL, 80115799954063LL,
		82463372083200LL, 84879733326402LL, 87366899358568LL,
			89926944918381LL, 92562005539006LL, 95274279329506LL,
			98066028808458LL, 100939582791291LL, 103897338332936LL,
			106941762727397LL, 110075395565907LL, 113300850855406LL,
			116620819199080LL, 120038070040809LL, 123555453975373LL,
			127175905126354LL, 130902443593716LL, 134738177973111LL,
			138686307948992LL, 142750126963725LL, 146933024964905LL,
			151238491233171LL, 155670117292890LL, 160231599908126LL,
		164926744166400LL, 169759466652804LL, 174733798717137LL,
			179853889836763LL, 185124011078013LL, 190548558659013LL,
			196132057616916LL, 201879165582582LL, 207794676665873LL,
			213883525454795LL, 220150791131815LL, 226601701710812LL,
			233241638398160LL, 240076140081619LL, 247110907950747LL,
			254351810252708LL, 261804887187433LL, 269476355946222LL,
			277372615897984LL, 285500253927451LL, 293866049929810LL,
			302476982466342LL, 311340234585780LL, 320463199816253LL,
		329853488332800LL, 339518933305609LL, 349467597434275LL,
			359707779673527LL, 370248022156026LL, 381097117318027LL,
			392264115233832LL, 403758331165164LL, 415589353331747LL,
			427767050909590LL, 440301582263631LL, 453203403421624LL,
			466483276796321LL, 480152280163238LL, 494221815901494LL,
			508703620505416LL, 523609774374866LL, 538952711892444LL,
			554745231795968LL, 571000507854903LL, 587732099859621LL,
			604953964932684LL, 622680469171561LL, 640926399632506LL,
		659706976665600LL, 679037866611218LL, 698935194868551LL,
			719415559347055LL, 740496044312053LL, 762194234636054LL,
			784528230467665LL, 807516662330329LL, 831178706663495LL,
			855534101819180LL, 880603164527263LL, 906406806843249LL,
			932966553592642LL, 960304560326477LL, 988443631802988LL,
			1017407241010832LL, 1047219548749733LL,
			1077905423784889LL, 1109490463591937LL,
			1142001015709806LL, 1175464199719243LL,
			1209907929865369LL, 1245360938343122LL,
			1281852799265013LL,
		1319413953331200LL, 1358075733222436LL, 1397870389737103LL,
			1438831118694110LL, 1480992088624106LL,
			1524388469272109LL, 1569056460935331LL,
			1615033324660658LL, 1662357413326990LL,
			1711068203638361LL, 1761206329054527LL,
			1812813613686499LL, 1865933107185284LL,
			1920609120652954LL, 1976887263605976LL};


__u8 __attribute__((const)) enc_log_64_11(__u32 value)
{
	int i;
	BUG_ON(log_64_11_table[255] != 571789581);
	for (i=1;i<256;i++) {
		if (log_64_11_table[i] > value)
			break;
	}

	return (__u8)(i-1); /* round down */
}

__u32 __attribute__((const)) dec_log_64_11(__u8 value)
{
	BUG_ON(log_64_11_table[255] != 571789581);
	return log_64_11_table[value];
}

static void check_log_64_11_table(void)
{
	int i;
	BUG_ON(log_64_11_table[0] != 0);
	for (i=1;i<256;i++) {
		BUG_ON(log_64_11_table[i] <= log_64_11_table[i-1]);
	}
}

__u16 __attribute__((const)) enc_log_300_24(__u64 value)
{
	int i;
	BUG_ON(log_300_24_table[1023] != 1976887263605976LL);
	for (i=1;i<1024;i++) {
		if (log_300_24_table[i] > value)
			break;
	}

	return (__u16)(i-1); /* round down */
}

__u64 __attribute__((const)) dec_log_300_24(__u16 value)
{
	BUG_ON(value >= 1024);
	BUG_ON(log_300_24_table[1023] != 1976887263605976LL);
	return log_300_24_table[value];
}

static void check_log_300_24_table(void)
{
	int i;
	BUG_ON(log_300_24_table[0] != 0);
	for (i=1;i<1024;i++) {
		BUG_ON(log_300_24_table[i] <= log_300_24_table[i-1]);
	}
}

static inline __u64 mul_saturated(__u64 a, __u64 b)
{
	__u64 res = a*b;
	if (unlikely(res / a != b))
		return -1;
	return res;
}

static inline int numdigits(__u64 value)
{
	int digits = 0;
	for (;value != 0;value = (value >> 1)) {
		digits++;
	}
	return digits;
}

static void mul(__u64 a, __u64 b, __u64 *reshigh, __u64 *reslow)
{
	__u32 al = a;
	__u32 ah = (a >> 32);
	__u32 bl = b;
	__u32 bh = (b >> 32);

	__u64 r1 = ((__u64) al) * bl;
	__u64 r20 = (r1 >> 32);
	__u64 r21 = ((__u64) ah) * bl;
	__u64 r22 = ((__u64) al) * bh;
	__u64 r2 = r20 + ((r21 << 32) >> 32) + ((r22 << 32) >> 32);
	__u64 r30 = (r21 >> 32) + (r22 >> 32) + (r2 >> 32);
	__u64 r31 = ((__u64) ah) * bh;

	BUG_ON(reshigh == 0);
	BUG_ON(reslow == 0);

	(*reslow) = ((r1 << 32) >> 32) + (r2 << 32);
	(*reshigh) = r30 + r31;
}

/* calculate (a*b+rem)/c */
__u64 multiply_div2(__u64 a, __u64 b, __s64 rem, __u64 c, __u64 *remainder)
{
	__u64 res = 0;
	__u64 reshigh = 0;
	__u64 reslow = 0;
	int u;

	__u64 high = 0;
	__u64 low = 0;

	mul(a, b, &high, &low);

	if (rem < 0) {
		__u64 rem0 = (rem == S64_MIN) ? (((__u64) S64_MAX) + 1) :
				(0 - rem);
		if (low - rem0 > low)
			high--;
		low -= rem0;
	} else {
		if (low + ((__u64) rem) < low)
			high++;
		low += ((__u64) rem);
	}


	for(u=63;u>=0;u--) {
		__u64 tmphigh;
		__u64 tmplow;

		__u64 tmpres = res + (1LL << u);

		mul(c, tmpres, &tmphigh, &tmplow);

		if (tmphigh > high)
			continue;
		if (tmphigh < high)
			goto apply;

		if (tmplow > low)
			continue;
		if (tmplow < low)
			goto apply;

		if (remainder != 0)
			(*remainder) = 0;
		return tmpres;
apply:
		res = tmpres;
		reshigh = tmphigh;
		reslow = tmplow;
	}

	if (remainder != 0) {
		(*remainder) = low - reslow;
		if ((*remainder) > c)
			(*remainder) = 0;

	}

	return res;
}

static inline int hdr_size(void)
{
	return ((sizeof(struct cell_hdr) + sizeof(void *) - 1) / sizeof(void *)
		) * sizeof(void *);
}

static inline int elements_per_cell(int cell_size)
{
	return (cell_size - hdr_size())/sizeof(void *);
}

static inline struct cell_hdr *cell_addr(struct htable *ht, __u32 id)
{
	int idx = (id%ht->htable_size) / (elements_per_cell(ht->cell_size));
	return (struct cell_hdr *) (((char *)ht->htable) + ht->cell_size * idx);
}

static inline char **element_addr(struct htable *ht, __u32 id)
{
	int idx = (id%ht->htable_size) % (elements_per_cell(ht->cell_size));
	return (char **)
			( ((char *)cell_addr(ht, id)) +
			hdr_size() + idx*sizeof(void *));
}


static inline char **next_element(struct htable *ht, char *element)
{
	return (char **)(element + ht->entry_offset);
}

static inline struct kref *element_kref(struct htable *ht, char *element)
{
	return (struct kref *)(element + ht->kref_offset);
}


static inline void unlock_element(struct htable *ht, __u32 key)
{
	struct cell_hdr *hdr = cell_addr(ht, key);
	spin_unlock(&(hdr->lock));
}


static char **get_element_nounlock(struct htable *ht, __u32 key,
		void *searcheditem)
{
	struct cell_hdr *hdr = cell_addr(ht, key);
	char **element = element_addr(ht, key);

	BUG_ON(0 == element);

	spin_lock(&(hdr->lock));

	while (1) {
		if (*element == 0)
			break;
		if (searcheditem != 0 && ht->matches(*element, searcheditem))
			break;
		element = next_element(ht, *element);
	}

	return element;
}

char *htable_get(struct htable *ht, __u32 key, void *searcheditem)
{
	unsigned long iflags;
	char *element;

	if (unlikely(ht->htable == 0))
		return 0;

	local_irq_save(iflags);
	element = *(get_element_nounlock(ht, key, searcheditem));
	if (likely(element != 0))
		kref_get(element_kref(ht, element));
	unlock_element(ht, key);
	local_irq_restore(iflags);

	return element;
}

int htable_delete(struct htable *ht, __u32 key,
		void *searcheditem, void (*free) (struct kref *ref))
{
	unsigned long iflags;
	char **element;
	char **next;
	int rc = 0;

	if (unlikely(ht->htable == 0))
		return 1;

	local_irq_save(iflags);

	element = get_element_nounlock(ht, key, searcheditem);
	BUG_ON(0 == element);

	if (unlikely(*element == 0)) {
		/* key not in table */
		rc = 1;
		goto out;
	}

	next = next_element(ht, *element);
	kref_put(element_kref(ht, *element), free);
	*element = *next;

out:
	unlock_element(ht, key);
	local_irq_restore(iflags);

	return rc;
}

void htable_insert(struct htable *ht, char *newelement, __u32 key)
{
	unsigned long iflags;
	char **element;

	if (unlikely(ht->htable == 0))
		return;

	BUG_ON(*next_element(ht, newelement) != 0);
	local_irq_save(iflags);

	element = get_element_nounlock(ht, key, 0);

	BUG_ON(element == 0);
	BUG_ON(*element != 0);

	*element = newelement;
	kref_get(element_kref(ht, newelement));

	unlock_element(ht, key);
	local_irq_restore(iflags);
}


void htable_init(struct htable *ht, int (*matches)(void *htentry,
		void *searcheditem), __u32 entry_offset, __u32 kref_offset)
{
	int num_cells;
	int j;

	BUG_ON(0 == ht);

	ht->htable = kmalloc(PAGE_SIZE, GFP_KERNEL);
	if (unlikely(ht->htable == 0)) {
		printk(KERN_CRIT "cor: error allocating htable (out of "
				"memory?)");
		return;
	}
	memset(ht->htable, 0, PAGE_SIZE);
	ht->cell_size = 256;

	num_cells = PAGE_SIZE/ht->cell_size;

	for (j=0;j<num_cells;j++) {
		struct cell_hdr *hdr = (struct cell_hdr *)
				( ((char *) ht->htable) + j * ht->cell_size);
		spin_lock_init(&(hdr->lock));
	}

	ht->htable_size = num_cells * elements_per_cell(ht->cell_size);
	ht->num_elements = 0;

	ht->matches = matches;
	ht->entry_offset = entry_offset;
	ht->kref_offset = kref_offset;
}

struct reverse_connid_matchparam {
	struct neighbor *nb;
	__u32 conn_id;
};

static __u32 rcm_to_key(struct reverse_connid_matchparam *rcm)
{
	return (((__u32) rcm->nb) ^ rcm->conn_id);
}

static int matches_reverse_connid(void *htentry, void *searcheditem)
{
	struct conn *conn = (struct conn *) htentry;
	struct reverse_connid_matchparam *rcm =
			(struct	reverse_connid_matchparam *)  searcheditem;
	BUG_ON(conn->targettype != TARGET_OUT);
	return (conn->target.out.nb == rcm->nb) &&
			(conn->target.out.conn_id == rcm->conn_id);
}

struct conn *get_conn_reverse(struct neighbor *nb, __u32 conn_id)
{
	struct reverse_connid_matchparam rcm;
	rcm.nb = nb;
	rcm.conn_id = conn_id;

	return (struct conn *) htable_get(&reverse_connid_table,
			rcm_to_key(&rcm), &rcm);
}

void insert_reverse_connid(struct conn *trgt_out_ll)
{
	struct reverse_connid_matchparam rcm;

	BUG_ON(trgt_out_ll->targettype != TARGET_OUT);

	rcm.nb = trgt_out_ll->target.out.nb;
	rcm.conn_id = trgt_out_ll->target.out.conn_id;
	htable_insert(&reverse_connid_table, (char *) trgt_out_ll,
			rcm_to_key(&rcm));
}

static int matches_connid_in(void *htentry, void *searcheditem)
{
	struct conn *conn = (struct conn *) htentry;
	__u32 conn_id = *((__u32 *) searcheditem);
	BUG_ON(conn->sourcetype != SOURCE_IN);
	return (conn->source.in.conn_id == conn_id);
}

struct conn *get_conn(__u32 conn_id)
{
	return (struct conn *) htable_get(&connid_table, conn_id, &conn_id);
}

static void free_connid_reuse(struct kref *ref)
{
	struct connid_reuse_item *cir = container_of(ref,
			struct connid_reuse_item, ref);

	kmem_cache_free(connid_reuse_slab, cir);
}

static int matches_reuse_connid(void *htentry, void *searcheditem)
{
	struct connid_reuse_item *cir = (struct connid_reuse_item *) htentry;
	__u32 conn_id = *((__u32 *) searcheditem);
	return (cir->conn_id == conn_id);
}

void connid_used_pingsuccess(struct neighbor *nb)
{
	unsigned long iflags;
	struct connid_reuse_item *cri;

	spin_lock_irqsave(&(nb->connid_reuse_lock), iflags);

	nb->connid_reuse_pingcnt++;
	while (list_empty(&(nb->connid_reuse_list)) == 0) {
		cri = container_of(nb->connid_reuse_list.next,
				struct connid_reuse_item, lh);
		if ((cri->pingcnt + CONNID_REUSE_RTTS -
				nb->connid_reuse_pingcnt) < 32768)
			break;

		if (htable_delete(&connid_reuse_table,
				cri->conn_id,
				&(cri->conn_id), free_connid_reuse)) {
			printk(KERN_ERR "error in connid_used_pingsuccess: "
					"htable_delete failed");
		}

		list_del(&(cri->lh));
		kref_put(&(cri->ref), free_connid_reuse);
	}

	spin_unlock_irqrestore(&(nb->connid_reuse_lock), iflags);
}

static int connid_used(__u32 conn_id)
{
	struct conn *cn;
	struct connid_reuse_item *cir;

	cn = get_conn(conn_id);
	if (unlikely(cn != 0)) {
		kref_put(&(cn->ref), free_conn);
		return 1;
	}

	cir = (struct connid_reuse_item *)
			htable_get(&connid_reuse_table, conn_id, &conn_id);

	if (unlikely(cir != 0)) {
		kref_put(&(cir->ref), free_connid_reuse);
		return 1;
	}

	return 0;
}

static int connid_alloc(struct conn *src_in_ll)
{
	__u32 conn_id;
	int i;

	BUG_ON(src_in_ll->sourcetype != SOURCE_IN);

	spin_lock_bh(&connid_gen);
	for(i=0;i<16;i++) {
		conn_id = 0;
		get_random_bytes((char *) &conn_id, sizeof(conn_id));

		if (unlikely(conn_id == 0))
			continue;

		if (unlikely(connid_used(conn_id)))
			continue;

		goto found;
	}
	spin_unlock_bh(&connid_gen);

	return 1;

found:
	src_in_ll->source.in.conn_id = conn_id;
	htable_insert(&connid_table, (char *) src_in_ll, conn_id);
	spin_unlock_bh(&connid_gen);
	return 0;
}

void _set_last_act(struct conn *src_in_l)
{
	unsigned long iflags;
	spin_lock_irqsave(&(src_in_l->source.in.nb->conn_list_lock), iflags);
	list_del(&(src_in_l->source.in.nb_list));
	list_add_tail(&(src_in_l->source.in.nb_list),
			&(src_in_l->source.in.nb->rcv_conn_list));
	spin_unlock_irqrestore(&(src_in_l->source.in.nb->conn_list_lock),
			iflags);
}

void free_conn(struct kref *ref)
{
	unsigned long iflags;
	struct conn *cn = container_of(ref, struct conn, ref);
	struct conn *reversedir = 0;

	spin_lock_irqsave(&conn_free, iflags);

	BUG_ON(cn->isreset == 0);

	if (cn->reversedir != 0)
		cn->reversedir->isreset = 3;

	if (cn->isreset != 3)
		goto out;

	if (cn->reversedir != 0) {
		cn->reversedir->reversedir = 0;
		reversedir = cn->reversedir;
		cn->reversedir = 0;
	}

	if (cn->sourcetype == SOURCE_IN) {
		kref_put(&(cn->source.in.nb->ref), neighbor_free);
		cn->source.in.nb = 0;
	}

	if (cn->targettype == TARGET_OUT) {
		kref_put(&(cn->target.out.nb->ref), neighbor_free);
		cn->target.out.nb = 0;
	}

	BUG_ON(cn->data_buf.totalsize != 0);
	BUG_ON(cn->data_buf.overhead != 0);

	kmem_cache_free(conn_slab, cn);

out:
	spin_unlock_irqrestore(&conn_free, iflags);

	if (reversedir != 0)
		free_conn(&(reversedir->ref));
}

/**
 * rc == 0 ==> ok
 * rc == 1 ==> connid_reuse or connid allocation failed
 */
int conn_init_out(struct conn *trgt_unconn_ll, struct neighbor *nb)
{
	unsigned long iflags;
	int rc = 0;
	struct conn *src_none_ll = trgt_unconn_ll->reversedir;

	BUG_ON(trgt_unconn_ll->targettype != TARGET_UNCONNECTED);
	BUG_ON(src_none_ll == 0);
	BUG_ON(src_none_ll->sourcetype != SOURCE_NONE);

	memset(&(trgt_unconn_ll->target.out), 0,
			sizeof(trgt_unconn_ll->target.out));
	memset(&(src_none_ll->source.in), 0, sizeof(src_none_ll->source.in));

	trgt_unconn_ll->targettype = TARGET_OUT;
	src_none_ll->sourcetype = SOURCE_IN;


	src_none_ll->source.in.cir = kmem_cache_alloc(connid_reuse_slab,
			GFP_ATOMIC);
	if (unlikely(src_none_ll->source.in.cir == 0)) {
		rc = 1;
		goto out;
	}
	memset(src_none_ll->source.in.cir, 0, sizeof(struct connid_reuse_item));


	if (unlikely(connid_alloc(src_none_ll))) {
		rc = 1;
		goto out_freecir;
	}


	trgt_unconn_ll->target.out.nb = nb;
	src_none_ll->source.in.nb = nb;

	/* neighbor pointer */
	kref_get(&(nb->ref));
	kref_get(&(nb->ref));

	skb_queue_head_init(&(src_none_ll->source.in.reorder_queue));

	INIT_LIST_HEAD(&(src_none_ll->source.in.acks_pending));

	INIT_LIST_HEAD(&(trgt_unconn_ll->target.out.retrans_list));

	reset_seqno(trgt_unconn_ll, 0);
	get_random_bytes((char *) &(src_none_ll->source.in.next_seqno),
			sizeof(src_none_ll->source.in.next_seqno));

	get_random_bytes((char *) &(trgt_unconn_ll->target.out.decaytime_seqno),
			1);
	trgt_unconn_ll->target.out.decaytime_seqno =
			trgt_unconn_ll->target.out.decaytime_seqno % 64;
	trgt_unconn_ll->source.in.decaytime_seqno = 255;

	src_none_ll->source.in.jiffies_last_act = jiffies;

	spin_lock_irqsave(&(nb->conn_list_lock), iflags);
	list_add_tail(&(src_none_ll->source.in.nb_list), &(nb->rcv_conn_list));
	spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);

	/* neighbor lists */
	kref_get(&(src_none_ll->ref));

	atomic_inc(&num_conns);


	refresh_conn_credits(trgt_unconn_ll, 0, 1);

	if (0) {
out_freecir:
		kmem_cache_free(connid_reuse_slab, src_none_ll->source.in.cir);
		src_none_ll->source.in.cir = 0;
	}
out:
	return rc;
}

void conn_init_sock_source(struct conn *cn)
{
	BUG_ON(cn == 0);
	cn->sourcetype = SOURCE_SOCK;
	memset(&(cn->source.sock), 0, sizeof(cn->source.sock));
	init_waitqueue_head(&(cn->source.sock.wait));
}

void conn_init_sock_target(struct conn *cn)
{
	BUG_ON(cn == 0);
	cn->targettype = TARGET_SOCK;
	memset(&(cn->target.sock), 0, sizeof(cn->target.sock));
	init_waitqueue_head(&(cn->target.sock.wait));
	reset_seqno(cn, 0);
}

#warning todo alloclimit (credits)
struct conn* alloc_conn(gfp_t allocflags)
{
	struct conn *cn1 = 0;
	struct conn *cn2 = 0;

	cn1 = kmem_cache_alloc(conn_slab, allocflags);
	if (unlikely(cn1 == 0))
		goto out_err0;

	cn2 = kmem_cache_alloc(conn_slab, allocflags);
	if (unlikely(cn2 == 0))
		goto out_err1;

	memset(cn1, 0, sizeof(struct conn));
	memset(cn2, 0, sizeof(struct conn));

	cn1->reversedir = cn2;
	cn2->reversedir = cn1;

	kref_init(&(cn1->ref));
	kref_init(&(cn2->ref));

	cn1->sourcetype = SOURCE_NONE;
	cn2->sourcetype = SOURCE_NONE;
	cn1->targettype = TARGET_UNCONNECTED;
	cn2->targettype = TARGET_UNCONNECTED;

	cn1->isreset = 0;
	cn2->isreset = 0;

	spin_lock_init(&(cn1->rcv_lock));
	spin_lock_init(&(cn2->rcv_lock));

	cn1->ktime_credit_update = ktime_get();
	cn2->ktime_credit_update = cn1->ktime_credit_update;

	cn1->crate_forward = ((1 << 31) - (((__u32) 1 << 31) / 10));
	cn2->crate_forward = ((1 << 31) - (((__u32) 1 << 31) / 10));

	databuf_init(cn1);
	databuf_init(cn2);

	return cn1;

out_err1:
	kmem_cache_free(conn_slab, cn1);
out_err0:
	return 0;
}

static struct cor_sock *get_corsock(__be64 port)
{
	struct list_head *curr = openports.next;

	while (curr != &openports) {
		struct cor_sock *cs = container_of(curr, struct cor_sock,
				data.listener.lh);
		BUG_ON(cs->type != SOCKTYPE_LISTENER);
		if (cs->data.listener.port == port)
			return cs;

		curr = curr->next;
	}

	return 0;
}

void close_port(struct cor_sock *cs)
{
	spin_lock_bh(&cor_bindnodes);

	list_del(&(cs->data.listener.lh));

	while (list_empty(&(cs->data.listener.conn_queue)) == 0) {
		struct conn *src_sock_o = container_of(
				cs->data.listener.conn_queue.next,
				struct conn, source.sock.cl_list);
		list_del(&(src_sock_o->source.sock.cl_list));
		reset_conn(src_sock_o);
		kref_put(&(src_sock_o->ref), free_conn);
	}

	spin_unlock_bh(&cor_bindnodes);
}

int open_port(struct cor_sock *cs_l, __be64 port)
{
	int rc = 0;

	spin_lock_bh(&cor_bindnodes);
	if (get_corsock(port) != 0) {
		rc = -EADDRINUSE;
		goto out;
	}

	BUG_ON(cs_l->type != SOCKTYPE_UNCONNECTED);

	cs_l->type = SOCKTYPE_LISTENER;
	cs_l->data.listener.port = port;

	/* kref is not actually used */
	INIT_LIST_HEAD(&(cs_l->data.listener.conn_queue));
	init_waitqueue_head(&(cs_l->data.listener.wait));

	list_add_tail((struct list_head *) &(cs_l->data.listener.lh),
			&openports);

out:
	spin_unlock_bh(&cor_bindnodes);

	return rc;
}

/**
 * rc == 0 connected
 * rc == 2 port not open
 * rc == 3 listener queue full
 */
int connect_port(struct conn *trtg_unconn_l, __be64 port)
{
	struct cor_sock *cs;
	int rc = 0;

	spin_lock_bh(&cor_bindnodes);

	cs = get_corsock(port);
	if (cs == 0) {
		rc = 2;
		goto out;
	}

	spin_lock_bh(&(cs->lock));

	if (unlikely(cs->data.listener.queue_len >=
			cs->data.listener.queue_maxlen)) {
		if (cs->data.listener.queue_maxlen <= 0)
			rc = 2;
		else
			rc = 3;

		goto out2;
	}

	kref_get(&(trtg_unconn_l->reversedir->ref));

	BUG_ON(trtg_unconn_l->is_client != 1);
	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	conn_init_sock_target(trtg_unconn_l);
	conn_init_sock_source(trtg_unconn_l->reversedir);
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));

	list_add_tail(&(trtg_unconn_l->reversedir->source.sock.cl_list),
			&(cs->data.listener.conn_queue));
	cs->data.listener.queue_len++;
	wake_up_interruptible(&(cs->data.listener.wait));

out2:
	spin_unlock_bh(&(cs->lock));

out:
	spin_unlock_bh(&cor_bindnodes);
	return rc;
}

/**
 * rc == 0 connected
 * rc == 2 addrtype not found
 * rc == 3 addr not found
 * rc == 4 ==> connid allocation failed
 * rc == 4 ==> control msg alloc failed
 */
int connect_neigh(struct conn *trtg_unconn_l,
		__u16 addrtypelen, __u8 *addrtype,
		__u16 addrlen, __u8 *addr)
{
	int rc = 0;
	int ciorc;
	struct control_msg_out *cm;
	struct neighbor *nb = 0;

	if (addrtype_known(addrtypelen, addrtype) == 0) {
		rc = 2;
		goto discard;
	}

	nb = find_neigh(addrtypelen, addrtype, addrlen, addr);

	if (nb == 0) {
		rc = 3;
		goto discard;
	}

	cm = alloc_control_msg(nb, ACM_PRIORITY_HIGH);
	if (unlikely(cm == 0)) {
		rc = 4;
		goto discard;
	}

	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	ciorc = conn_init_out(trtg_unconn_l, nb);
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	if (unlikely(ciorc)) {
		rc = 4;
		goto freecm;
	}

	send_connect_nb(cm, trtg_unconn_l->reversedir->source.in.conn_id,
			trtg_unconn_l->reversedir->source.in.next_seqno,
			trtg_unconn_l->reversedir);

	if (0) {
freecm:
		free_control_msg(cm);
discard:
		trtg_unconn_l->targettype = TARGET_DISCARD;
	}

	if (nb != 0)
		kref_put(&(nb->ref), neighbor_free);

	return rc;
}

static int _reset_conn(struct conn *cn, int trgt_out_resetneeded)
{
	/**
	 * active conns have an additional ref to make sure that they are not
	 * freed when only one direction is referenced by the connid hashtable
	 */
	int krefput = 1;

	/* lock sourcetype/targettype */
	spin_lock_bh(&(cn->rcv_lock));

	if (cn->sourcetype == SOURCE_IN) {
		unsigned long iflags;

		spin_lock_irqsave(&(cn->source.in.nb->conn_list_lock), iflags);
		list_del(&(cn->source.in.nb_list));
		spin_unlock_irqrestore(&(cn->source.in.nb->conn_list_lock),
				iflags);

		set_busy_till(cn->source.in.nb, 0);

		krefput++;

		if (cn->source.in.conn_id != 0) {
			BUG_ON(cn->source.in.cir == 0);

			kref_init(&(cn->source.in.cir->ref));
			cn->source.in.cir->conn_id = cn->source.in.conn_id;
			cn->source.in.cir->pingcnt =
					cn->source.in.nb->connid_reuse_pingcnt;

			spin_lock_irqsave(
					&(cn->source.in.nb->connid_reuse_lock),
					iflags);
			htable_insert(&connid_reuse_table,
					(char *) cn->source.in.cir,
					cn->source.in.cir->conn_id);
			list_add_tail(&(cn->source.in.cir->lh),
					&(cn->source.in.nb->connid_reuse_list));
			spin_unlock_irqrestore(
					&(cn->source.in.nb->connid_reuse_lock),
					iflags);

			cn->source.in.cir = 0;

			if (htable_delete(&connid_table,
					cn->source.in.conn_id,
					&(cn->source.in.conn_id), free_conn)) {
				printk(KERN_ERR "error in _reset_conn: "
						"htable_delete src_in failed");
			}

			cn->source.in.conn_id = 0;

			free_ack_conns(cn);
		}

		atomic_dec(&num_conns);
		BUG_ON(atomic_read(&num_conns) < 0);

		reset_ooo_queue(cn);
	} else if (cn->sourcetype == SOURCE_SOCK) {
		wake_up_interruptible(&(cn->source.sock.wait));
	}

	if (cn->targettype == TARGET_UNCONNECTED) {
		connreset_cpacket_buffer(cn);
	} else if (cn->targettype == TARGET_OUT) {
		if (trgt_out_resetneeded && cn->target.out.conn_id != 0) {
			send_reset_conn(cn->target.out.nb,
					cn->target.out.conn_id,
					cn->reversedir->source.in.conn_id, 0);
		}

		if (cn->target.out.conn_id != 0) {
			struct reverse_connid_matchparam rcm;
			rcm.nb = cn->target.out.nb;
			rcm.conn_id = cn->target.out.conn_id;
			if (htable_delete(&reverse_connid_table,
					rcm_to_key(&rcm),
					&rcm, free_conn)){
				printk(KERN_ERR "error in _reset_conn: "
						"htable_delete target_out "
						"failed");
			}
		}

		cn->target.out.conn_id = 0;

		cancel_retrans(cn);

		qos_remove_conn(cn);
	} else if (cn->targettype == TARGET_SOCK) {
		wake_up_interruptible(&(cn->target.sock.wait));
	}

	databuf_ackdiscard(cn);

	spin_unlock_bh(&(cn->rcv_lock));

	reset_bufferusage(cn); /* source in only */
	connreset_credits(cn);
	connreset_sbt(cn); /* source sock only */

	return krefput;
}

/* warning: do not hold the rcv_lock while calling this! */
void reset_conn(struct conn *cn)
{
	int put1;
	int put2;

	int isreset1;
	int isreset2;

	if (cn->is_client) {
		spin_lock_bh(&(cn->rcv_lock));
		spin_lock_bh(&(cn->reversedir->rcv_lock));
	} else {
		spin_lock_bh(&(cn->reversedir->rcv_lock));
		spin_lock_bh(&(cn->rcv_lock));
	}

	BUG_ON(cn->isreset <= 1 && cn->reversedir->isreset >= 2);
	BUG_ON(cn->isreset >= 2 && cn->reversedir->isreset <= 1);

	isreset1 = cn->isreset;
	if (cn->isreset <= 1)
		cn->isreset = 2;

	isreset2 = cn->reversedir->isreset;
	if (cn->reversedir->isreset <= 1)
		cn->reversedir->isreset = 2;

	if (cn->is_client) {
		spin_unlock_bh(&(cn->rcv_lock));
		spin_unlock_bh(&(cn->reversedir->rcv_lock));
	} else {
		spin_unlock_bh(&(cn->reversedir->rcv_lock));
		spin_unlock_bh(&(cn->rcv_lock));
	}

	if (isreset1 >= 2)
		return;

	put1 = _reset_conn(cn, isreset1 == 0);
	put2 = _reset_conn(cn->reversedir, isreset2 == 0);

	/* free_conn may not be called, before both _reset_conn have finished */
	while (put1 > 0) {
		kref_put(&(cn->ref), free_conn);
		put1--;
	}

	while (put2 > 0) {
		kref_put(&(cn->reversedir->ref), free_conn);
		put2--;
	}
}

static int __init cor_common_init(void)
{
	int rc;

	struct conn c;

	check_log_64_11_table();
	check_log_300_24_table();

	printk(KERN_ERR "sizeof conn: %d", sizeof(c));
	printk(KERN_ERR "  conn.source: %d", sizeof(c.source));
	printk(KERN_ERR "  conn.target: %d", sizeof(c.target));
	printk(KERN_ERR "  conn.target.out: %d", sizeof(c.target.out));
	printk(KERN_ERR "  conn.buf: %d", sizeof(c.data_buf));

	printk(KERN_ERR "  mutex: %d", sizeof(struct mutex));
	printk(KERN_ERR "  spinlock: %d", sizeof(spinlock_t));
	printk(KERN_ERR "  kref: %d", sizeof(struct kref));

	conn_slab = kmem_cache_create("cor_conn", sizeof(struct conn), 8, 0, 0);
	connid_reuse_slab = kmem_cache_create("cor_connid_reuse",
			sizeof(struct connid_reuse_item), 8, 0, 0);

	htable_init(&connid_table, matches_connid_in,
			offsetof(struct conn, source.in.htab_entry),
			offsetof(struct conn, ref));

	htable_init(&reverse_connid_table, matches_reverse_connid,
			offsetof(struct conn, target.out.htab_entry),
			offsetof(struct conn, ref));

	htable_init(&connid_reuse_table, matches_reuse_connid,
			offsetof(struct connid_reuse_item, htab_entry),
			offsetof(struct connid_reuse_item, ref));

	atomic_set(&num_conns, 0);

	credits_init();
	forward_init();
	cor_kgen_init();
	cor_cpacket_init();

	rc = cor_snd_init();
	if (unlikely(rc != 0))
		return rc;

	rc = cor_neighbor_init();
	if (unlikely(rc != 0))
		return rc;

	rc = cor_rcv_init();
	if (unlikely(rc != 0))
		return rc;

	return 0;
}

module_init(cor_common_init);
MODULE_LICENSE("GPL");
