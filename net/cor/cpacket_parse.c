/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "cor.h"

struct work_struct cpacket_resume_work;
static int cpacket_resume_running;
DEFINE_SPINLOCK(cpacket_bufferlimits_lock);
static __u64 cpacket_bufferusage;
static int cpacket_kmallocfailed;
LIST_HEAD(cpacket_bufferwaiting);

static int reserve_cpacket_buffer(struct conn *trtg_unconn_l, int fromresume)
{
	unsigned long iflags;
	int putconn = 0;

	__u32 paramlen = trtg_unconn_l->target.unconnected.paramlen;
	__u32 resplen = 128;

	if (trtg_unconn_l->target.unconnected.cmd == CD_LIST_NEIGH)
		resplen = LISTNEIGH_RESP_MAXSIZE + 128;

	if (trtg_unconn_l->target.unconnected.in_buffer_wait_list &&
			fromresume == 0)
		return 1;

	if (trtg_unconn_l->data_buf.cpacket_buffer + paramlen + resplen >
			BUFFERLIMIT_CONNDATA_PERCONN)
		return 1;

	spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);

	if (list_empty(&cpacket_bufferwaiting) == 0 || paramlen + resplen +
			cpacket_bufferusage > BUFFERLIMIT_CONNDATA_GLOBAL) {
		/* out of bufferspace */
		if (trtg_unconn_l->target.unconnected.in_buffer_wait_list == 0){
			trtg_unconn_l->target.unconnected.in_buffer_wait_list=1;
			list_add_tail(&(trtg_unconn_l->target.unconnected.buffer_wait_list),
					&cpacket_bufferwaiting);
			kref_get(&(trtg_unconn_l->ref));

			if (cpacket_resume_running == 0) {
				schedule_work(&cpacket_resume_work);
				cpacket_resume_running = 1;
			}
		}
		spin_unlock_irqrestore(&cpacket_bufferlimits_lock,
					iflags);
		return 1;
	}

	cpacket_bufferusage += paramlen + resplen;

	if (trtg_unconn_l->target.unconnected.in_buffer_wait_list) {
		trtg_unconn_l->target.unconnected.in_buffer_wait_list = 0;
		list_del(&(trtg_unconn_l->target.unconnected.buffer_wait_list));
		putconn = 1;
	}

	spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);

	trtg_unconn_l->target.unconnected.cmdparams = kmalloc(paramlen,
			GFP_ATOMIC);
	if (unlikely(trtg_unconn_l->target.unconnected.cmdparams == 0)) {
		spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);
		cpacket_kmallocfailed = 1;
		trtg_unconn_l->target.unconnected.in_buffer_wait_list = 1;
		list_add(&(trtg_unconn_l->target.unconnected.buffer_wait_list),
				&cpacket_bufferwaiting);
		if (cpacket_resume_running == 0) {
			schedule_work(&cpacket_resume_work);
			cpacket_resume_running = 1;
		}
		if (putconn == 0)
			kref_get(&(trtg_unconn_l->ref));
		cpacket_bufferusage -= paramlen + resplen;
		spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);
		return 1;
	}

	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	trtg_unconn_l->reversedir->data_buf.cpacket_buffer += resplen;
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));

	if (putconn)
		kref_put(&(trtg_unconn_l->ref), free_conn);

	return 0;
}

static void cpacket_buffer_resume(struct work_struct *work)
{
	unsigned long iflags;
	int i;

	spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);

	cpacket_kmallocfailed = 0;

	for (i=0;i<100;i++) {
		struct conn *trtg_unconn;
		int doparse = 1;

		if (list_empty(&cpacket_bufferwaiting))
			goto finish;

		if (cpacket_kmallocfailed)
			break;

		trtg_unconn = container_of(cpacket_bufferwaiting.next,
				struct conn,
				target.unconnected.buffer_wait_list);

		kref_get(&(trtg_unconn->ref));

		spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);

		spin_lock_bh(&(trtg_unconn->rcv_lock));

		if (unlikely(trtg_unconn->targettype != TARGET_UNCONNECTED)) {
			doparse = 0;
			goto unlock;
		}

		spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);

		if (unlikely(trtg_unconn->target.unconnected.in_buffer_wait_list
				== 0)) {
			doparse = 0;
		} else if (unlikely(trtg_unconn->isreset != 0)) {
			trtg_unconn->target.unconnected.in_buffer_wait_list = 0;
			list_del(&(trtg_unconn->target.unconnected.buffer_wait_list));
			kref_put(&(trtg_unconn->ref), free_conn);
			doparse = 0;
		}

		spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);

unlock:
		spin_unlock_bh(&(trtg_unconn->rcv_lock));

		if (likely(doparse))
			parse(trtg_unconn, 1);

		kref_put(&(trtg_unconn->ref), free_conn);
		spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);
	}

	schedule_work(&cpacket_resume_work);

	if (0) {
finish:
		cpacket_resume_running = 0;
	}

	spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);
}

void free_cpacket_buffer(__s32 amount)
{
	unsigned long iflags;
	spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);
	BUG_ON(amount > 0 && amount > cpacket_bufferusage);
	cpacket_bufferusage -= amount;

	if (amount > 0 && list_empty(&cpacket_bufferwaiting) == 0 &&
			cpacket_resume_running == 0) {
		schedule_work(&cpacket_resume_work);
		cpacket_resume_running = 1;
	}
	spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);
}

void connreset_cpacket_buffer(struct conn *trtg_unconn_l)
{
	if (trtg_unconn_l->target.unconnected.in_buffer_wait_list) {
		unsigned long iflags;
		spin_lock_irqsave(&cpacket_bufferlimits_lock, iflags);
		trtg_unconn_l->target.unconnected.in_buffer_wait_list = 0;
		list_del(&(trtg_unconn_l->target.unconnected.buffer_wait_list));
		spin_unlock_irqrestore(&cpacket_bufferlimits_lock, iflags);
		kref_put(&(trtg_unconn_l->ref), free_conn);
	}

	if (trtg_unconn_l->target.unconnected.cmdparams != 0) {
		BUG_ON(trtg_unconn_l->target.unconnected.paramlen <= 0);
		kfree(trtg_unconn_l->target.unconnected.cmdparams);
		trtg_unconn_l->target.unconnected.cmdparams = 0;
		free_cpacket_buffer(trtg_unconn_l->target.unconnected.paramlen);
	}
}

int encode_len(char *buf, int buflen, __u32 len)
{
	BUG_ON(buf == 0);
	BUG_ON(buflen < 4);

	if (len <= 127) {
		buf[0] = (__u8) len;
		return 1;
	}

	if (len <= 16511) {
		buf[0] = (__u8) ((len - 128) /256 + 128);
		buf[1] = (__u8) ((len- 128) & 255);
		return 2;
	}

	if (len < 1073741951) {
		__u32 len_be = cpu_to_be32(len - 16511);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 192;
		buf[1] = len_p[1];
		buf[2] = len_p[2];
		buf[3] = len_p[3];
		return 4;
	}

	return -1;
}

int decode_len(char *buf, int buflen, __u32 *len)
{
	if (buflen < 1)
		return 0;

	if ((__u8) buf[0] <= 127) {
		*len = (__u8) buf[0];
		return 1;
	}

	if (buflen < 2)
		return 0;

	if ((__u8) buf[0] <= 191) {
		*len = 128 + ((__u32) ((__u8) buf[0]) - 128) * 256 +
				((__u8) buf[1]);
		return 2;
	}

	if (buflen < 4)
		return 0;

	((char *) len)[0] = buf[0] - 192;
	((char *) len)[1] = buf[1];
	((char *) len)[2] = buf[2];
	((char *) len)[3] = buf[3];

	*len = be32_to_cpu(*len) + 16511;
	return 4;
}

static void send_resp_ok(struct conn *trtg_unconn_l)
{
	__u8 respcode = CDR_EXECOK;

	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	receive_cpacketresp(trtg_unconn_l->reversedir, (char *) &respcode, 1);
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
}

static char *get_error_reason_text(__u16 reasoncode)
{
	if (reasoncode == CDR_EXECFAILED_UNKNOWN_COMMAND)
		return "Unknown command";
	else if (reasoncode == CDR_EXECFAILED_PERMISSION_DENIED)
		return "Permission denied";
	else if (reasoncode == CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESSOURCES)
		return "Temporarily out of ressources";
	else if (reasoncode == CDR_EXECFAILED_CMD_TOO_SHORT)
		return "The length of the command is too short for all params";
	else if (reasoncode == CDR_EXECFAILED_CMD_TOO_LONG)
		return "command param is too long";
	else if (reasoncode == CDR_EXECFAILED_TARGETADDRTYPE_UNKNOWN)
		return "targettype unknown";
	else if (reasoncode == CDR_EXECFAILED_TARGETADDR_DOESNTEXIST)
		return "targetaddr does not exist";
	else if (reasoncode == CDR_EXECFAILED_TARGETADDR_PORTCLOSED)
		return "Port is on open";
	else if (reasoncode == CDR_EXECFAILED_LISTENERQUEUE_FULL)
		return "Listener queue full";
	else
		return 0;
}

static void send_resp_failed(struct conn *trtg_unconn_l, __u16 reasoncode)
{
	char hdr[7];

	char *reasontext;
	int reasonlen = 0;
	int reasonlen_len;

	hdr[0] = CDR_EXECFAILED;

	reasoncode = cpu_to_be16(reasoncode);
	hdr[1] = ((char *) &reasoncode)[0];
	hdr[2] = ((char *) &reasoncode)[1];

	reasontext = get_error_reason_text(reasoncode);
	if (reasontext)
		reasonlen = strnlen(reasontext, 1024);
	reasonlen_len = encode_len(hdr + 3, 4, reasonlen);
	BUG_ON(reasonlen_len <= 0);

	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	receive_cpacketresp(trtg_unconn_l->reversedir, hdr, 3 + reasonlen_len);
	receive_cpacketresp(trtg_unconn_l->reversedir, reasontext, reasonlen);
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
}

static void send_resp_bin(struct conn *trtg_unconn_l, char *buf, __u32 len)
{
	char hdr[5];
	int len_len;

	hdr[0] = CDR_BINDATA;
	len_len = encode_len(hdr + 1, 4, len);

	BUG_ON(len_len <= 0);

	spin_lock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
	receive_cpacketresp(trtg_unconn_l->reversedir, hdr, len_len + 1);
	receive_cpacketresp(trtg_unconn_l->reversedir, buf, len);
	spin_unlock_bh(&(trtg_unconn_l->reversedir->rcv_lock));
}

static void parse_set_tos(struct conn *trtg_unconn_l)
{
	if (unlikely(trtg_unconn_l->target.unconnected.paramlen < 2)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	trtg_unconn_l->tos =
			(trtg_unconn_l->target.unconnected.cmdparams[0]) & 3;
	trtg_unconn_l->reversedir->tos =
			(trtg_unconn_l->target.unconnected.cmdparams[1]) & 3;
}

static void parse_list_neigh(struct conn *trtg_unconn_l)
{
	__u32 limit;
	__u32 offset;
	__u32 len;

	__u32 read = 0;
	int rc;

	char *buf;

	BUG_ON(read > trtg_unconn_l->target.unconnected.paramlen);
	rc = decode_len(trtg_unconn_l->target.unconnected.cmdparams + read,
			trtg_unconn_l->target.unconnected.paramlen - read,
			&limit);

	if (unlikely(rc <= 0)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	read += rc;

	BUG_ON(read > trtg_unconn_l->target.unconnected.paramlen);
	rc = decode_len(trtg_unconn_l->target.unconnected.cmdparams + read,
			trtg_unconn_l->target.unconnected.paramlen - read,
			&offset);

	if (unlikely(rc <= 0)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	read += rc;

	buf = kmalloc(LISTNEIGH_RESP_MAXSIZE, GFP_ATOMIC);

	if (unlikely(buf == 0)) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESSOURCES);
		return;
	}

	send_resp_ok(trtg_unconn_l);

	len = generate_neigh_list(buf, 2048, limit, offset);
	send_resp_bin(trtg_unconn_l, buf, len);
}

static void parse_connect_port(struct conn *trtg_unconn_l)
{
	__be64 addr;
	int rc;

	if (unlikely(trtg_unconn_l->target.unconnected.paramlen < 8)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	memcpy((char *) &addr, trtg_unconn_l->target.unconnected.cmdparams, 8);

	rc = connect_port(trtg_unconn_l, addr);

	if (rc == 0) {
		send_resp_ok(trtg_unconn_l);
	} else if (rc == 2) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TARGETADDR_DOESNTEXIST);
	} else if (rc == 3) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_LISTENERQUEUE_FULL);
	} else {
		BUG();
	}
}

static void parse_connect_nb(struct conn *trtg_unconn_l)
{
	__u32 addrtypelen;
	__u32 addrlen;
	__u8 *addrtype;
	__u8 *addr;

	__u32 read = 0;
	int rc;

	BUG_ON(read > trtg_unconn_l->target.unconnected.paramlen);
	rc = decode_len(trtg_unconn_l->target.unconnected.cmdparams + read,
			trtg_unconn_l->target.unconnected.paramlen - read,
			&addrtypelen);

	if (unlikely(rc <= 0)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	read += rc;

	BUG_ON(read > trtg_unconn_l->target.unconnected.paramlen);
	rc = decode_len(trtg_unconn_l->target.unconnected.cmdparams + read,
			trtg_unconn_l->target.unconnected.paramlen - read,
			&addrlen);

	if (unlikely(rc <= 0)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	read += rc;

	if (unlikely(addrtypelen > 65535 || addrtypelen > 65535)) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TARGETADDR_DOESNTEXIST);
		return;
	}

	if (unlikely((read + addrtypelen + addrlen) >
			trtg_unconn_l->target.unconnected.paramlen)) {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_CMD_TOO_SHORT);
		return;
	}

	addrtype = trtg_unconn_l->target.unconnected.cmdparams + read;
	addr = addrtype + addrtypelen;

	rc = connect_neigh(trtg_unconn_l, (__u16) addrtypelen, addrtype,
			(__u16) addrlen, addr);

	if (rc == 0) {
		#warning todo send ok when receiving conn_success and failed when receiving reset
		send_resp_ok(trtg_unconn_l);
	} else if (rc == 2) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TARGETADDRTYPE_UNKNOWN);
	} else if (rc == 3) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TARGETADDR_DOESNTEXIST);
	} else if (rc == 4) {
		send_resp_failed(trtg_unconn_l,
				CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESSOURCES);
	} else {
		BUG();
	}
}

static void parse_cmd(struct conn *trtg_unconn_l)
{
	__u16 code = trtg_unconn_l->target.unconnected.cmd;

	BUG_ON(trtg_unconn_l->target.unconnected.cmdparams == 0);

	if (code == CD_CONNECT_NB) {
		parse_connect_nb(trtg_unconn_l);
	} else if (code == CD_CONNECT_PORT) {
		parse_connect_port(trtg_unconn_l);
	} else if (code == CD_LIST_NEIGH) {
		parse_list_neigh(trtg_unconn_l);
	} else if (code == CD_SET_TOS) {
		parse_set_tos(trtg_unconn_l);
	} else {
		send_resp_failed(trtg_unconn_l, CDR_EXECFAILED_UNKNOWN_COMMAND);
	}
}

static void read_cmd(struct conn *trtg_unconn_l)
{
	int pull;

	pull = min(trtg_unconn_l->target.unconnected.paramlen -
			trtg_unconn_l->target.unconnected.cmdread,
			trtg_unconn_l->data_buf.read_remaining);

	BUG_ON(pull < 0);
	BUG_ON(trtg_unconn_l->target.unconnected.cmdparams == 0);

	if (pull == 0)
		return;

	databuf_pull(trtg_unconn_l,
			trtg_unconn_l->target.unconnected.cmdparams +
			trtg_unconn_l->target.unconnected.cmdread, pull);
	databuf_ackread(trtg_unconn_l);

	trtg_unconn_l->target.unconnected.cmdread += pull;
}

static void read_discard(struct conn *trtg_unconn_l)
{
	BUG_ON(trtg_unconn_l->target.unconnected.paramlen_read == 0);

	while (trtg_unconn_l->target.unconnected.paramlen <
			trtg_unconn_l->target.unconnected.cmdread &&
			trtg_unconn_l->data_buf.read_remaining > 0) {
		char buf[1];
		databuf_pull(trtg_unconn_l, buf, 1);
		databuf_ackread(trtg_unconn_l);

		trtg_unconn_l->target.unconnected.cmdread++;
	}
}

static void read_hdr(struct conn *trtg_unconn_l)
{
	BUG_ON(trtg_unconn_l->target.unconnected.cmdparams != 0);

	while (trtg_unconn_l->target.unconnected.paramlen_read == 0 &&
			trtg_unconn_l->data_buf.read_remaining > 0) {
		int rc;

		BUG_ON(trtg_unconn_l->target.unconnected.cmdread >= 6);

		if (trtg_unconn_l->target.unconnected.cmdread < 2) {
			char buf[1];
			databuf_pull(trtg_unconn_l, buf, 1);
			trtg_unconn_l->target.unconnected.cmd <<= 8;
			trtg_unconn_l->target.unconnected.cmd += buf[0];
			trtg_unconn_l->target.unconnected.cmdread += 1;
			continue;
		}

		databuf_pull(trtg_unconn_l,
				trtg_unconn_l->target.unconnected.paramlen_buf +
				trtg_unconn_l->target.unconnected.cmdread - 2,
				1);
		trtg_unconn_l->target.unconnected.cmdread += 1;

		rc = decode_len(trtg_unconn_l->target.unconnected.paramlen_buf,
				trtg_unconn_l->target.unconnected.cmdread - 2,
				&(trtg_unconn_l->target.unconnected.paramlen));

		if (rc > 0) {
			trtg_unconn_l->target.unconnected.paramlen_read = 1;
			trtg_unconn_l->target.unconnected.cmdread = 0;
			break;
		}
	}
}

void parse(struct conn *trtg_unconn, int fromresume)
{
start:
	spin_lock_bh(&(trtg_unconn->rcv_lock));

	if (unlikely(trtg_unconn->isreset != 0))
		goto out;

	if (unlikely(trtg_unconn->targettype != TARGET_UNCONNECTED))
		goto out;

	BUG_ON(trtg_unconn->data_buf.cpacket_buffer != 0);

	if (trtg_unconn->target.unconnected.paramlen_read == 0) {
		read_hdr(trtg_unconn);
		if (trtg_unconn->target.unconnected.paramlen_read == 0)
			goto out;

		if (trtg_unconn->target.unconnected.paramlen >
				MAX_CONN_CMD_LEN) {
			send_resp_failed(trtg_unconn,
					CDR_EXECFAILED_CMD_TOO_LONG);
		}
	}

	if (trtg_unconn->target.unconnected.paramlen > MAX_CONN_CMD_LEN) {
		read_discard(trtg_unconn);
		goto out;
	}

	if (trtg_unconn->target.unconnected.cmdparams == 0) {
		if (reserve_cpacket_buffer(trtg_unconn, fromresume))
			goto out;
	}

	if (trtg_unconn->target.unconnected.paramlen >
			trtg_unconn->target.unconnected.cmdread) {
		read_cmd(trtg_unconn);
	}

	if (trtg_unconn->target.unconnected.paramlen ==
			trtg_unconn->target.unconnected.cmdread) {
		char *cmd = trtg_unconn->target.unconnected.cmdparams;
		__s32 cpacket_buffer;
		__u32 paramlen = trtg_unconn->target.unconnected.paramlen;
		int flush = 0;

		BUG_ON(trtg_unconn->is_client == 0);
		spin_lock_bh(&(trtg_unconn->reversedir->rcv_lock));
		cpacket_buffer = trtg_unconn->reversedir->data_buf.cpacket_buffer;
		spin_unlock_bh(&(trtg_unconn->reversedir->rcv_lock));
		parse_cmd(trtg_unconn);
		kfree(cmd);
		spin_lock_bh(&(trtg_unconn->reversedir->rcv_lock));
		trtg_unconn->reversedir->data_buf.cpacket_buffer =
				trtg_unconn->reversedir->data_buf.totalsize +
				trtg_unconn->reversedir->data_buf.overhead;
		cpacket_buffer -= trtg_unconn->reversedir->data_buf.cpacket_buffer;
		spin_unlock_bh(&(trtg_unconn->reversedir->rcv_lock));
		free_cpacket_buffer(paramlen + cpacket_buffer);

		if (trtg_unconn->targettype != TARGET_UNCONNECTED) {
			flush = 1;
		} else {
			trtg_unconn->target.unconnected.cmdparams = 0;
			trtg_unconn->target.unconnected.cmdread = 0;
			trtg_unconn->target.unconnected.paramlen_read = 0;
			trtg_unconn->target.unconnected.cmd = 0;
			trtg_unconn->target.unconnected.paramlen = 0;
		}

		spin_unlock_bh(&(trtg_unconn->rcv_lock));
		if (flush)
			flush_buf(trtg_unconn);
		flush_buf(trtg_unconn->reversedir);
		if (flush == 0)
			goto start;
		return;
	}

out:
	spin_unlock_bh(&(trtg_unconn->rcv_lock));
}

int __init cor_cpacket_init(void)
{
	INIT_WORK(&cpacket_resume_work, cpacket_buffer_resume);
	cpacket_resume_running = 0;
	cpacket_bufferusage = 0;
	cpacket_kmallocfailed = 0;
	return 0;
}

MODULE_LICENSE("GPL");
