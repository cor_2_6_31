/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */
#include "cor.h"

DEFINE_SPINLOCK(credits_list_lock);
LIST_HEAD(credit_refresh_conns);
static struct delayed_work credit_refresh_work;
static int refresh_running = 0;

static __u64 decay_credits(__u64 credits)
{
	int decay_rate = (CREDIT_DECAYTIME_HOURS * 3) / 2;
	if (unlikely(decay_rate == 0))
		return 0;
	return credits - credits/decay_rate;
}

__u32 creditrate_initial(void)
{
	__u64 rate = (CREDITS_TOTAL - decay_credits(CREDITS_TOTAL)) / 3600;
	if (unlikely((rate >> 32) > 0))
		return -1;
	return rate;
}

/* credit_lock must be held while calling this */
static void refresh_credits_state(struct neighbor *nb, ktime_t now)
{
	__u64 creditrate_add;
	__u64 creditrate_sub;

	if (ktime_before_eq(now, nb->ktime_credit_update))
		return;

	creditrate_add = nb->creditrate_earning;
	if (unlikely(creditrate_add + nb->creditrate_initial < creditrate_add))
		creditrate_add = -1;
	else
		creditrate_add += nb->creditrate_initial;

	creditrate_sub = multiply_div(nb->creditrate_spending,
			1000 - CREDIT_EXCHANGETAX_PERMILLE, 1000);

	if (creditrate_sub > creditrate_add) {
		__u64 rem;

		__u64 adj = multiply_div2(creditrate_sub - creditrate_add,
				0 - nb->credits_rem, ktime_to_ns(now) -
				ktime_to_ns(nb->ktime_credit_update),
				1000000000, &rem);

		BUG_ON(rem >= 1000000000);

		if (unlikely(nb->credits < adj)) {
			nb->credits = 0;
			nb->credits_rem = 0;
		} else {
			nb->credits -= adj;
			nb->credits_rem = 0 - rem;
		}
	} else if (creditrate_sub < creditrate_add) {
		__u64 rem;

		__u64 adj = multiply_div2(creditrate_add - creditrate_sub,
				nb->credits_rem, ktime_to_ns(now) -
				ktime_to_ns(nb->ktime_credit_update),
				1000000000, &rem);

		BUG_ON(rem >= 1000000000);

		if (unlikely(nb->credits + adj < nb->credits)) {
			nb->credits = -1;
			nb->credits_rem = 0;
		} else {
			nb->credits += adj;
			nb->credits_rem = rem;
		}
	}

	nb->ktime_credit_update = now;

	if (unlikely(ktime_after(ktime_add_us(nb->ktime_credit_decay,
			3600LL * 1000000), now))) {
		nb->credits = decay_credits(nb->credits);

		nb->ktime_credit_decay = ktime_add_us(
				nb->ktime_credit_decay, 3600LL * 1000000);

		if (unlikely(ktime_after(ktime_add_us(nb->ktime_credit_decay,
				3600LL * 1000000), now))) {
			nb->ktime_credit_decay = ktime_sub_us(now,
					3600LL * 1000000);
		}
	}
}

void set_creditrate_initial(struct neighbor *nb, __u32 creditrate, ktime_t now)
{
	unsigned long iflags;

	spin_lock_irqsave(&(nb->credits_lock), iflags);
	refresh_credits_state(nb, now);
	nb->creditrate_initial = creditrate;
	spin_unlock_irqrestore(&(nb->credits_lock), iflags);
}

static __u64 decay_time_to_crate(struct neighbor *nb, __u16 decay_time_raw)
{
	__u64 decay_time = dec_log_300_24(decay_time_raw);
	if (decay_time == 0)
		return 0;
	return nb->credits / (decay_time * 3 / 2);
}

static __u16 crate_to_decay_time(struct neighbor *nb, __u64 crate_out)
{
	if (crate_out <= 2)
		return enc_log_300_24(0);
	return enc_log_300_24(nb->credits / multiply_div(crate_out,
			2*(1000 - CREDIT_EXCHANGETAX_PERMILLE), 3 * 1000));
}

static void refresh_crate_forward(struct conn *cn_lc, __u64 timediff_ns)
{
	__u8 last_bufferstate = cn_lc->last_bufferstate;

	int shift_base;

	if (cn_lc->tos == TOS_NORMAL)
		shift_base = 3;
	else if (cn_lc->tos == TOS_LATENCY)
		shift_base = 2;
	else if (cn_lc->tos == TOS_THROUGHPUT)
		shift_base = 5;
	else if (cn_lc->tos == TOS_PRIVACY)
		shift_base = 6;
	else
		BUG();

	/**
	 * buffer full shifts 1 bit more so only about 1/3rd if there is no
	 * bufferspace left
	 */

	BUG_ON(cn_lc->crate_forward > (1 << 31));

	while (timediff_ns > 0) {
		__u64 currdiff = timediff_ns;
		if (currdiff > 1000000)
			currdiff = 1000000;

		if (last_bufferstate == 0)
			cn_lc->crate_forward = ( ((__u64)cn_lc->crate_forward) *
					(currdiff << (shift_base - 2))) /
					(currdiff << shift_base);
		else
			cn_lc->crate_forward = ( ((__u64)cn_lc->crate_forward) *
					(currdiff << shift_base)) /
					(currdiff << (shift_base - 1));

		if (cn_lc->crate_forward > (1 << 31)) {
			cn_lc->crate_forward = (1 << 31);
			break;
		}

		if (cn_lc->crate_forward < (1 << 16)) {
			cn_lc->crate_forward = (1 << 16);
			break;
		}

		timediff_ns -= currdiff;
	}

	cn_lc->crate_out = multiply_div(cn_lc->crate_in, cn_lc->crate_forward,
			1 << 31);
}

static int decaytime_send_needed(struct conn *trgt_out_lc, __u16 *decaytime)
{
	__u16 diff = 0;

	if (trgt_out_lc->target.out.decaytime_send_allowed == 0)
		return 0;

	*decaytime = crate_to_decay_time(trgt_out_lc->target.out.nb,
			trgt_out_lc->crate_out);

	if (unlikely(trgt_out_lc->target.out.conn_id == 0)) {
		trgt_out_lc->reversedir->source.in.decaytime = *decaytime;
		trgt_out_lc->target.out.decaytime_last = *decaytime;
		trgt_out_lc->target.out.decaytime_send_allowed = 0;
		return 0;
	}

	if (*decaytime > trgt_out_lc->target.out.decaytime_last)
		diff = *decaytime - trgt_out_lc->target.out.decaytime_last;
	if (*decaytime < trgt_out_lc->target.out.decaytime_last)
		diff = trgt_out_lc->target.out.decaytime_last - *decaytime;

	if (diff != 0 && (*decaytime == 0 ||
			trgt_out_lc->target.out.decaytime_last == 0))
		return 2;

	if (trgt_out_lc->tos == TOS_PRIVACY) {
		if (diff >= 5)
			return 2;
		if (diff >= 3)
			return 1;

	} else {
		if (diff >= 2)
			return 2;
		if (diff >= 1)
			return 1;
	}

	return 0;
}

static __u64 newnbrate(__u64 nbrate, __u64 oldconnrate, __u64 newconnrate)
{
	if (unlikely(nbrate < oldconnrate))
		nbrate = 0;
	else
		nbrate -= oldconnrate;

	if (unlikely(nbrate + newconnrate < nbrate))
		nbrate = -1;
	else
		nbrate += newconnrate;

	return nbrate;
}

static void refresh_conn_crate_out(struct conn *trgt_out_lc, ktime_t now,
		__u64 timediff_ns)
{
	unsigned long iflags;

	__u64 oldrate;

	int send;
	__u16 crate;

	if (likely(trgt_out_lc->target.out.conn_id != 0)) {
		refresh_crate_forward(trgt_out_lc, timediff_ns);

		if (((__s32) (trgt_out_lc->target.out.seqno_nextsend -
				trgt_out_lc->target.out.seqno_windowlimit)) <=0)
			trgt_out_lc->last_bufferstate = 1;
		else
			trgt_out_lc->last_bufferstate = 0;
	}

	spin_lock_irqsave(&(trgt_out_lc->target.out.nb->credits_lock), iflags);

	oldrate = trgt_out_lc->crate_out;
	refresh_credits_state(trgt_out_lc->target.out.nb, now);
	trgt_out_lc->target.out.nb->creditrate_spending = newnbrate(
			trgt_out_lc->target.out.nb->creditrate_spending,
			oldrate, trgt_out_lc->crate_out);

	send = decaytime_send_needed(trgt_out_lc, &crate);

	spin_unlock_irqrestore(&(trgt_out_lc->target.out.nb->credits_lock),
			iflags);

	if (unlikely(send != 0)) {
		send_decaytime(trgt_out_lc, (send == 2), crate);
		return;
	}
}

static void refresh_conn_crate_in(struct conn *src_in_lc, ktime_t now)
{
	unsigned long iflags;

	__u64 oldrate = src_in_lc->crate_in;

	spin_lock_irqsave(&(src_in_lc->source.in.nb->credits_lock), iflags);

	refresh_credits_state(src_in_lc->source.in.nb, now);

	src_in_lc->crate_in = decay_time_to_crate(src_in_lc->source.in.nb,
			src_in_lc->source.in.decaytime);
	src_in_lc->source.in.nb->creditrate_earning = newnbrate(
			src_in_lc->source.in.nb->creditrate_earning, oldrate,
			src_in_lc->crate_in);

	spin_unlock_irqrestore(&(src_in_lc->source.in.nb->credits_lock),
			iflags);
}

static void refresh_conn_crates(struct conn *cn_lc, ktime_t now,
		__u64 timediff_ns)
{
	/* set conn->crate_in */
	if (cn_lc->sourcetype == SOURCE_NONE) {
		cn_lc->crate_in = cn_lc->reversedir->crate_out;
	} else if (cn_lc->sourcetype == SOURCE_IN) {
		refresh_conn_crate_in(cn_lc, now);
	} else if (cn_lc->sourcetype == SOURCE_SOCK) {
		if (cn_lc->reversedir->target.sock.credituser == 1)
			cn_lc->crate_in = cn_lc->reversedir->crate_out;
		else if ((cn_lc->source.sock.crate +
				cn_lc->reversedir->crate_out) <
				cn_lc->reversedir->crate_out)
			cn_lc->crate_in = -1;
		else
			cn_lc->crate_in = cn_lc->reversedir->crate_out +
					cn_lc->source.sock.crate;
	} else {
		BUG();
	}

	/* set conn->crate_forward + conn->crate_out */
	if (cn_lc->targettype == TARGET_UNCONNECTED ||
			cn_lc->targettype == TARGET_DISCARD) {
		cn_lc->crate_out = cn_lc->crate_in;
	} else if (cn_lc->targettype == TARGET_SOCK) {
		if (cn_lc->target.sock.credituser == 0) {
			cn_lc->crate_out = cn_lc->crate_in;
		} else {
			refresh_crate_forward(cn_lc, timediff_ns);
			cn_lc->last_bufferstate = (
					cn_lc->reversedir->source.sock.wait_len
				!= 0);
		}
	} else if (cn_lc->targettype == TARGET_OUT) {
		refresh_conn_crate_out(cn_lc, now, timediff_ns);
	} else {
		BUG();
	}
}

static void _refresh_conn_credits(struct conn *cn_lc, ktime_t now)
{
	__u64 timediff_ns = ktime_to_ns(now) -
			ktime_to_ns(cn_lc->ktime_credit_update);

	if (ktime_before_eq(now, cn_lc->ktime_credit_update)) {
		timediff_ns = 0;
		goto crates;
	}

	if (unlikely(cn_lc->crate_out > cn_lc->crate_in)) {
		__u64 rem;

		__u64 adj = multiply_div2(cn_lc->crate_out - cn_lc->crate_in,
				0 - cn_lc->credits_rem, timediff_ns,
				1000000000, &rem);

		BUG_ON(rem >= 1000000000);

		if (unlikely(cn_lc->credits < adj)) {
			cn_lc->credits = 0;
			cn_lc->credits_rem = 0;
		} else {
			cn_lc->credits -= adj;
			cn_lc->credits_rem = 0 - rem;
		}
	} else {
		__u64 rem;

		__u64 adj = multiply_div2(cn_lc->crate_in - cn_lc->crate_out,
				cn_lc->credits_rem, timediff_ns,
				1000000000, &rem);

		BUG_ON(rem >= 1000000000);

		if (unlikely(cn_lc->credits + adj < cn_lc->credits)) {
			cn_lc->credits = -1;
			cn_lc->credits_rem = 0;
		} else {
			cn_lc->credits += adj;
			cn_lc->credits_rem = rem;
		}
	}

crates:
	refresh_conn_crates(cn_lc, now, timediff_ns);

	if (timediff_ns != 0)
		cn_lc->ktime_credit_update = now;
}

static void credits_unlock_conn(struct conn *cn, __u32 hints)
{
	if ((hints & 1) != 0)
		spin_unlock_bh(&(cn->rcv_lock));
	if ((hints & 2) != 0)
		spin_unlock_bh(&(cn->reversedir->rcv_lock));
	if ((hints & 4) != 0)
		spin_unlock_bh(&(cn->rcv_lock));
}

static __u32 credits_lock_conn(struct conn *cn)
{
	spin_lock_bh(&(cn->rcv_lock));
	if (cn->sourcetype == SOURCE_IN && cn->targettype == TARGET_OUT) {
		if (likely(cn->target.out.conn_id != 0))
			return 1;
	}

	if (cn->is_client) {
		spin_lock_bh(&(cn->reversedir->rcv_lock));
		return 6;
	} else {
		spin_unlock_bh(&(cn->rcv_lock));

		spin_lock_bh(&(cn->reversedir->rcv_lock));
		spin_lock_bh(&(cn->rcv_lock));

		return 3;
	}
}

int refresh_conn_credits(struct conn *cn, int fromperiodic, int locked)
{
	unsigned long iflags;
	__u32 unlockhints = 0;
	int rc = 0;
	int put = 0;
	ktime_t now;

	if (likely(locked == 0))
		unlockhints = credits_lock_conn(cn);

	if (unlikely(cn->isreset != 0)) {
		if (fromperiodic)
			rc = 1;
		goto out;
	}

	now = ktime_get();

	if (fromperiodic) {
		/* quit if not time for refresh yet */
		if (ktime_after(ktime_add_us(cn->ktime_credit_update,
				CREDIT_REFRESHINTERVAL_SEC * 1000000), now)) {
			int alreadyrefreshed;

			spin_lock_irqsave(&credits_list_lock, iflags);
			alreadyrefreshed = &(cn->credit_list) !=
					credit_refresh_conns.next;
			spin_unlock_irqrestore(&credits_list_lock, iflags);

			if (unlikely(alreadyrefreshed))
				goto out;

			rc = usecs_to_jiffies(ktime_us_delta(ktime_add_us(
					cn->ktime_credit_update,
					CREDIT_REFRESHINTERVAL_SEC * 1000000),
					now)) + 1;
			if (rc < HZ)
				rc = HZ;
			if (unlikely(rc > HZ * CREDIT_REFRESHINTERVAL_SEC))
				rc = HZ * CREDIT_REFRESHINTERVAL_SEC;
			goto out;
		}
	}

	if (cn->targettype == TARGET_UNCONNECTED ||
			cn->targettype == TARGET_DISCARD || (
			cn->targettype == TARGET_SOCK &&
			cn->target.sock.credituser == 0)) {
		if (unlikely(cn->in_credit_list)) {
			spin_lock_irqsave(&credits_list_lock, iflags);
			list_del(&(cn->credit_list));
			cn->in_credit_list = 0;
			spin_unlock_irqrestore(&credits_list_lock, iflags);
			put = 1;
		}

		if (cn->sourcetype == SOURCE_NONE || (
				cn->sourcetype == SOURCE_SOCK &&
				cn->reversedir->target.sock.credituser == 0))
			goto out;

		goto refresh;
	}

	spin_lock_irqsave(&credits_list_lock, iflags);

	if (unlikely(cn->in_credit_list == 0)) {
		cn->in_credit_list = 1;
		kref_get(&(cn->ref));
		if (refresh_running == 0) {
			schedule_delayed_work(&credit_refresh_work, HZ *
					CREDIT_REFRESHINTERVAL_SEC);
			refresh_running = 1;
		}
	} else {
		list_del(&(cn->credit_list));
	}
	list_add_tail(&(cn->credit_list), &credit_refresh_conns);


	spin_unlock_irqrestore(&credits_list_lock, iflags);

refresh:
	if (cn->sourcetype == SOURCE_NONE || (
			cn->sourcetype == SOURCE_SOCK &&
			cn->reversedir->target.sock.credituser == 0))
		_refresh_conn_credits(cn->reversedir, now);
	_refresh_conn_credits(cn, now);
	if (cn->targettype == TARGET_UNCONNECTED ||
			cn->targettype == TARGET_DISCARD || (
			cn->targettype == TARGET_OUT &&
			cn->target.out.conn_id == 0) || (
			cn->targettype == TARGET_SOCK &&
			cn->target.sock.credituser == 0))
		_refresh_conn_credits(cn->reversedir, now);

out:
	if (likely(locked == 0))
		credits_unlock_conn(cn, unlockhints);

	if (put)
		kref_put(&(cn->ref), free_conn);

	return rc;
}

void connreset_credits(struct conn *cn)
{
	unsigned long iflags;

	__u32 unlockhints = credits_lock_conn(cn);
	if (cn->in_credit_list) {
		spin_lock_irqsave(&credits_list_lock, iflags);
		list_del(&(cn->credit_list));
		cn->in_credit_list = 0;
		spin_unlock_irqrestore(&credits_list_lock, iflags);
		kref_put(&(cn->ref), free_conn);
	}

	if (cn->sourcetype == SOURCE_IN) {
		struct neighbor *nb = cn->source.in.nb;
		spin_lock_irqsave(&(nb->credits_lock), iflags);
		refresh_credits_state(nb, ktime_get());
		nb->creditrate_earning = newnbrate(nb->creditrate_earning,
				cn->crate_in, 0);
		spin_unlock_irqrestore(&(nb->credits_lock), iflags);
	}

	if (cn->targettype == TARGET_OUT) {
		struct neighbor *nb = cn->target.out.nb;
		spin_lock_irqsave(&(nb->credits_lock), iflags);
		refresh_credits_state(nb, ktime_get());
		nb->creditrate_spending = newnbrate(nb->creditrate_spending,
				cn->crate_out, 0);
		spin_unlock_irqrestore(&(nb->credits_lock), iflags);
	}
	credits_unlock_conn(cn, unlockhints);
}

void set_conn_in_decaytime(struct neighbor *nb, __u32 conn_id,
		struct conn *src_in, __u8 decaytime_seqno, __u16 decaytime)
{
	__u32 unlockhints = credits_lock_conn(src_in);

	if (unlikely(is_conn_in(src_in, nb, conn_id) == 0))
		goto out;

	if (unlikely(src_in->source.in.decaytime_seqno == 255)) {
		src_in->source.in.decaytime_seqno = decaytime_seqno;
		goto set;
	}

	if (src_in->source.in.decaytime_seqno != decaytime_seqno)
		goto out;
	src_in->source.in.decaytime_seqno = (src_in->source.in.decaytime_seqno +
			1) % 64;

set:
	src_in->source.in.decaytime = decaytime;
	refresh_conn_credits(src_in, 0, 1);

out:
	credits_unlock_conn(src_in, unlockhints);
}

static void background_refresh_credits(struct work_struct *work)
{
	unsigned long iflags;
	struct conn *cn;
	int rc = 0;

	while (rc == 0) {
		spin_lock_irqsave(&credits_list_lock, iflags);
		if (unlikely(list_empty(&(credit_refresh_conns)))) {
			rc = -1;
			refresh_running = 0;
			cn = 0;
		} else {
			cn = container_of(credit_refresh_conns.next,
					struct conn, credit_list);
			kref_get(&(cn->ref));
		}
		spin_unlock_irqrestore(&credits_list_lock, iflags);

		if (cn != 0) {
			rc = refresh_conn_credits(cn, 1, 0);
			kref_put(&(cn->ref), free_conn);
		}
	}

	if (likely(rc > 0)) {
		schedule_delayed_work(&credit_refresh_work, rc);
	}
}

void __init credits_init(void)
{
	INIT_DELAYED_WORK(&credit_refresh_work, background_refresh_credits);
}

MODULE_LICENSE("GPL");
