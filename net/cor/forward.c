/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/mutex.h>

#include "cor.h"

struct kmem_cache *data_buf_item_slab;

void databuf_init(struct conn *cn_init)
{
	memset(&(cn_init->data_buf), 0, sizeof(cn_init->data_buf));
	INIT_LIST_HEAD(&(cn_init->data_buf.items));
}

static inline void databuf_item_unlink(struct conn *cn_l, struct data_buf_item *item)
{
	list_del(&(item->buf_list));
	if (item->type == TYPE_BUF) {
		cn_l->data_buf.overhead -= sizeof(struct data_buf_item) +
				item->buflen - item->datalen;
	} else if (item->type == TYPE_SKB) {
		cn_l->data_buf.overhead -= sizeof(struct sk_buff);
	} else {
		BUG();
	}
}

void databuf_ackdiscard(struct conn *cn_l)
{
	__u32 freed = 0;
	while (!list_empty(&(cn_l->data_buf.items))) {
		struct data_buf_item *item = container_of(
				cn_l->data_buf.items.next,
				struct data_buf_item, buf_list);
		freed += item->datalen;

		databuf_item_unlink(cn_l, item);
		databuf_item_free(item);
	}

	cn_l->data_buf.totalsize -= freed;
	cn_l->data_buf.first_offset += freed;

	BUG_ON(cn_l->data_buf.totalsize != 0);
	BUG_ON(cn_l->data_buf.overhead != 0);

	if (cn_l->data_buf.cpacket_buffer != 0) {
		free_cpacket_buffer(cn_l->data_buf.cpacket_buffer);
		cn_l->data_buf.cpacket_buffer = 0;
	}

	cn_l->data_buf.read_remaining = 0;
	cn_l->data_buf.last_read_offset = 0;
	cn_l->data_buf.lastread = 0;

	if (cn_l->isreset == 0 && cn_l->sourcetype == SOURCE_IN)
		refresh_speedstat(cn_l, freed);
}

void reset_seqno(struct conn *cn_l, __u32 initseqno)
{
	cn_l->data_buf.first_offset = initseqno -
			cn_l->data_buf.totalsize +
			cn_l->data_buf.read_remaining;
}

static void databuf_nextreadchunk(struct conn *cn_l)
{
	if (cn_l->data_buf.lastread == 0) {
		BUG_ON(cn_l->data_buf.last_read_offset != 0);
		BUG_ON(list_empty(&(cn_l->data_buf.items)));
		cn_l->data_buf.lastread = container_of(
				cn_l->data_buf.items.next,
				struct data_buf_item, buf_list);
	} else if (&(cn_l->data_buf.lastread->buf_list) !=
			cn_l->data_buf.items.prev) {
		cn_l->data_buf.lastread = container_of(
				cn_l->data_buf.lastread->buf_list.next,
				struct data_buf_item, buf_list);

		cn_l->data_buf.last_read_offset = 0;
	}
}

void databuf_pull(struct conn *cn_l, char *dst, int len)
{
	BUG_ON(cn_l->data_buf.read_remaining < len);

	if (cn_l->data_buf.lastread == 0)
		databuf_nextreadchunk(cn_l);

	while(len > 0) {
		int cpy = len;

		char *srcbufcpystart = 0;
		int srcbufcpylen = 0;

		BUG_ON(cn_l->data_buf.lastread == 0);

		srcbufcpystart = cn_l->data_buf.lastread->buf +
				cn_l->data_buf.last_read_offset;
		srcbufcpylen = cn_l->data_buf.lastread->datalen -
				cn_l->data_buf.last_read_offset;

		if (cpy > srcbufcpylen)
			cpy = srcbufcpylen;

		memcpy(dst, srcbufcpystart, cpy);

		dst += cpy;
		len -= cpy;

		cn_l->data_buf.read_remaining -= cpy;
		cn_l->data_buf.last_read_offset += cpy;

		if (cpy == srcbufcpylen)
			databuf_nextreadchunk(cn_l);
	}
}

void databuf_pull_dbi(struct cor_sock *cs_rl, struct conn *trgt_sock_l)
{
	struct data_buf_item *dbi = 0;
	BUG_ON(cs_rl->type != SOCKTYPE_CONN);
	BUG_ON(cs_rl->data.conn.rcvitem != 0);

	if (trgt_sock_l->data_buf.read_remaining == 0)
		return;

	if (trgt_sock_l->data_buf.lastread == 0)
		databuf_nextreadchunk(trgt_sock_l);

	dbi = trgt_sock_l->data_buf.lastread;

	BUG_ON(dbi == 0);

	BUG_ON(&(dbi->buf_list) != trgt_sock_l->data_buf.items.next);

	cs_rl->data.conn.rcvitem = dbi;
	cs_rl->data.conn.rcvoffset = trgt_sock_l->data_buf.last_read_offset;

	trgt_sock_l->data_buf.first_offset += dbi->datalen;
	trgt_sock_l->data_buf.totalsize -= dbi->datalen;
	trgt_sock_l->data_buf.read_remaining -= dbi->datalen;

	if (unlikely(trgt_sock_l->data_buf.cpacket_buffer != 0)) {
		__u32 amount = dbi->datalen <
				trgt_sock_l->data_buf.cpacket_buffer ?
				dbi->datalen :
				trgt_sock_l->data_buf.cpacket_buffer;
		free_cpacket_buffer(amount);
		trgt_sock_l->data_buf.cpacket_buffer -= amount;
	}

	databuf_item_unlink(trgt_sock_l, dbi);

	trgt_sock_l->data_buf.lastread = 0;
	trgt_sock_l->data_buf.last_read_offset = 0;

	/* databuf_item_free(firstitem); */
}

void databuf_unpull(struct conn *trgt_out_l, __u32 bytes)
{
	trgt_out_l->data_buf.read_remaining += bytes;

	BUG_ON(trgt_out_l->data_buf.lastread == 0);

	while (bytes > trgt_out_l->data_buf.last_read_offset) {
		bytes -= trgt_out_l->data_buf.last_read_offset;
		trgt_out_l->data_buf.lastread = container_of(
				trgt_out_l->data_buf.lastread->buf_list.prev,
				struct data_buf_item, buf_list);
		BUG_ON(&(trgt_out_l->data_buf.lastread->buf_list) ==
				&(trgt_out_l->data_buf.items));
	}

	trgt_out_l->data_buf.last_read_offset -= bytes;
}

void databuf_pullold(struct conn *trgt_out_l, __u32 startpos, char *dst,
		int len)
{
	__u32 pos = trgt_out_l->data_buf.first_offset;
	struct data_buf_item *dbi = container_of(
			trgt_out_l->data_buf.items.next,
			struct data_buf_item, buf_list);

	while(1) {
		BUG_ON(&(dbi->buf_list) == &(trgt_out_l->data_buf.items));

		if (((__s32) (pos + dbi->datalen - startpos)) > 0)
			break;

		pos += dbi->datalen;
		dbi = container_of(dbi->buf_list.next, struct data_buf_item,
				buf_list);
	}

	while (len > 0) {
		int cpy = len;

		char *srcbufcpystart = 0;
		int srcbufcpylen = 0;

		BUG_ON(&(dbi->buf_list) == &(trgt_out_l->data_buf.items));

		BUG_ON(((__s32) (pos - startpos)) > 0);

		srcbufcpystart = dbi->buf + ((__s32) (startpos - pos));
		srcbufcpylen = dbi->datalen - ((__s32) (startpos - pos));

		if (cpy > srcbufcpylen)
			cpy = srcbufcpylen;

		memcpy(dst, srcbufcpystart, cpy);

		dst += cpy;
		len -= cpy;
		startpos += cpy;

		pos += dbi->datalen;
		dbi = container_of(dbi->buf_list.next, struct data_buf_item,
				buf_list);
	}
}

/* ack up to *not* including pos */
void databuf_ack(struct conn *trgt_out_l, __u32 pos)
{
	__u32 acked = 0;

	while (!list_empty(&(trgt_out_l->data_buf.items))) {
		struct data_buf_item *firstitem = container_of(
				trgt_out_l->data_buf.items.next,
				struct data_buf_item, buf_list);

		if (firstitem == trgt_out_l->data_buf.lastread)
			break;

		if ( ((__s32) (trgt_out_l->data_buf.first_offset +
				firstitem->datalen - pos)) > 0)
			break;

		trgt_out_l->data_buf.first_offset += firstitem->datalen;
		acked += firstitem->datalen;

		databuf_item_unlink(trgt_out_l, firstitem);
		databuf_item_free(firstitem);
	}

	trgt_out_l->data_buf.totalsize -= acked;

	BUG_ON(trgt_out_l->data_buf.totalsize == 0 &&
			trgt_out_l->data_buf.overhead != 0);

	if (unlikely(trgt_out_l->data_buf.cpacket_buffer != 0)) {
		__u32 amount = acked < trgt_out_l->data_buf.cpacket_buffer ?
				acked : trgt_out_l->data_buf.cpacket_buffer;
		free_cpacket_buffer(amount);
		trgt_out_l->data_buf.cpacket_buffer -= amount;
	}

	if (trgt_out_l->sourcetype == SOURCE_IN)
		refresh_speedstat(trgt_out_l, acked);
}

void databuf_ackread(struct conn *cn_l)
{
	__u32 acked = 0;

	while (!list_empty(&(cn_l->data_buf.items)) &&
			cn_l->data_buf.lastread != 0) {
		struct data_buf_item *firstitem = container_of(
				cn_l->data_buf.items.next,
				struct data_buf_item, buf_list);

		if (firstitem == cn_l->data_buf.lastread)
			break;

		acked += firstitem->datalen;

		databuf_item_unlink(cn_l, firstitem);
		databuf_item_free(firstitem);
	}

	cn_l->data_buf.first_offset += acked;
	cn_l->data_buf.totalsize -= acked;

	BUG_ON(cn_l->data_buf.totalsize == 0 && cn_l->data_buf.overhead != 0);

	if (unlikely(cn_l->data_buf.cpacket_buffer != 0)) {
		__u32 amount = acked < cn_l->data_buf.cpacket_buffer ?
				acked : cn_l->data_buf.cpacket_buffer;
		free_cpacket_buffer(amount);
		cn_l->data_buf.cpacket_buffer -= amount;
	}

	if (cn_l->sourcetype == SOURCE_IN)
		refresh_speedstat(cn_l, acked);
}

__s64 receive_buf(struct conn *cn_l, char *buf, __u32 datalen, __u32 buflen,
		int forcecpy)
{
	char *freewhenfinished = 0;
	struct data_buf_item *item = 0;

	__s64 totalcpy = 0;

	if (list_empty(&(cn_l->data_buf.items)) == 0) {
		struct list_head *last = cn_l->data_buf.items.prev;
		item = container_of(last, struct data_buf_item, buf_list);
	}

	while (datalen > 0) {
		int rc = 0;
		int cpy = datalen;

		#warning todo convert to bugon and do check on caller
		if (unlikely(unlikely(cn_l->data_buf.totalsize + datalen >
				 (1 << 30)) ||
				unlikely(cn_l->data_buf.overhead > (1<< 30)))) {
			rc = -EAGAIN;
			goto error;
		}

		if (forcecpy == 0 && item != 0 &&
				datalen < (item->buflen - item->datalen) &&
				datalen*2 < (buflen +
				sizeof(struct data_buf_item))) {
			forcecpy = 1;
			freewhenfinished = buf;
		}

		if (forcecpy == 0 || item == 0 ||
				item->buflen <= item->datalen) {
			item = kmem_cache_alloc(data_buf_item_slab, GFP_ATOMIC);
			if (unlikely(item == 0)) {
				rc = -ENOMEM;
				goto error;
			}

			memset(item, 0, sizeof(item));
			item->type = TYPE_BUF;

			if (forcecpy == 0) {
				item->buf = buf;
				item->datalen = datalen;
				item->buflen = buflen;
			} else {
				buflen = buf_optlen(datalen);
				item->buf = kmalloc(buflen, GFP_ATOMIC);

				if (unlikely(item->buf == 0)) {
					kmem_cache_free(data_buf_item_slab,
							item);
					rc = -ENOMEM;
					goto error;
				}
				item->datalen = 0;
				item->buflen = buflen;
			}

			list_add_tail(&(item->buf_list),
					&(cn_l->data_buf.items));

			cn_l->data_buf.overhead += item->buflen +
					sizeof(struct data_buf_item);
		}

		if (forcecpy == 0) {
			cpy = item->datalen;
		} else {
			BUG_ON(item->type != TYPE_BUF);
			BUG_ON(item->buflen <= item->datalen);

			if (item->buflen - item->datalen < cpy)
				cpy = (item->buflen - item->datalen);

			memcpy(item->buf + item->datalen, buf, cpy);
			item->datalen += cpy;
		}

		buf += cpy;
		datalen -= cpy;

		cn_l->data_buf.read_remaining += cpy;
		cn_l->data_buf.totalsize += cpy;
		cn_l->data_buf.overhead -= cpy;
		BUG_ON(cn_l->data_buf.totalsize != 0 &&
				cn_l->data_buf.overhead == 0);
		totalcpy += cpy;

error:
		if (unlikely(rc < 0)) {
			if (totalcpy == 0)
				return rc;
			break;
		}
	}

	if (freewhenfinished != 0)
		kfree(freewhenfinished);

	return totalcpy;
}

void receive_cpacketresp(struct conn *trtg_unconn_l, char *buf, int len)
{
	__s64 rc;
	BUG_ON(trtg_unconn_l->data_buf.cpacket_buffer <
			trtg_unconn_l->data_buf.totalsize + len);
	rc = receive_buf(trtg_unconn_l, buf, len, len, 1);
	BUG_ON(rc < 0);
	BUG_ON(rc < len);
}

int receive_skb(struct conn *src_in_l, struct sk_buff *skb)
{
	struct skb_procstate *ps = skb_pstate(skb);
	struct data_buf_item *item = &(ps->funcstate.rcv.dbi);

	if (unlikely(unlikely(src_in_l->data_buf.totalsize + skb->len >
			(1 << 30)) || unlikely(src_in_l->data_buf.overhead >
			(1 << 30))))
		return 1;

	item->type = TYPE_SKB;
	item->buf = skb->data;
	item->datalen = skb->len;
	item->buflen = item->datalen;
	list_add_tail(&(item->buf_list), &(src_in_l->data_buf.items));

	src_in_l->data_buf.read_remaining += item->datalen;
	src_in_l->data_buf.totalsize += item->datalen;
	src_in_l->data_buf.overhead += sizeof(struct sk_buff);

	return 0;
}

void wake_sender(struct conn *cn)
{
	unreserve_sock_buffer(cn);

	spin_lock_bh(&(cn->rcv_lock));
	switch (cn->sourcetype) {
	case SOURCE_NONE:
		spin_unlock_bh(&(cn->rcv_lock));
		parse(cn->reversedir, 0);
		break;
	case SOURCE_SOCK:
		wake_up_interruptible(&(cn->source.sock.wait));
		spin_unlock_bh(&(cn->rcv_lock));
		break;
	case SOURCE_IN:
		drain_ooo_queue(cn);
		spin_unlock_bh(&(cn->rcv_lock));
		get_window(cn, 0, 0, 0);
		break;
	default:
		BUG();
	}
}

void flush_buf(struct conn *cn)
{
	int rc;
	int sent = 0;
	spin_lock_bh(&(cn->rcv_lock));

	switch (cn->targettype) {
	case TARGET_UNCONNECTED:
		spin_unlock_bh(&(cn->rcv_lock));
		parse(cn, 0);
		break;
	case TARGET_SOCK:
		if (cn->sourcetype != SOURCE_SOCK ||
				cn->source.sock.delay_flush == 0 ||
				cn->data_buf.totalsize +
				cn->data_buf.overhead -
				cn->data_buf.cpacket_buffer >=
				BUFFERLIMIT_SOCK_SOCK/2)
			wake_up_interruptible(&(cn->target.sock.wait));
		spin_unlock_bh(&(cn->rcv_lock));
		break;
	case TARGET_OUT:
		rc = flush_out(cn, 0, 0);
		spin_unlock_bh(&(cn->rcv_lock));
		sent = (rc == RC_FLUSH_CONN_OUT_OK_SENT);
		break;
	case TARGET_DISCARD:
		databuf_ackdiscard(cn);
		spin_unlock_bh(&(cn->rcv_lock));
		sent = 1;
		break;
	default:
		BUG();
	}

	refresh_conn_credits(cn, 0, 0);

	if (sent) {
		wake_sender(cn);
	}
}

void __init forward_init(void)
{
	data_buf_item_slab = kmem_cache_create("cor_data_buf_item",
			sizeof(struct data_buf_item), 8, 0, 0);
}

MODULE_LICENSE("GPL");
