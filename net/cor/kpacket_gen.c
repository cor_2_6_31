/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <asm/byteorder.h>

#include "cor.h"

/* not sent over the network - internal meaning only */
#define MSGTYPE_PONG 1
#define MSGTYPE_ACK 2
#define MSGTYPE_ACK_CONN 3
#define MSGTYPE_CONNECT 4
#define MSGTYPE_CONNECT_SUCCESS 5
#define MSGTYPE_RESET_CONN 6
#define MSGTYPE_CONNDATA 7
#define MSGTYPE_CONNID_UNKNOWN 8
#define MSGTYPE_SET_MAX_CMSG_DELAY 9

#define MSGTYPE_PONG_TIMEENQUEUED 1
#define MSGTYPE_PONG_RESPDELAY 2

struct control_msg_out{
	struct list_head lh; /* either neighbor or control_retrans_packet */
	struct neighbor *nb;

	struct kref ref;

	unsigned long timeout;

	__u32 length;

	__u8 type;
	union{
		struct{

			__u32 cookie;
			__u8 type;

			union {
				ktime_t time_enqueued;
				__u32 respdelay;
			} delaycomp;
		}pong;

		struct{
			__u32 seqno;
		}ack;

		struct{
			struct conn *src_in;
			struct list_head conn_acks;
			__u32 conn_id;
			__u32 seqno;
			__u32 seqno_ooo;
			__u32 length;

			__u16 decaytime;
			__u8 decaytime_seqno;

			__u8 flags;

			__u32 ack_seqno;
		}ack_conn;

		struct{
			__u32 conn_id;
			__u32 init_seqno;
			struct conn *src_in;
		}connect;

		struct{
			__u32 rcvd_conn_id;
			__u32 gen_conn_id;
			__u32 init_seqno;
			struct conn *src_in;
		}connect_success;

		struct{
			struct htab_entry htab_entry;
			__u32 conn_id_reset;
			__u32 conn_id_unknown;
		}reset_connidunknown;

		struct{
			__u32 conn_id;
			__u32 seqno;
			char *data_orig;
			char *data;
			__u32 datalen;
		}conn_data;

		struct{
			__u32 delay;
		}set_max_cmsg_delay;
	}msg;
};

struct control_retrans {
	struct kref ref;

	struct neighbor *nb;
	__u32 seqno;

	unsigned long timeout;

	struct list_head msgs;

	struct htab_entry htab_entry;
	struct list_head timeout_list;
};

struct unknownconnid_matchparam {
	struct neighbor *nb;
	__u32 conn_id;
};

struct retransmit_matchparam {
	struct neighbor *nb;
	__u32 seqno;
};


struct kmem_cache *controlmsg_slab;
struct kmem_cache *controlretrans_slab;

static struct htable retransmits;

DEFINE_SPINLOCK(unknown_connids_lock);
static struct htable unknown_connids;

atomic_t cmcnt = ATOMIC_INIT(0);


static void add_control_msg(struct control_msg_out *msg, int retrans);

static void try_merge_ackconns(struct conn *src_in_l,
		struct control_msg_out *cm);

static void mergeadd_ackconn(struct conn *src_in_l, struct control_msg_out *cm);


static __u32 ucm_to_key(struct unknownconnid_matchparam *ucm)
{
	return ((__u32)((long) ucm->nb)) ^ ucm->conn_id;
}

static __u32 rm_to_key(struct retransmit_matchparam *rm)
{
	return ((__u32)((long) rm->nb)) ^ rm->seqno;
}

static inline int isurgent(struct control_msg_out *cm)
{
	if (unlikely(cm->type == MSGTYPE_PONG || cm->type == MSGTYPE_ACK))
		return 1;
	return 0;
}

static struct control_msg_out *__alloc_control_msg(void)
{
	struct control_msg_out *cm = kmem_cache_alloc(controlmsg_slab,
			GFP_ATOMIC);
	if (unlikely(cm == 0))
		return 0;
	memset(cm, 0, sizeof(struct control_msg_out));
	cm->lh.next = LIST_POISON1;
	cm->lh.prev = LIST_POISON2;
	kref_init(&(cm->ref));
	return cm;
}

static int calc_limit(int limit, int priority)
{
	if (priority == ACM_PRIORITY_LOW)
		return (limit+2)/3;
	else if (priority == ACM_PRIORITY_MED)
		return (limit * 2 + 1)/3;
	else if (priority == ACM_PRIORITY_HIGH)
		return limit;
	else
		BUG();
}

int may_alloc_control_msg(struct neighbor *nb, int priority)
{
	long packets1 = atomic_read(&(nb->cmcnt));
	long packets2 = atomic_read(&(cmcnt));

	BUG_ON(packets1 < 0);
	BUG_ON(packets2 < 0);

	if (packets1 < calc_limit(GUARANTEED_CMSGS_PER_NEIGH, priority))
		return 1;

	if (unlikely(unlikely(packets2 >= calc_limit(MAX_CMSGS_PER_NEIGH,
			priority)) || unlikely(packets1 >= (
			calc_limit(MAX_CMSGS_PER_NEIGH, priority) *
			(MAX_CMSGS - packets2) / MAX_CMSGS))))
		return 0;
	return 1;
}

static struct control_msg_out *_alloc_control_msg(struct neighbor *nb,
		int priority, int urgent)
{
	struct control_msg_out *cm = 0;

	BUG_ON(nb == 0);

	if (urgent == 0) {
		long packets1 = atomic_inc_return(&(nb->cmcnt));
		long packets2 = atomic_inc_return(&(cmcnt));

		BUG_ON(packets1 <= 0);
		BUG_ON(packets2 <= 0);

		if (packets1 <= calc_limit(GUARANTEED_CMSGS_PER_NEIGH,
				priority))
			goto alloc;

		if (unlikely(unlikely(packets2 > calc_limit(MAX_CMSGS_PER_NEIGH,
				priority)) || unlikely(packets1 > (
				calc_limit(MAX_CMSGS_PER_NEIGH, priority) *
				(MAX_CMSGS - packets2) / MAX_CMSGS))))
			goto full;
	}

alloc:
	cm = __alloc_control_msg();
	if (unlikely(cm == 0))
		goto full;
	cm->nb = nb;

	if (0) {
full:
		if (urgent == 0) {
			atomic_dec(&(nb->cmcnt));
			atomic_dec(&(cmcnt));
		}
	}
	return cm;
}

struct control_msg_out *alloc_control_msg(struct neighbor *nb, int priority)
{
	return _alloc_control_msg(nb, priority, 0);
}

static void cmsg_kref_free(struct kref *ref)
{
	struct control_msg_out *cm = container_of(ref, struct control_msg_out,
			ref);
	kmem_cache_free(controlmsg_slab, cm);
}

void free_control_msg(struct control_msg_out *cm)
{
	if (isurgent(cm) == 0) {
		atomic_dec(&(cm->nb->cmcnt));
		atomic_dec(&(cmcnt));
	}

	if (cm->type == MSGTYPE_ACK_CONN) {
		struct conn *trgt_out = cm->msg.ack_conn.src_in->reversedir;
		BUG_ON(cm->msg.ack_conn.src_in == 0);
		spin_lock_bh(&(trgt_out->rcv_lock));
		BUG_ON(trgt_out->targettype != TARGET_OUT);
		if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_CREDITS) != 0 &&
				trgt_out->target.out.decaytime_send_allowed !=
				0) {
			trgt_out->target.out.decaytime_send_allowed = 1;
			spin_unlock_bh(&(trgt_out->rcv_lock));
			refresh_conn_credits(trgt_out, 0, 0);
		} else {
			spin_unlock_bh(&(trgt_out->rcv_lock));
		}

		kref_put(&(cm->msg.ack_conn.src_in->ref), free_conn);
		cm->msg.ack_conn.src_in = 0;
	} else if (cm->type == MSGTYPE_CONNECT) {
		BUG_ON(cm->msg.connect.src_in == 0);
		kref_put(&(cm->msg.connect.src_in->ref), free_conn);
		cm->msg.connect.src_in = 0;
	} else if (cm->type == MSGTYPE_CONNECT_SUCCESS) {
		BUG_ON(cm->msg.connect_success.src_in == 0);
		kref_put(&(cm->msg.connect_success.src_in->ref), free_conn);
		cm->msg.connect_success.src_in = 0;
	} else if (cm->type == MSGTYPE_RESET_CONN ||
			cm->type == MSGTYPE_CONNID_UNKNOWN) {
		struct unknownconnid_matchparam ucm;

		ucm.nb = cm->nb;
		ucm.conn_id = cm->msg.reset_connidunknown.conn_id_unknown;

		htable_delete(&unknown_connids, ucm_to_key(&ucm), &ucm,
				cmsg_kref_free);
	}

	kref_put(&(cm->ref), cmsg_kref_free);
}

static void free_control_retrans(struct kref *ref)
{
	struct control_retrans *cr = container_of(ref, struct control_retrans,
			ref);

	while (list_empty(&(cr->msgs)) == 0) {
		struct control_msg_out *cm = container_of(cr->msgs.next,
					struct control_msg_out, lh);
		list_del(&(cm->lh));
		free_control_msg(cm);
	}

	kmem_cache_free(controlretrans_slab, cr);
}


static void set_retrans_timeout(struct control_retrans *cr, struct neighbor *nb)
{
	cr->timeout = jiffies + usecs_to_jiffies(100000 +
			((__u32) atomic_read(&(nb->latency))) * 2 +
			((__u32) atomic_read(&(nb->max_remote_cmsg_delay))));
}

static void remove_connack_oooflag_ifold(struct conn *src_in_l,
		struct control_msg_out *cm)
{
	if (ooolen(cm->msg.ack_conn.flags) != 0 && seqno_before_eq(
			cm->msg.ack_conn.seqno_ooo +
			cm->msg.ack_conn.length,
			src_in_l->source.in.next_seqno)) {
		cm->msg.ack_conn.length = 0;
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
			(~KP_ACK_CONN_FLAGS_OOO));
	}
}

static int ackconn_prepare_readd(struct conn *cn_l,
		struct control_msg_out *cm)
{
	if (unlikely(unlikely(cn_l->sourcetype != SOURCE_IN) ||
			unlikely(cn_l->source.in.nb != cm->nb) ||
			unlikely(cn_l->reversedir->target.out.conn_id !=
				cm->msg.ack_conn.conn_id) ||
			unlikely(cn_l->isreset != 0)))
		return 0;

	remove_connack_oooflag_ifold(cn_l, cm);

	if (cm->msg.ack_conn.ack_seqno != cn_l->source.in.ack_seqno)
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
				(~KP_ACK_CONN_FLAGS_SEQNO) &
				(~KP_ACK_CONN_FLAGS_WINDOW));

	if (cm->msg.ack_conn.flags == 0)
		return 0;

	cm->length = 6 + ack_conn_len(cm->msg.ack_conn.flags);

	return 1;
}

static void readd_control_retrans(struct control_retrans *cr)
{
	while (list_empty(&(cr->msgs)) == 0) {
		struct control_msg_out *cm = container_of(cr->msgs.next,
				struct control_msg_out, lh);
		list_del(&(cm->lh));
		if (cm->type == MSGTYPE_ACK_CONN) {
			struct conn *cn_l = cm->msg.ack_conn.src_in;
			spin_lock_bh(&(cn_l->rcv_lock));
			if (unlikely(ackconn_prepare_readd(cn_l, cm) == 0)) {
				free_control_msg(cm);
			} else {
				mergeadd_ackconn(cn_l, cm);
			}

			spin_unlock_bh(&(cn_l->rcv_lock));
		} else {
			add_control_msg(cm, 1);
		}
	}
}

void retransmit_taskfunc(unsigned long arg)
{
	struct neighbor *nb = (struct neighbor *) arg;
	unsigned long iflags;

	int nbstate;
	int nbput = 0;

	spin_lock_irqsave(&(nb->state_lock), iflags);
	nbstate = nb->state;
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	while (1) {
		struct control_retrans *cr = 0;
		struct retransmit_matchparam rm;

		spin_lock_bh(&(nb->retrans_lock));

		if (list_empty(&(nb->retrans_list))) {
			nb->retrans_timer_running = 0;
			nbput = 1;
			break;
		}

		cr = container_of(nb->retrans_list.next,
					struct control_retrans, timeout_list);

		BUG_ON(cr->nb != nb);

		rm.seqno = cr->seqno;
		rm.nb = nb;

		list_del(&(cr->timeout_list));

		if (unlikely(nbstate == NEIGHBOR_STATE_KILLED)) {
			spin_unlock_bh(&(nb->retrans_lock));

			htable_delete(&retransmits, rm_to_key(&rm), &rm,
					free_control_retrans);
			kref_put(&(cr->ref), free_control_retrans);
			continue;
		}

		if (time_after(cr->timeout, jiffies)) {
			list_add(&(cr->timeout_list), &(nb->retrans_list));
			mod_timer(&(nb->retrans_timer_conn), cr->timeout);
			break;
		}

		if (unlikely(htable_delete(&retransmits, rm_to_key(&rm), &rm,
				free_control_retrans)))
			BUG();

		spin_unlock_bh(&(nb->retrans_lock));

		readd_control_retrans(cr);

		kref_put(&(cr->ref), free_control_retrans);
	}

	spin_unlock_bh(&(nb->retrans_lock));

	if (nbput)
		kref_put(&(nb->ref), neighbor_free);
}

void retransmit_timerfunc(unsigned long arg)
{
	struct neighbor *nb = (struct neighbor *) arg;
	tasklet_schedule(&(nb->retrans_task));
}

static void schedule_retransmit(struct control_retrans *cr, struct neighbor *nb)
{
	struct retransmit_matchparam rm;
	int first;

	rm.seqno = cr->seqno;
	rm.nb = nb;

	set_retrans_timeout(cr, nb);

	spin_lock_bh(&(nb->retrans_lock));
	htable_insert(&retransmits, (char *) cr, rm_to_key(&rm));
	first = list_empty(&(nb->retrans_list));
	list_add_tail(&(cr->timeout_list), &(nb->retrans_list));

	if (first && nb->retrans_timer_running == 0) {
		mod_timer(&(nb->retrans_timer), cr->timeout);
		nb->retrans_timer_running = 1;
		kref_get(&(nb->ref));
	}

	spin_unlock_bh(&(nb->retrans_lock));
}

void kern_ack_rcvd(struct neighbor *nb, __u32 seqno)
{
	struct control_retrans *cr = 0;
	struct retransmit_matchparam rm;

	rm.seqno = seqno;
	rm.nb = nb;

	spin_lock_bh(&(nb->retrans_lock));

	cr = (struct control_retrans *) htable_get(&retransmits, rm_to_key(&rm),
			&rm);

	if (cr == 0) {
		printk(KERN_ERR "bogus/duplicate ack received");
		goto out;
	}

	if (unlikely(htable_delete(&retransmits, rm_to_key(&rm), &rm,
			free_control_retrans)))
		 BUG();

	BUG_ON(cr->nb != nb);

	list_del(&(cr->timeout_list));

out:
	spin_unlock_bh(&(nb->retrans_lock));

	if (cr != 0) {
		kref_put(&(cr->ref), free_control_retrans); /* htable_get */
		kref_put(&(cr->ref), free_control_retrans); /* list */
	}
}

static void padding(struct sk_buff *skb, int length)
{
	char *dst;
	if (length <= 0)
		return;
	dst = skb_put(skb, length);
	BUG_ON(dst == 0);
	memset(dst, KP_PADDING, length);
}

static int add_ack(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = KP_ACK;
	put_u32(dst + 1, cm->msg.ack.seqno, 1);

	atomic_dec(&(cm->nb->ucmcnt));
	free_control_msg(cm);

	return 5;
}

static int add_ack_conn(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;
	int offset = 0;

	if (unlikely(spaceleft < cm->length))
		return 0;

	dst = skb_put(skb, cm->length);
	BUG_ON(dst == 0);

	dst[offset] = KP_ACK_CONN;
	offset++;
	put_u32(dst + offset, cm->msg.ack_conn.conn_id, 1);
	offset += 4;
	dst[offset] = cm->msg.ack_conn.flags;
	offset++;

	if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) != 0) {
		put_u32(dst + offset, cm->msg.ack_conn.seqno, 1);
		offset += 4;

		if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_WINDOW) != 0) {
			BUG_ON(cm->msg.ack_conn.src_in == 0);
			dst[offset] = get_window(cm->msg.ack_conn.src_in,
					cm->nb, cm->msg.ack_conn.conn_id, 1);
			offset++;
		}
	}

	if (ooolen(cm->msg.ack_conn.flags) != 0) {
		put_u32(dst + offset, cm->msg.ack_conn.seqno_ooo, 1);
		offset += 4;
		if (ooolen(cm->msg.ack_conn.flags) == 1) {
			BUG_ON(cm->msg.ack_conn.length > 255);
			dst[offset] = cm->msg.ack_conn.length;
			offset += 1;
		} else if (ooolen(cm->msg.ack_conn.flags) == 2) {
			BUG_ON(cm->msg.ack_conn.length <= 255);
			BUG_ON(cm->msg.ack_conn.length > 65535);
			put_u16(dst + offset, cm->msg.ack_conn.length, 1);
			offset += 2;
		} else if (ooolen(cm->msg.ack_conn.flags) == 4) {
			BUG_ON(cm->msg.ack_conn.length <= 65535);
			put_u32(dst + offset, cm->msg.ack_conn.length, 1);
			offset += 4;
		} else {
			BUG();
		}
	}

	if ((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_CREDITS) != 0) {
		__u16 value = cm->msg.ack_conn.decaytime + (
				cm->msg.ack_conn.decaytime_seqno << 10);

		BUG_ON(cm->msg.ack_conn.decaytime >= 1024);
		BUG_ON(cm->msg.ack_conn.decaytime_seqno >= 64);

		put_u16(dst + offset, value, 1);
		offset += 2;
	}

	list_add_tail(&(cm->lh), &(cr->msgs));

	BUG_ON(offset != cm->length);
	return offset;
}

static int add_ping(struct sk_buff *skb, __u32 cookie,
		int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = KP_PING;
	put_u32(dst + 1, cookie, 0);

	return 5;
}

static int add_pong(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 9))
		return 0;

	if (cm->msg.pong.type == MSGTYPE_PONG_TIMEENQUEUED) {
		__s64 now = ktime_to_ns(ktime_get());
		__s64 enq = ktime_to_ns(cm->msg.pong.delaycomp.time_enqueued);
		__s64 respdelay = (now - enq + 500) / 1000;
		if (unlikely(respdelay >= (1LL << 32)))
			respdelay = (1LL << 32) - 1;
		cm->msg.pong.type = MSGTYPE_PONG_RESPDELAY;
		cm->msg.pong.delaycomp.respdelay = (__u32) respdelay;
	}

	BUG_ON(cm->msg.pong.type != MSGTYPE_PONG_RESPDELAY);

	dst = skb_put(skb, 9);
	BUG_ON(dst == 0);

	dst[0] = KP_PONG;
	put_u32(dst + 1, cm->msg.pong.cookie, 0);
	put_u32(dst + 5, cm->msg.pong.delaycomp.respdelay, 1);


	atomic_dec(&(cm->nb->ucmcnt));
	list_add_tail(&(cm->lh), &(cr->msgs));

	return 9;
}

static __u16 get_credits(struct conn *sconn)
{
	__u16 ret;
	spin_lock_bh(&(sconn->reversedir->rcv_lock));
	BUG_ON(sconn->reversedir->targettype != TARGET_OUT);

	BUG_ON(sconn->reversedir->target.out.decaytime_last >= 1024);
	BUG_ON(sconn->reversedir->target.out.decaytime_seqno >= 64);
	ret = sconn->reversedir->target.out.decaytime_last + (
				sconn->reversedir->target.out.decaytime_seqno <<
				10);
	spin_unlock_bh(&(sconn->reversedir->rcv_lock));

	return ret;
}

static int add_connect(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 12))
		return 0;

	dst = skb_put(skb, 12);
	BUG_ON(dst == 0);

	dst[0] = KP_CONNECT;
	put_u32(dst + 1, cm->msg.connect.conn_id, 1);
	put_u32(dst + 5, cm->msg.connect.init_seqno, 1);
	BUG_ON(cm->msg.connect.src_in == 0);
	dst[9] = get_window(cm->msg.connect.src_in, cm->nb, 0, 1);
	put_u16(dst + 10, get_credits(cm->msg.connect.src_in), 1);

	list_add_tail(&(cm->lh), &(cr->msgs));

	return 12;
}

static int add_connect_success(struct sk_buff *skb, struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 16))
		return 0;

	dst = skb_put(skb, 16);
	BUG_ON(dst == 0);

	dst[0] = KP_CONNECT_SUCCESS;
	put_u32(dst + 1, cm->msg.connect_success.rcvd_conn_id, 1);
	put_u32(dst + 5, cm->msg.connect_success.gen_conn_id, 1);
	put_u32(dst + 9, cm->msg.connect_success.init_seqno, 1);
	BUG_ON(cm->msg.connect_success.src_in == 0);
	dst[13] = get_window(cm->msg.connect_success.src_in, cm->nb,
			cm->msg.connect_success.rcvd_conn_id, 1);
	put_u16(dst + 14, get_credits(cm->msg.connect_success.src_in), 1);

	list_add_tail(&(cm->lh), &(cr->msgs));

	return 16;
}

static int add_reset_conn(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = KP_RESET_CONN;
	put_u32(dst + 1, cm->msg.reset_connidunknown.conn_id_reset, 1);

	list_add_tail(&(cm->lh), &(cr->msgs));

	return 5;
}

static int add_conndata(struct sk_buff *skb,  struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft,
		struct control_msg_out **split_conndata, __u32 *sc_sendlen)
{
	char *dst;

	int totallen = cm->msg.conn_data.datalen + 11;
	int putlen = min(totallen, spaceleft);
	int dataputlen = putlen - 11;

	BUG_ON(split_conndata == 0);
	BUG_ON(sc_sendlen == 0);

	if (dataputlen < 1 || (spaceleft < 25 && spaceleft < totallen))
		return 0;

	dst = skb_put(skb, putlen);
	BUG_ON(dst == 0);

	dst[0] = KP_CONN_DATA;
	put_u32(dst + 1, cm->msg.conn_data.conn_id, 1);
	put_u32(dst + 5, cm->msg.conn_data.seqno, 1);
	put_u16(dst + 9, dataputlen, 1);

	memcpy(dst + 11, cm->msg.conn_data.data, dataputlen);

	if (cm->msg.conn_data.datalen == dataputlen) {
		list_add_tail(&(cm->lh), &(cr->msgs));
	} else {
		*split_conndata = cm;
		*sc_sendlen = dataputlen;
	}

	return putlen;
}

static int add_connid_unknown(struct sk_buff *skb, struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = KP_CONNID_UNKNOWN;
	put_u32(dst + 1, cm->msg.reset_connidunknown.conn_id_unknown, 1);

	list_add_tail(&(cm->lh), &(cr->msgs));

	return 5;
}

static int add_set_max_cmsg_dly(struct sk_buff *skb, struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft)
{
	char *dst;

	if (unlikely(spaceleft < 5))
		return 0;

	dst = skb_put(skb, 5);
	BUG_ON(dst == 0);

	dst[0] = KP_SET_MAX_CMSG_DELAY;
	put_u32(dst + 1, cm->msg.set_max_cmsg_delay.delay, 1);

	list_add_tail(&(cm->lh), &(cr->msgs));

	return 5;
}

static int add_message(struct sk_buff *skb, struct control_retrans *cr,
		struct control_msg_out *cm, int spaceleft,
		struct control_msg_out **split_conndata, __u32 *sc_sendlen)
{
	BUG_ON(split_conndata != 0 && *split_conndata != 0);
	BUG_ON(sc_sendlen != 0 && *sc_sendlen != 0);

	switch (cm->type) {
	case MSGTYPE_ACK:
		return add_ack(skb, cr, cm, spaceleft);
	case MSGTYPE_ACK_CONN:
		return add_ack_conn(skb, cr, cm, spaceleft);
	case MSGTYPE_PONG:
		return add_pong(skb, cr, cm, spaceleft);
	case MSGTYPE_CONNECT:
		return add_connect(skb, cr, cm, spaceleft);
	case MSGTYPE_CONNECT_SUCCESS:
		return add_connect_success(skb, cr, cm, spaceleft);
	case MSGTYPE_RESET_CONN:
		return add_reset_conn(skb, cr, cm, spaceleft);
	case MSGTYPE_CONNDATA:
		return add_conndata(skb, cr, cm, spaceleft, split_conndata,
				sc_sendlen);
	case MSGTYPE_CONNID_UNKNOWN:
		return add_connid_unknown(skb, cr, cm, spaceleft);
	case MSGTYPE_SET_MAX_CMSG_DELAY:
		return add_set_max_cmsg_dly(skb, cr, cm, spaceleft);
	default:
		BUG();
	}
	BUG();
	return 0;
}

static void requeue_message(struct control_msg_out *cm)
{
	if (cm->type == MSGTYPE_ACK_CONN) {
		struct conn *cn_l = cm->msg.ack_conn.src_in;

		spin_lock_bh(&(cn_l->rcv_lock));
		if (unlikely(ackconn_prepare_readd(cn_l, cm) == 0)) {
			free_control_msg(cm);
		} else {
			spin_lock_bh(&(cm->nb->cmsg_lock));

			list_add(&(cm->lh), &(cm->nb->control_msgs_out));
			cm->nb->cmlength += cm->length;

			list_add(&(cm->msg.ack_conn.conn_acks),
					&(cn_l->source.in.acks_pending));
			try_merge_ackconns(cn_l, cm);

			spin_unlock_bh(&(cm->nb->cmsg_lock));
		}
		spin_unlock_bh(&(cn_l->rcv_lock));
		return;
	}

	if (isurgent(cm)) {
		list_add(&(cm->lh), &(cm->nb->ucontrol_msgs_out));
		cm->nb->ucmlength += cm->length;
	} else {
		list_add(&(cm->lh), &(cm->nb->control_msgs_out));
		cm->nb->cmlength += cm->length;
	}
}

static struct control_msg_out *dequeue_message(struct neighbor *nb,
		int urgentonly)
{
	struct control_msg_out *cm;

	if (list_empty(&(nb->ucontrol_msgs_out)) == 0) {
		cm = container_of(nb->ucontrol_msgs_out.next,
				struct control_msg_out, lh);
		nb->ucmlength -= cm->length;
	} else if (urgentonly) {
		return 0;
	} else {
		if (list_empty(&(nb->control_msgs_out)))
			return 0;

		cm = container_of(nb->control_msgs_out.next,
				struct control_msg_out, lh);
		nb->cmlength -= cm->length;
	}

	BUG_ON(cm->nb != nb);

 	list_del(&(cm->lh));
	if (cm->type == MSGTYPE_ACK_CONN)
		list_del(&(cm->msg.ack_conn.conn_acks));

	return cm;
}

static __u32 __send_messages(struct neighbor *nb, struct sk_buff *skb,
		struct control_retrans *cr, int spaceleft, int urgentonly,
		struct control_msg_out **split_conndata, __u32 *sc_sendlen)
{
	__u32 length = 0;
	while (1) {
		int rc;
		struct control_msg_out *cm;

		spin_lock_bh(&(nb->cmsg_lock));
		cm = dequeue_message(nb, urgentonly);
		spin_unlock_bh(&(nb->cmsg_lock));

		if (cm == 0)
			break;

		rc = add_message(skb, cr, cm, spaceleft - length,
				split_conndata, sc_sendlen);
		if (rc == 0) {
			requeue_message(cm);
			break;
		}

		length += rc;
	}

	return length;
}

static __u32 __send_messages_smcd(struct neighbor *nb, struct sk_buff *skb,
		struct control_retrans *cr, int spaceleft)
{
	struct control_msg_out *cm;
	int rc;

	cm = alloc_control_msg(nb, ACM_PRIORITY_LOW);

	if (unlikely(cm == 0))
		return 0;

	cm->type = MSGTYPE_SET_MAX_CMSG_DELAY;
	cm->msg.set_max_cmsg_delay.delay = CMSG_INTERVAL_MS * 10;
	cm->length = 5;

	rc = add_message(skb, cr, cm, spaceleft, 0, 0);

	nb->max_cmsg_delay_sent = 1;

	return rc;
}

static int _send_messages(struct neighbor *nb, struct sk_buff *skb, int ping,
		struct control_retrans *cr, int spaceleft, int urgentonly)
{
	int rc;
	__u32 length = 0;
	__u32 pinglen = 0;
	__u32 pingcookie = 0;
	unsigned long last_ping_time;
	struct control_msg_out *split_conndata = 0;
	__u32 sc_sendlen = 0;

	spin_lock_bh(&(nb->cmsg_lock));

	if (ping != 0) {
		int rc;
		pingcookie = add_ping_req(nb, &last_ping_time);
		rc = add_ping(skb, pingcookie, spaceleft - length);
		BUG_ON(rc == 0);
		pinglen = rc;
		length += rc;
	}

	if (likely(urgentonly == 0) && unlikely(nb->max_cmsg_delay_sent == 0))
		length += __send_messages_smcd(nb, skb, cr, spaceleft - length);

	spin_unlock_bh(&(nb->cmsg_lock));

	length += __send_messages(nb, skb, cr, spaceleft - length, urgentonly,
			&split_conndata, &sc_sendlen);

	BUG_ON(length > spaceleft);

	if (likely(ping != 2) && unlikely(length == pinglen)) {
		unadd_ping_req(nb, pingcookie, last_ping_time);
		kfree_skb(skb);

		BUG_ON(list_empty(&(cr->msgs)) == 0);
		kref_put(&(cr->ref), free_control_retrans);

		atomic_sub(1, &(nb->kpacket_seqno));
		return 0;
	}

	padding(skb, spaceleft - length);

	rc = dev_queue_xmit(skb);

	if (rc != 0) {
		unadd_ping_req(nb, pingcookie, last_ping_time);

		while (list_empty(&(cr->msgs)) == 0) {
			struct control_msg_out *cm = container_of(cr->msgs.prev,
					struct control_msg_out, lh);
			list_del(&(cm->lh));
			add_control_msg(cm, 1);
		}

		if (split_conndata != 0) {
			add_control_msg(split_conndata, 1);
		}

		kref_put(&(cr->ref), free_control_retrans);
	} else {
		struct list_head *curr = cr->msgs.next;

		while(curr != &(cr->msgs)) {
			struct control_msg_out *cm = container_of(curr,
					struct control_msg_out, lh);

			curr = curr->next;

			if (cm->type == MSGTYPE_CONNDATA) {
				list_del(&(cm->lh));
				kfree(cm->msg.conn_data.data_orig);
				free_control_msg(cm);
			}
		}

		if (split_conndata != 0) {
			BUG_ON(sc_sendlen == 0);
			BUG_ON(sc_sendlen >=
					split_conndata->msg.conn_data.datalen);

			split_conndata->msg.conn_data.data += sc_sendlen;
			split_conndata->msg.conn_data.datalen -= sc_sendlen;

			send_conndata(split_conndata,
					split_conndata->msg.conn_data.conn_id,
					split_conndata->msg.conn_data.seqno,
					split_conndata->msg.conn_data.data_orig,
					split_conndata->msg.conn_data.data,
					split_conndata->msg.conn_data.datalen);
		}


		if (list_empty(&(cr->msgs)))
			kref_put(&(cr->ref), free_control_retrans);
		else
			schedule_retransmit(cr, nb);
	}

	return rc;
}

static __u32 get_total_messages_length(struct neighbor *nb, int ping,
		int urgentonly)
{
	__u32 length = nb->ucmlength;

	if (likely(urgentonly == 0)) {
		length += nb->cmlength;

		if (unlikely(nb->max_cmsg_delay_sent == 0))
			length += 5;
	}
	if (ping == 2 || (length > 0 && ping != 0))
		length += 5;

	return length;
}

static int reset_timeouted_conn_needed(struct neighbor *nb,
		struct conn *src_in_l)
{
	if (unlikely(unlikely(src_in_l->sourcetype != SOURCE_IN) ||
			unlikely(src_in_l->source.in.nb != nb) ||
			unlikely(src_in_l->isreset != 0)))
		return 0;
	else if (likely(time_after(src_in_l->source.in.jiffies_last_act +
			CONN_ACTIVITY_UPDATEINTERVAL_SEC * HZ +
			CONN_INACTIVITY_TIMEOUT_SEC * HZ, jiffies)))
		return 0;

	return 1;
}

static int reset_timeouted_conn(struct neighbor *nb, struct conn *src_in)
{
	int resetted = 0;

	if (src_in->is_client) {
		spin_lock_bh(&(src_in->rcv_lock));
		spin_lock_bh(&(src_in->reversedir->rcv_lock));
	} else {
		spin_lock_bh(&(src_in->reversedir->rcv_lock));
		spin_lock_bh(&(src_in->rcv_lock));
	}

	resetted = reset_timeouted_conn_needed(nb, src_in);
	if (unlikely(resetted == 0))
		goto unlock;

	resetted = (send_reset_conn(nb, src_in->reversedir->target.out.conn_id,
			src_in->source.in.conn_id, 1) == 0);
	if (unlikely(resetted == 0))
		goto unlock;


	BUG_ON(src_in->reversedir->isreset != 0);
	src_in->reversedir->isreset = 1;

unlock:
	if (src_in->is_client) {
		spin_unlock_bh(&(src_in->rcv_lock));
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
	} else {
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
		spin_unlock_bh(&(src_in->rcv_lock));
	}

	if (resetted)
		reset_conn(src_in);

	return resetted;
}

static void reset_timeouted_conns(struct neighbor *nb)
{
	int i;
	for (i=0;i<10000;i++) {
		unsigned long iflags;
		struct conn *src_in;

		int resetted = 1;

		spin_lock_irqsave(&(nb->conn_list_lock), iflags);

		if (list_empty(&(nb->rcv_conn_list))) {
			spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);
			break;
		}

		src_in = container_of(nb->rcv_conn_list.next, struct conn,
				source.in.nb_list);
		kref_get(&(src_in->ref));

		spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);


		spin_lock_bh(&(src_in->rcv_lock));
		resetted = reset_timeouted_conn_needed(nb, src_in);
		spin_unlock_bh(&(src_in->rcv_lock));
		if (likely(resetted == 0))
			goto put;

		resetted = reset_timeouted_conn(nb, src_in);

put:
		kref_put(&(src_in->ref), free_conn);

		if (likely(resetted == 0))
			break;
	}
}

int send_messages(struct neighbor *nb, int resume)
{
	int i;
	int rc = 0;
	int ping;
	int targetmss = mss(nb);

	int nbstate = get_neigh_state(nb);
	int urgentonly = (nbstate != NEIGHBOR_STATE_ACTIVE);

	if (likely(urgentonly == 0))
		reset_timeouted_conns(nb);

	spin_lock_bh(&(nb->send_cmsg_lock));
	spin_lock_bh(&(nb->cmsg_lock));

	ping = time_to_send_ping(nb);

	for (i=0;1;i++) {
		__u32 length;

		__u32 seqno;
		struct sk_buff *skb;
		struct control_retrans *cr;

		BUG_ON(list_empty(&(nb->control_msgs_out)) &&
				(nb->cmlength != 0));
		BUG_ON((list_empty(&(nb->control_msgs_out)) == 0) &&
				(nb->cmlength == 0));
		BUG_ON(list_empty(&(nb->ucontrol_msgs_out)) &&
				(nb->ucmlength != 0));
		BUG_ON((list_empty(&(nb->ucontrol_msgs_out)) == 0) &&
				(nb->ucmlength == 0));
		BUG_ON(nb->cmlength < 0);
		BUG_ON(nb->ucmlength < 0);

		length = get_total_messages_length(nb, ping, urgentonly);

		if (length == 0)
			break;

		if (length < targetmss && i > 0)
			break;

		seqno = atomic_add_return(1, &(nb->kpacket_seqno));

		if (length > targetmss)
			length = targetmss;

		spin_unlock_bh(&(nb->cmsg_lock));
		skb = create_packet(nb, length, GFP_ATOMIC, 0, seqno);
		if (unlikely(skb == 0)) {
			printk(KERN_ERR "cor: send_messages: cannot allocate "
					"skb (out of memory?)");
			goto oom;
		}

		cr = kmem_cache_alloc(controlretrans_slab, GFP_ATOMIC);
		if (unlikely(cr == 0)) {
			kfree_skb(skb);
			printk(KERN_ERR "cor: send_messages: cannot allocate "
					"control_retrans (out of memory?)");
			goto oom;
		}
		memset(cr, 0, sizeof(struct control_retrans));
		kref_init(&(cr->ref));
		cr->nb = nb;
		cr->seqno = seqno;
		INIT_LIST_HEAD(&(cr->msgs));

		rc = _send_messages(nb, skb, ping, cr, length, urgentonly);
		ping = 0;

		spin_lock_bh(&(nb->cmsg_lock));

		if (rc != 0)
			break;
	}

	if (0) {
oom:
		spin_lock_bh(&(nb->cmsg_lock));
	}

	if (rc != 0) {
		if (resume == 0)
			qos_enqueue(nb->queue, &(nb->rb_kp),
					QOS_CALLER_KPACKET);
	} else {
		atomic_set(&(nb->cmsg_task_scheduled), 0);
		schedule_controlmsg_timer(nb);
	}

	spin_unlock_bh(&(nb->cmsg_lock));
	spin_unlock_bh(&(nb->send_cmsg_lock));

	if (resume == 0)
		kref_put(&(nb->ref), neighbor_free);

	return rc;
}

void controlmsg_taskfunc(unsigned long nb)
{
	send_messages((struct neighbor *)nb, 0);
}

static void schedule_cmsg_task(struct neighbor *nb)
{
	if (atomic_cmpxchg(&(nb->cmsg_task_scheduled), 0, 1) == 0) {
		kref_get(&(nb->ref));
		atomic_cmpxchg(&(nb->cmsg_timer_running), 1, 2);
		tasklet_schedule(&(nb->cmsg_task));
	}
}

void controlmsg_timerfunc(unsigned long arg)
{
	struct neighbor *nb = (struct neighbor *) arg;

	int oldval = atomic_xchg(&(nb->cmsg_timer_running), 0);

	BUG_ON(oldval == 0);

	if (likely(oldval == 1))
		schedule_cmsg_task(nb);
	kref_put(&(nb->ref), neighbor_free);
}

static unsigned long get_cmsg_timeout(struct neighbor *nb, int nbstate)
{
	unsigned long timeout = get_next_ping_time(nb);

	if (likely(nbstate == NEIGHBOR_STATE_ACTIVE) &&
			list_empty(&(nb->control_msgs_out)) == 0) {
		struct control_msg_out *first = container_of(
				nb->control_msgs_out.next,
				struct control_msg_out, lh);
		if (time_before(first->timeout, jiffies +
				usecs_to_jiffies(nb->cmsg_interval)))
			timeout = jiffies;
		else if (time_before(first->timeout, timeout))
			timeout = first->timeout;
	}

	if (list_empty(&(nb->ucontrol_msgs_out)) == 0) {
		struct control_msg_out *first = container_of(
				nb->ucontrol_msgs_out.next,
				struct control_msg_out, lh);
		if (time_before(first->timeout, jiffies +
				usecs_to_jiffies(nb->cmsg_interval)))
			timeout = jiffies;
		else if (time_before(first->timeout, timeout))
			timeout = first->timeout;
	}

	return timeout;
}

static int cmsg_full_packet(struct neighbor *nb, int nbstate)
{
	int ping = time_to_send_ping(nb);
	int urgentonly = (nbstate != NEIGHBOR_STATE_ACTIVE);
	__u32 len = get_total_messages_length(nb, ping, urgentonly);

	if (len == 0)
		return 0;
	if (len < mss(nb))
		return 0;

	return 1;
}

void schedule_controlmsg_timer(struct neighbor *nb)
{
	unsigned long timeout;
	int state = get_neigh_state(nb);

	if (unlikely(state == NEIGHBOR_STATE_KILLED)) {
		atomic_cmpxchg(&(nb->cmsg_timer_running), 1, 2);
		return;
	}

	if (unlikely(atomic_read(&(nb->cmsg_task_scheduled)) == 1))
		return;

	if (cmsg_full_packet(nb, state))
		goto now;

	timeout = get_cmsg_timeout(nb, state);

	if (time_before_eq(timeout, jiffies)) {
now:
		schedule_cmsg_task(nb);
	} else {
		if (atomic_xchg(&(nb->cmsg_timer_running), 1) == 0)
			kref_get(&(nb->ref));
		mod_timer(&(nb->cmsg_timer), timeout);
	}
}

static void free_oldest_ucm(struct neighbor *nb)
{
	struct control_msg_out *cm = container_of(nb->ucontrol_msgs_out.next,
			struct control_msg_out, lh);

	BUG_ON(list_empty(&(nb->ucontrol_msgs_out)));
	BUG_ON(isurgent(cm) == 0);

	list_del(&(cm->lh));
	nb->ucmlength -= cm->length;
	atomic_dec(&(nb->ucmcnt));
	free_control_msg(cm);
}

static void add_control_msg(struct control_msg_out *cm, int retrans)
{
	int nbstate;
	__u64 newinterval;
	unsigned long jiffies_tmp;

	BUG_ON(cm->nb == 0);

	nbstate = get_neigh_state(cm->nb);

	BUG_ON(cm == 0);
	BUG_ON(cm->lh.next != LIST_POISON1 || cm->lh.prev != LIST_POISON2);

	cm->timeout = jiffies + msecs_to_jiffies(CMSG_INTERVAL_MS);

	spin_lock_bh(&(cm->nb->cmsg_lock));

	if (isurgent(cm)) {
		long msgs;

		msgs = atomic_inc_return(&(cm->nb->ucmcnt));
		BUG_ON(msgs <= 0);

		if (unlikely(retrans)) {
			if (msgs > MAX_URGENT_CMSGS_PER_NEIGH_RETRANSALLOW ||
					msgs > MAX_URGENT_CMSGS_PER_NEIGH) {
				atomic_dec(&(cm->nb->ucmcnt));
				free_control_msg(cm);
				goto out;
			}

			cm->nb->ucmlength += cm->length;
			list_add(&(cm->lh), &(cm->nb->ucontrol_msgs_out));
		} else {
			if (msgs > MAX_URGENT_CMSGS_PER_NEIGH) {
				free_oldest_ucm(cm->nb);
			}

			cm->nb->ucmlength += cm->length;
			list_add_tail(&(cm->lh), &(cm->nb->ucontrol_msgs_out));
		}
	} else {
		cm->nb->cmlength += cm->length;
		list_add_tail(&(cm->lh), &(cm->nb->control_msgs_out));
	}

	jiffies_tmp = jiffies;
	newinterval = (((__u64) cm->nb->cmsg_interval) * 255 +
			jiffies_to_usecs(jiffies_tmp -
			cm->nb->jiffies_last_cmsg)) / 256;
	cm->nb->jiffies_last_cmsg = jiffies_tmp;
	if (unlikely(newinterval > (1LL << 32) - 1))
		cm->nb->cmsg_interval = (__u32) ((1LL << 32) - 1);
	else
		cm->nb->cmsg_interval = newinterval;

	schedule_controlmsg_timer(cm->nb);

out:
	spin_unlock_bh(&(cm->nb->cmsg_lock));
}


void send_pong(struct neighbor *nb, __u32 cookie)
{
	struct control_msg_out *cm = _alloc_control_msg(nb, 0, 1);

	if (unlikely(cm == 0))
		return;

	cm->nb = nb;
	cm->type = MSGTYPE_PONG;
	cm->msg.pong.cookie = cookie;
	cm->msg.pong.type = MSGTYPE_PONG_TIMEENQUEUED;
	cm->msg.pong.delaycomp.time_enqueued = ktime_get();
	cm->length = 9;
	add_control_msg(cm, 0);
}

int send_reset_conn(struct neighbor *nb, __u32 conn_id_reset,
		__u32 conn_id_unknown, int lowprio)
{
	unsigned long iflags;
	int killed;
	struct control_msg_out *cm;
	struct unknownconnid_matchparam ucm;

	spin_lock_irqsave(&(nb->state_lock), iflags);
	killed = (nb->state == NEIGHBOR_STATE_KILLED);
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	if (unlikely(killed))
		return 0;

	cm = alloc_control_msg(nb, lowprio ?
			ACM_PRIORITY_LOW : ACM_PRIORITY_HIGH);

	if (unlikely(cm == 0))
		return 1;

	cm->type = MSGTYPE_RESET_CONN;
	cm->msg.reset_connidunknown.conn_id_reset = conn_id_reset;
	cm->msg.reset_connidunknown.conn_id_unknown = conn_id_unknown;
	cm->length = 5;

	if (conn_id_unknown != 0) {
		ucm.nb = nb;
		ucm.conn_id = conn_id_unknown;

		spin_lock_irqsave(&unknown_connids_lock, iflags);
		BUG_ON(htable_get(&unknown_connids, ucm_to_key(&ucm), &ucm) !=
				0);
		htable_insert(&unknown_connids, (char *) cm, ucm_to_key(&ucm));
		spin_unlock_irqrestore(&unknown_connids_lock, iflags);
	}

	add_control_msg(cm, 0);

	return 0;
}

void send_ack(struct neighbor *nb, __u32 seqno)
{
	struct control_msg_out *cm = _alloc_control_msg(nb, 0, 1);

	if (unlikely(cm == 0))
		return;

	cm->nb = nb;
	cm->type = MSGTYPE_ACK;
	cm->msg.ack.seqno = seqno;
	cm->length = 5;
	add_control_msg(cm, 0);
}

static void set_ooolen_flags(struct control_msg_out *cm)
{
	cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags &
			(~KP_ACK_CONN_FLAGS_OOO));
	cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
			ooolen_to_flags(cm->msg.ack_conn.length));
}

/* cmsg_lock must be held */
static void remove_pending_ackconn(struct control_msg_out *cm)
{
	cm->nb->cmlength -= cm->length;
	list_del(&(cm->lh));

	list_del(&(cm->msg.ack_conn.conn_acks));
	kref_put(&(cm->msg.ack_conn.src_in->ref), free_conn);
	cm->msg.ack_conn.src_in = 0;

	cm->type = 0;
	free_control_msg(cm);
}

/* cmsg_lock must be held */
static void recalc_scheduled_ackconn_size(struct control_msg_out *cm)
{
	cm->nb->cmlength -= cm->length;
	cm->length = 6 + ack_conn_len(cm->msg.ack_conn.flags);
	cm->nb->cmlength += cm->length;
}

/* cmsg_lock must be held */
static int _try_merge_ackconn(struct conn *src_in_l,
		struct control_msg_out *fromcm, struct control_msg_out *tocm,
		int from_newack)
{
	if (ooolen(fromcm->msg.ack_conn.flags) != 0 &&
			ooolen(tocm->msg.ack_conn.flags) != 0) {
		__u32 tocmseqno = tocm->msg.ack_conn.seqno_ooo;
		__u32 tocmlength = tocm->msg.ack_conn.length;
		__u32 fromcmseqno = fromcm->msg.ack_conn.seqno_ooo;
		__u32 fromcmlength = fromcm->msg.ack_conn.length;

		if (tocmseqno == fromcmseqno) {
			if (fromcmlength > tocmlength)
				tocm->msg.ack_conn.length = fromcmlength;
		} else if (seqno_after(fromcmseqno, tocmseqno) &&
				seqno_before_eq(fromcmseqno, tocmseqno +
				tocmlength)) {
			tocm->msg.ack_conn.length = fromcmseqno + fromcmlength -
					tocmseqno;
		} else if (seqno_before(fromcmseqno, tocmseqno) &&
				seqno_after_eq(fromcmseqno, tocmseqno)) {
			tocm->msg.ack_conn.seqno_ooo = fromcmseqno;
			tocm->msg.ack_conn.length = tocmseqno + tocmlength -
					fromcmseqno;
		} else {
			return 1;
		}
		set_ooolen_flags(tocm);
	}

	if ((fromcm->msg.ack_conn.flags &
			KP_ACK_CONN_FLAGS_SEQNO) != 0) {
		if ((tocm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_SEQNO) == 0)
			goto setseqno;

		BUG_ON(fromcm->msg.ack_conn.ack_seqno ==
				tocm->msg.ack_conn.ack_seqno);
		if ((tocm->msg.ack_conn.ack_seqno -
				fromcm->msg.ack_conn.ack_seqno) < (1 << 31)) {
			BUG_ON(seqno_after(fromcm->msg.ack_conn.seqno,
						tocm->msg.ack_conn.seqno));
			goto skipseqno;
		}

		BUG_ON(seqno_before(fromcm->msg.ack_conn.seqno,
				tocm->msg.ack_conn.seqno));

setseqno:
		tocm->msg.ack_conn.flags = (tocm->msg.ack_conn.flags |
					KP_ACK_CONN_FLAGS_SEQNO);
		tocm->msg.ack_conn.seqno = fromcm->msg.ack_conn.seqno;
		tocm->msg.ack_conn.ack_seqno = fromcm->msg.ack_conn.ack_seqno;

skipseqno:
		if ((fromcm->msg.ack_conn.flags &
				KP_ACK_CONN_FLAGS_WINDOW) != 0)
			tocm->msg.ack_conn.flags = (tocm->msg.ack_conn.flags |
					KP_ACK_CONN_FLAGS_WINDOW);
	}


	if (ooolen(fromcm->msg.ack_conn.flags) != 0) {
		tocm->msg.ack_conn.seqno_ooo = fromcm->msg.ack_conn.seqno_ooo;
		tocm->msg.ack_conn.length = fromcm->msg.ack_conn.length;
		set_ooolen_flags(tocm);
	}

	if ((fromcm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_CREDITS) != 0) {
		BUG_ON((tocm->msg.ack_conn.flags &
				KP_ACK_CONN_FLAGS_CREDITS) != 0);
		tocm->msg.ack_conn.decaytime_seqno =
				fromcm->msg.ack_conn.decaytime_seqno;
		tocm->msg.ack_conn.decaytime =
				fromcm->msg.ack_conn.decaytime;
	}

	recalc_scheduled_ackconn_size(tocm);
	if (from_newack)
		kref_put(&(fromcm->msg.ack_conn.src_in->ref), free_conn);
	else
		remove_pending_ackconn(fromcm);

	return 0;
}

/* cmsg_lock must be held */
static void try_merge_ackconns(struct conn *src_in_l,
		struct control_msg_out *cm)
{
	struct list_head *currlh = cm->msg.ack_conn.conn_acks.next;

	while (currlh != &(src_in_l->source.in.acks_pending)) {
		struct control_msg_out *currcm = container_of(currlh,
				struct control_msg_out,
				msg.ack_conn.conn_acks);
		currlh = currlh->next;
		remove_connack_oooflag_ifold(src_in_l, currcm);
		_try_merge_ackconn(src_in_l, currcm, cm, 0);
	}
}

static void mergeadd_ackconn(struct conn *src_in_l, struct control_msg_out *cm)
{
	struct list_head *currlh;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	spin_lock_bh(&(cm->nb->cmsg_lock));

	currlh = src_in_l->source.in.acks_pending.next;

	while (currlh != &(src_in_l->source.in.acks_pending)) {
		struct control_msg_out *currcm = container_of(currlh,
				struct control_msg_out,
				msg.ack_conn.conn_acks);

		BUG_ON(currcm->nb != cm->nb);
		BUG_ON(currcm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != src_in_l);
		BUG_ON(currcm->msg.ack_conn.conn_id !=
				cm->msg.ack_conn.conn_id);

		if (_try_merge_ackconn(src_in_l, cm, currcm, 1) == 0) {
			try_merge_ackconns(src_in_l, currcm);
			schedule_controlmsg_timer(currcm->nb);
			spin_unlock_bh(&(currcm->nb->cmsg_lock));
			return;
		}

		currlh = currlh->next;
	}

	list_add_tail(&(cm->msg.ack_conn.conn_acks),
			&(src_in_l->source.in.acks_pending));

	spin_unlock_bh(&(cm->nb->cmsg_lock));

	add_control_msg(cm, 0);
}

static int try_update_ackconn_seqno(struct conn *src_in_l)
{
	int rc = 1;

	spin_lock_bh(&(src_in_l->source.in.nb->cmsg_lock));

	if (list_empty(&(src_in_l->source.in.acks_pending)) == 0) {
		struct control_msg_out *cm = container_of(
				src_in_l->source.in.acks_pending.next,
				struct control_msg_out,
				msg.ack_conn.conn_acks);
		BUG_ON(cm->nb != src_in_l->source.in.nb);
		BUG_ON(cm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != src_in_l);
		BUG_ON(cm->msg.ack_conn.conn_id !=
				src_in_l->reversedir->target.out.conn_id);

		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
				KP_ACK_CONN_FLAGS_SEQNO |
				KP_ACK_CONN_FLAGS_WINDOW);
		cm->msg.ack_conn.seqno = src_in_l->source.in.next_seqno;

		src_in_l->source.in.ack_seqno++;
		cm->msg.ack_conn.ack_seqno = src_in_l->source.in.ack_seqno;

		remove_connack_oooflag_ifold(src_in_l, cm);
		recalc_scheduled_ackconn_size(cm);

		try_merge_ackconns(src_in_l, cm);

		rc = 0;
	}

	spin_unlock_bh(&(src_in_l->source.in.nb->cmsg_lock));

	return rc;
}

void send_ack_conn_ifneeded(struct conn *src_in_l)
{
	struct control_msg_out *cm;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	if (src_in_l->source.in.inorder_ack_needed == 0 &&
			((src_in_l->source.in.window_seqnolimit -
			src_in_l->source.in.next_seqno)/2) <
			(src_in_l->source.in.window_seqnolimit_remote -
			src_in_l->source.in.next_seqno))
		return;

	if (try_update_ackconn_seqno(src_in_l) == 0)
		goto out;

	cm = alloc_control_msg(src_in_l->source.in.nb, ACM_PRIORITY_LOW);
	if (cm == 0)
		return;

	cm->type = MSGTYPE_ACK_CONN;
	cm->msg.ack_conn.flags = KP_ACK_CONN_FLAGS_SEQNO |
			KP_ACK_CONN_FLAGS_WINDOW;
	kref_get(&(src_in_l->ref));
	cm->msg.ack_conn.src_in = src_in_l;
	cm->msg.ack_conn.conn_id = src_in_l->reversedir->target.out.conn_id;
	cm->msg.ack_conn.seqno = src_in_l->source.in.next_seqno;
	src_in_l->source.in.ack_seqno++;
	cm->msg.ack_conn.ack_seqno = src_in_l->source.in.ack_seqno;
	cm->length = 6 + ack_conn_len(cm->msg.ack_conn.flags);

	mergeadd_ackconn(src_in_l, cm);

out:
	src_in_l->source.in.inorder_ack_needed = 0;
	src_in_l->source.in.window_seqnolimit_remote =
			src_in_l->source.in.window_seqnolimit;
}

void send_ack_conn_ooo(struct control_msg_out *cm, struct conn *src_in_l,
		__u32 conn_id, __u32 seqno_ooo, __u32 length)
{
	cm->type = MSGTYPE_ACK_CONN;
	kref_get(&(src_in_l->ref));
	BUG_ON(src_in_l->sourcetype != SOURCE_IN);
	cm->msg.ack_conn.flags = 0;
	cm->msg.ack_conn.src_in = src_in_l;
	cm->msg.ack_conn.conn_id = conn_id;
	cm->msg.ack_conn.seqno_ooo = seqno_ooo;
	cm->msg.ack_conn.length = length;
	set_ooolen_flags(cm);
	cm->length = 6 + ack_conn_len(cm->msg.ack_conn.flags);

	mergeadd_ackconn(src_in_l, cm);
}

static int try_add_decaytime(struct conn *trgt_out_l, __u16 decaytime)
{
	int rc = 1;
	struct conn *src_in = trgt_out_l->reversedir;

	spin_lock_bh(&(trgt_out_l->target.out.nb->cmsg_lock));

	if (list_empty(&(src_in->source.in.acks_pending)) == 0) {
		struct control_msg_out *cm = container_of(
				src_in->source.in.acks_pending.next,
				struct control_msg_out,
				msg.ack_conn.conn_acks);
		BUG_ON(cm->nb != trgt_out_l->target.out.nb);
		BUG_ON(cm->type != MSGTYPE_ACK_CONN);
		BUG_ON(cm->msg.ack_conn.src_in != trgt_out_l->reversedir);
		BUG_ON(cm->msg.ack_conn.conn_id !=
				trgt_out_l->target.out.conn_id);

		BUG_ON((cm->msg.ack_conn.flags & KP_ACK_CONN_FLAGS_CREDITS) !=
				0);
		cm->msg.ack_conn.flags = (cm->msg.ack_conn.flags |
				KP_ACK_CONN_FLAGS_CREDITS);
		cm->msg.ack_conn.decaytime_seqno =
				trgt_out_l->target.out.decaytime_seqno;
		cm->msg.ack_conn.decaytime = decaytime;
		recalc_scheduled_ackconn_size(cm);

		rc = 0;
	}

	spin_unlock_bh(&(trgt_out_l->target.out.nb->cmsg_lock));

	return rc;
}

void send_decaytime(struct conn *trgt_out_l, int force, __u16 decaytime)
{
	struct control_msg_out *cm;

	if (try_add_decaytime(trgt_out_l, decaytime) == 0)
		goto out;

	if (force == 0)
		return;

	cm = alloc_control_msg(trgt_out_l->target.out.nb, ACM_PRIORITY_LOW);

	if (cm == 0)
		return;

	cm->type = MSGTYPE_ACK_CONN;
	cm->msg.ack_conn.flags = KP_ACK_CONN_FLAGS_CREDITS;
	kref_get(&(trgt_out_l->reversedir->ref));
	BUG_ON(trgt_out_l->targettype != TARGET_OUT);
	cm->msg.ack_conn.src_in = trgt_out_l->reversedir;
	cm->msg.ack_conn.conn_id = trgt_out_l->target.out.conn_id;
	cm->msg.ack_conn.decaytime_seqno =
			trgt_out_l->target.out.decaytime_seqno;
	cm->msg.ack_conn.decaytime = decaytime;

	cm->length = 6 + ack_conn_len(cm->msg.ack_conn.flags);
	mergeadd_ackconn(trgt_out_l, cm);

out:
	trgt_out_l->target.out.decaytime_last = decaytime;
	trgt_out_l->target.out.decaytime_seqno =
			(trgt_out_l->target.out.decaytime_seqno + 1) % 64;
	trgt_out_l->target.out.decaytime_send_allowed = 0;
}

void free_ack_conns(struct conn *src_in_l)
{
	int changed = 0;
	spin_lock_bh(&(src_in_l->source.in.nb->cmsg_lock));
	while (list_empty(&(src_in_l->source.in.acks_pending)) == 0) {
		struct list_head *currlh =
				src_in_l->source.in.acks_pending.next;
		struct control_msg_out *currcm = container_of(currlh,
				struct control_msg_out,
				msg.ack_conn.conn_acks);

		remove_pending_ackconn(currcm);
		changed = 1;
	}
	if (changed)
		schedule_controlmsg_timer(src_in_l->source.in.nb);
	spin_unlock_bh(&(src_in_l->source.in.nb->cmsg_lock));
}

void send_connect_success(struct control_msg_out *cm, __u32 rcvd_conn_id,
		__u32 gen_conn_id, __u32 init_seqno, struct conn *src_in)
{
	cm->type = MSGTYPE_CONNECT_SUCCESS;
	cm->msg.connect_success.rcvd_conn_id = rcvd_conn_id;
	cm->msg.connect_success.gen_conn_id = gen_conn_id;
	cm->msg.connect_success.init_seqno = init_seqno;
	kref_get(&(src_in->ref));
	cm->msg.connect_success.src_in = src_in;
	cm->length = 16;
	add_control_msg(cm, 0);
}

void send_connect_nb(struct control_msg_out *cm, __u32 conn_id,
		__u32 init_seqno, struct conn *src_in)
{
	cm->type = MSGTYPE_CONNECT;
	cm->msg.connect.conn_id = conn_id;
	cm->msg.connect.init_seqno = init_seqno;
	kref_get(&(src_in->ref));
	BUG_ON(src_in->sourcetype != SOURCE_IN);
	cm->msg.connect.src_in = src_in;
	cm->length = 12;
	add_control_msg(cm, 0);
}

#warning todo ref to buf instead
void send_conndata(struct control_msg_out *cm, __u32 conn_id, __u32 seqno,
		char *data_orig, char *data, __u32 datalen)
{
	cm->type = MSGTYPE_CONNDATA;
	cm->msg.conn_data.conn_id = conn_id;
	cm->msg.conn_data.seqno = seqno;
	cm->msg.conn_data.data_orig = data_orig;
	cm->msg.conn_data.data = data;
	cm->msg.conn_data.datalen = datalen;
	cm->length = 11 + datalen;
	add_control_msg(cm, 0);
}

void send_connid_unknown(struct neighbor *nb, __u32 conn_id)
{
	unsigned long iflags;
	char *ret;
	struct unknownconnid_matchparam ucm;

	struct control_msg_out *cm = alloc_control_msg(nb, ACM_PRIORITY_HIGH);

	if (unlikely(cm == 0))
		return;

	cm->type = MSGTYPE_CONNID_UNKNOWN;
	cm->msg.reset_connidunknown.conn_id_unknown = conn_id;
	cm->length = 5;

	ucm.nb = nb;
	ucm.conn_id = conn_id;

	spin_lock_irqsave(&unknown_connids_lock, iflags);
	ret = htable_get(&unknown_connids, ucm_to_key(&ucm), &ucm);
	if (ret == 0)
		htable_insert(&unknown_connids, (char *) cm, ucm_to_key(&ucm));
	spin_unlock_irqrestore(&unknown_connids_lock, iflags);

	if (ret != 0) {
		struct control_msg_out *cm2 = (struct control_msg_out *) ret;

		BUG_ON(cm2->type != MSGTYPE_RESET_CONN &&
			cm2->type != MSGTYPE_CONNID_UNKNOWN);

		kref_put(&(cm2->ref), cmsg_kref_free);

		cm->type = 0;
		free_control_msg(cm);
	} else {
		add_control_msg(cm, 0);
	}
}


static int matches_connretrans(void *htentry, void *searcheditem)
{
	struct control_retrans *cr = (struct control_retrans *) htentry;
	struct retransmit_matchparam *rm = (struct retransmit_matchparam *)
			searcheditem;

	return rm->nb == cr->nb && rm->seqno == cr->seqno;
}

static int matches_unknownconnid(void *htentry, void *searcheditem)
{
	struct control_msg_out *cm = (struct control_msg_out *) htentry;

	struct unknownconnid_matchparam *ucm =
			(struct unknownconnid_matchparam *)searcheditem;

	BUG_ON(cm->type != MSGTYPE_RESET_CONN &&
			cm->type != MSGTYPE_CONNID_UNKNOWN);

	return ucm->nb == cm->nb && ucm->conn_id ==
			cm->msg.reset_connidunknown.conn_id_unknown;
}

void __init cor_kgen_init(void)
{
	controlmsg_slab = kmem_cache_create("cor_controlmsg",
			sizeof(struct control_msg_out), 8, 0, 0);
	controlretrans_slab = kmem_cache_create("cor_controlretransmsg",
			sizeof(struct control_retrans), 8, 0, 0);
	htable_init(&retransmits, matches_connretrans,
			offsetof(struct control_retrans, htab_entry),
			offsetof(struct control_retrans, ref));
	htable_init(&unknown_connids, matches_unknownconnid,
			offsetof(struct control_msg_out,
			msg.reset_connidunknown.htab_entry),
			offsetof(struct control_msg_out, ref));
}

MODULE_LICENSE("GPL");
