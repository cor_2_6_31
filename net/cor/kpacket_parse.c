/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <asm/byteorder.h>

#include "cor.h"

/*static __u64 pull_u64(struct sk_buff *skb, int convbo)
{
	char *ptr = cor_pull_skb(skb, 8);

	__u64 ret = 0;

	BUG_ON(0 == ptr);

	((char *)&ret)[0] = ptr[0];
	((char *)&ret)[1] = ptr[1];
	((char *)&ret)[2] = ptr[2];
	((char *)&ret)[3] = ptr[3];
	((char *)&ret)[4] = ptr[4];
	((char *)&ret)[5] = ptr[5];
	((char *)&ret)[6] = ptr[6];
	((char *)&ret)[7] = ptr[7];

	if (convbo)
		return be64_to_cpu(ret);
	return ret;
}*/

static __u32 pull_u32(struct sk_buff *skb, int convbo)
{
	char *ptr = cor_pull_skb(skb, 4);

	__u32 ret = 0;

	BUG_ON(0 == ptr);

	((char *)&ret)[0] = ptr[0];
	((char *)&ret)[1] = ptr[1];
	((char *)&ret)[2] = ptr[2];
	((char *)&ret)[3] = ptr[3];

	if (convbo)
		return be32_to_cpu(ret);
	return ret;
}

static __u16 pull_u16(struct sk_buff *skb, int convbo)
{
	char *ptr = cor_pull_skb(skb, 2);

	__u16 ret = 0;

	BUG_ON(0 == ptr);

	((char *)&ret)[0] = ptr[0];
	((char *)&ret)[1] = ptr[1];

	if (convbo)
		return be16_to_cpu(ret);
	return ret;
}

static __u8 pull_u8(struct sk_buff *skb)
{
	char *ptr = cor_pull_skb(skb, 1);
	BUG_ON(0 == ptr);
	return *ptr;
}

static void parse_ack_conn(struct neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id = pull_u32(skb, 1);
	__u8 flags = pull_u8(skb);

	struct conn *src_in = get_conn(conn_id);

	if ((flags & KP_ACK_CONN_FLAGS_SEQNO) != 0) {
		__u32 seqno = pull_u32(skb, 1);
		int setwindow = 0;
		__u8 window = 0;

		if ((flags & KP_ACK_CONN_FLAGS_WINDOW) != 0) {
			setwindow = 1;
			window = pull_u8(skb);
		}

		if (likely(src_in != 0))
			conn_ack_rcvd(nb, conn_id, src_in->reversedir, seqno,
					setwindow, window);
	}

	if (ooolen(flags) != 0) {
		__u32 seqno_ooo = pull_u32(skb, 1);
		__u32 ooo_len;

		if (ooolen(flags) == 1) {
			ooo_len = pull_u8(skb);
		} else if (ooolen(flags) == 2) {
			ooo_len = pull_u16(skb, 1);
		} else if (ooolen(flags) == 4) {
			ooo_len = pull_u32(skb, 1);
		} else {
			BUG();
		}

		if (likely(src_in != 0))
			conn_ack_ooo_rcvd(nb, conn_id, src_in->reversedir,
					seqno_ooo, ooo_len);
	}

	if (flags & KP_ACK_CONN_FLAGS_CREDITS) {
		__u16 value = pull_u16(skb, 1);

		__u8 seqno = value >> 10;
		__u16 decaytime = value & 1023;

		BUG_ON(seqno >= 64);
		BUG_ON(decaytime >= 1024);

		if (likely(src_in != 0))
			set_conn_in_decaytime(nb, conn_id, src_in, seqno,
					decaytime);
	}

	if (unlikely(src_in == 0)) {
		send_connid_unknown(nb, conn_id);
		return;
	}

	spin_lock_bh(&(src_in->rcv_lock));

	if (unlikely(is_conn_in(src_in, nb, conn_id) == 0)) {
		send_connid_unknown(nb, conn_id);
	} else {
		set_last_act(src_in);
	}

	spin_unlock_bh(&(src_in->rcv_lock));

	kref_put(&(src_in->ref), free_conn);
}

static void parse_connect(struct neighbor *nb, struct sk_buff *skb)
{
	struct conn *src_in;
	__u32 conn_id = pull_u32(skb, 1);
	__u32 init_seqno = pull_u32(skb, 1);
	__u8 window = pull_u8(skb);
	__u16 credits = pull_u16(skb, 1);
	__u8 credits_seqno = credits >> 10;
	__u16 decaytime = credits & 1023;
	struct control_msg_out *cm = alloc_control_msg(nb, ACM_PRIORITY_MED);
	__u32 gen_conn_id;
	__u32 gen_seqno;

	if (unlikely(cm == 0))
		goto err;

	src_in = alloc_conn(GFP_ATOMIC);

	if (unlikely(src_in == 0))
		goto err;

	src_in->is_client = 1;

	spin_lock_bh(&(src_in->rcv_lock));
	spin_lock_bh(&(src_in->reversedir->rcv_lock));
	if (unlikely(conn_init_out(src_in->reversedir, nb))) {
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
		spin_unlock_bh(&(src_in->rcv_lock));
		kref_put(&(src_in->reversedir->ref), free_conn);
		kref_put(&(src_in->ref), free_conn);
		goto err;
	}

	gen_conn_id = src_in->source.in.conn_id;
	gen_seqno = src_in->source.in.next_seqno;

	src_in->reversedir->target.out.conn_id = conn_id;

	src_in->reversedir->target.out.seqno_nextsend = init_seqno;
	src_in->reversedir->target.out.seqno_acked = init_seqno;
	reset_seqno(src_in->reversedir, init_seqno);

	src_in->reversedir->target.out.seqno_windowlimit =
			src_in->reversedir->target.out.seqno_nextsend +
			dec_log_64_11(window);
	insert_reverse_connid(src_in->reversedir);
	spin_unlock_bh(&(src_in->reversedir->rcv_lock));
	spin_unlock_bh(&(src_in->rcv_lock));

	BUG_ON(credits_seqno >= 64);
	BUG_ON(decaytime >= 1024);
	set_conn_in_decaytime(nb, gen_conn_id, src_in, credits_seqno,
			decaytime);

	send_connect_success(cm, conn_id, gen_conn_id, gen_seqno, src_in);

	if (0) {
err:
		if (cm != 0) {
			free_control_msg(cm);
		}
		send_reset_conn(nb, conn_id, 0, 0);
	}
}

static void parse_conn_success(struct neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id1 = pull_u32(skb, 1);
	__u32 conn_id2 = pull_u32(skb, 1);
	__u32 init_seqno = pull_u32(skb, 1);
	__u8 window = pull_u8(skb);
	__u16 credits = pull_u16(skb, 1);

	__u8 credits_seqno = credits >> 10;
	__u16 decaytime = credits & 1023;

	int set = 0;

	struct conn *src_in = get_conn(conn_id1);

	if (unlikely(src_in == 0))
		goto err;

	if (unlikely(src_in->is_client != 0))
		goto err;


	spin_lock_bh(&(src_in->reversedir->rcv_lock));
	spin_lock_bh(&(src_in->rcv_lock));

	if (unlikely(is_conn_in(src_in, nb, conn_id1) == 0))
		goto err_unlock;

	if (unlikely(unlikely(src_in->reversedir->targettype != TARGET_OUT) ||
			unlikely(src_in->reversedir->target.out.nb != nb) ||
			unlikely(src_in->reversedir->target.out.conn_id != 0 &&
			src_in->reversedir->target.out.conn_id != conn_id2)))
		goto err_unlock;

	if (unlikely(src_in->reversedir->isreset != 0))
		goto err_unlock;

	set_last_act(src_in);

	if (likely(src_in->reversedir->target.out.conn_id == 0)) {
		src_in->reversedir->target.out.conn_id = conn_id2;
		src_in->reversedir->target.out.seqno_nextsend = init_seqno;
		src_in->reversedir->target.out.seqno_acked = init_seqno;
		src_in->reversedir->target.out.decaytime_send_allowed = 1;
		reset_seqno(src_in->reversedir, init_seqno);

		src_in->reversedir->target.out.seqno_windowlimit =
				src_in->reversedir->target.out.seqno_nextsend +
				dec_log_64_11(window);

		insert_reverse_connid(src_in->reversedir);

		set = 1;
	}

	spin_unlock_bh(&(src_in->reversedir->rcv_lock));
	spin_unlock_bh(&(src_in->rcv_lock));

	BUG_ON(credits_seqno >= 64);
	BUG_ON(decaytime >= 1024);
	if (set)
		set_conn_in_decaytime(nb, conn_id1, src_in, credits_seqno,
				decaytime);

	flush_buf(src_in->reversedir);

	if (0) {
err_unlock:
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
		spin_unlock_bh(&(src_in->rcv_lock));
err:
		send_reset_conn(nb, conn_id2, conn_id1, 0);
	}

	if (likely(src_in != 0))
		kref_put(&(src_in->ref), free_conn);
}

static void parse_conndata(struct neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id = pull_u32(skb, 1);
	__u32 seqno = pull_u32(skb, 1);
	__u16 datalength = pull_u16(skb, 1);
	char *data = cor_pull_skb(skb, datalength);

	BUG_ON(data == 0);

	conn_rcv_buildskb(nb, data, datalength, conn_id, seqno);
}

static void parse_reset(struct neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id = pull_u32(skb, 1);

	int send;

	struct conn *src_in = get_conn(conn_id);
	if (src_in == 0)
		return;

	if (src_in->is_client) {
		spin_lock_bh(&(src_in->rcv_lock));
		spin_lock_bh(&(src_in->reversedir->rcv_lock));
	} else {
		spin_lock_bh(&(src_in->reversedir->rcv_lock));
		spin_lock_bh(&(src_in->rcv_lock));
	}

	send = unlikely(is_conn_in(src_in, nb, conn_id));
	if (send && src_in->reversedir->isreset == 0)
		src_in->reversedir->isreset = 1;

	if (src_in->is_client) {
		spin_unlock_bh(&(src_in->rcv_lock));
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
	} else {
		spin_unlock_bh(&(src_in->reversedir->rcv_lock));
		spin_unlock_bh(&(src_in->rcv_lock));
	}

	if (send)
		reset_conn(src_in);

	kref_put(&(src_in->ref), free_conn);
}

static void parse_connid_unknown(struct neighbor *nb, struct sk_buff *skb)
{
	__u32 conn_id = pull_u32(skb, 1);
	struct conn *trgt_out = get_conn_reverse(nb, conn_id);
	if (trgt_out == 0)
		return;

	spin_lock_bh(&(trgt_out->rcv_lock));

	if (unlikely(trgt_out->targettype != TARGET_OUT))
		goto err;
	if (unlikely((trgt_out->target.out.nb != nb)))
		goto err;
	if (unlikely((trgt_out->target.out.conn_id != conn_id)))
		goto err;

	if (trgt_out->isreset == 0)
		trgt_out->isreset = 1;

	spin_unlock_bh(&(trgt_out->rcv_lock));

	reset_conn(trgt_out);

	if (0) {
err:
		spin_unlock_bh(&(trgt_out->rcv_lock));
	}
	kref_put(&(trgt_out->ref), free_conn);
}

static void kernel_packet2(struct neighbor *nb, struct sk_buff *skb,
		__u32 seqno1)
{
	int ack = 0;

	while (1) {
		__u32 seqno2;

		__u32 cookie;
		__u32 respdelay;

		__u32 max_cmsg_dly;

		__u8 *codeptr = cor_pull_skb(skb, 1);
		__u8 code;

		if (codeptr == 0)
			break;

		code = *codeptr;

		switch (code) {
		case KP_PADDING:
			break;
		case KP_PING:
			cookie = pull_u32(skb, 0);
			send_pong(nb, cookie);
			break;
		case KP_PONG:
			cookie = pull_u32(skb, 0);
			respdelay = pull_u32(skb, 1);
			ping_resp(nb, cookie, respdelay);
			ack = 1;
			break;
		case KP_ACK:
			seqno2 = pull_u32(skb, 1);
			kern_ack_rcvd(nb, seqno2);
			break;
		case KP_ACK_CONN:
			parse_ack_conn(nb, skb);
			ack = 1;
			break;
		case KP_CONNECT:
			parse_connect(nb, skb);
			ack = 1;
			break;
		case KP_CONNECT_SUCCESS:
			parse_conn_success(nb, skb);
			ack = 1;
			break;
		case KP_CONN_DATA:
			parse_conndata(nb, skb);
			break;
		case KP_RESET_CONN:
			parse_reset(nb, skb);
			ack = 1;
			break;
		case KP_CONNID_UNKNOWN:
			parse_connid_unknown(nb, skb);
			ack = 1;
			break;
		case KP_SET_MAX_CMSG_DELAY:
			max_cmsg_dly = pull_u32(skb, 1);
			if (((__u64) max_cmsg_dly) * 1000 > ((__u64)
					(max_cmsg_dly * 1000)))
				max_cmsg_dly = 400000000;
			else
				max_cmsg_dly *= 100;
			atomic_set(&(nb->max_remote_cmsg_delay), max_cmsg_dly);
			ack = 1;
			break;
		default:
			BUG();
		}
	}

	if (ack)
		send_ack(nb, seqno1);
}

void kernel_packet(struct neighbor *nb, struct sk_buff *skb, __u32 seqno)
{
	unsigned char *data = skb->data;
	unsigned int len = skb->len;

	while (1) {
		__u8 *codeptr = cor_pull_skb(skb, 1);
		__u8 code;
		__u8 *flags;

		char *lengthptr;
		__u32 length;

		if (codeptr == 0)
			break;
		code = *codeptr;

		switch (code) {
		case KP_PADDING:
			break;
		case KP_PING:
			if (cor_pull_skb(skb, 4) == 0)
				goto discard;
			break;
		case KP_PONG:
			if (cor_pull_skb(skb, 8) == 0)
				goto discard;
			break;
		case KP_ACK:
			if (cor_pull_skb(skb, 4) == 0)
				goto discard;
			break;
		case KP_ACK_CONN:
			if (cor_pull_skb(skb, 4) == 0)
				goto discard;
			flags = cor_pull_skb(skb, 1);
			if (flags == 0)
				goto discard;
			if (cor_pull_skb(skb, ack_conn_len(*flags)) == 0)
				goto discard;
			break;
		case KP_CONNECT:
			if (cor_pull_skb(skb, 11) == 0)
				goto discard;
			break;
		case KP_CONNECT_SUCCESS:
			if (cor_pull_skb(skb, 15) == 0)
				goto discard;
			break;
		case KP_CONN_DATA:
			if (cor_pull_skb(skb, 8) == 0)
				goto discard;
			lengthptr = cor_pull_skb(skb, 2);
			if (lengthptr == 0)
				goto discard;
			length = ntohs(*((__u16 *)lengthptr));
			if (cor_pull_skb(skb, length) == 0)
				goto discard;
			break;
		case KP_RESET_CONN:
		case KP_CONNID_UNKNOWN:
			if (cor_pull_skb(skb, 4) == 0)
				goto discard;
			break;
		case KP_SET_MAX_CMSG_DELAY:
			if (cor_pull_skb(skb, 4) == 0)
				goto discard;
			break;
		default:
			goto discard;
		}
	}

	skb->data = data;
	skb->len = len;

	kernel_packet2(nb, skb, seqno);
discard:
	kfree_skb(skb);
}

MODULE_LICENSE("GPL");
