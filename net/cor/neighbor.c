/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "cor.h"

/**
 * Splited packet data format:
 * announce proto version [4]
 *   is 0, may be increased if format changes
 * packet version [4]
 *   starts with 0, increments every time the data field changes
 * total size [4]
 *   total data size of all merged packets
 * offset [4]
 *   used to determine the order when merging the split packet
 *   unit is bytes
 * [data]
 * commulative checksum [8] (not yet)
 *   chunk 1 contains the checksum of the data in chunk 1
 *   chunk 2 contains the checksum of the data in chunk 1+2
 *   ...
 *
 * Data format of the announce packet "data" field:
 * min_announce_proto_version [4]
 * max_announce_proto_version [4]
 * min_cor_proto_version [4]
 * max_cor_proto_version [4]
 *   versions which are understood
 *
 * command [4]
 * commandlength [4]
 * commanddata [commandlength]
 */

/* Commands */

#define NEIGHCMD_ADDADDR 1

/**
 * Parameter:
 * addrtypelen [2]
 * addrtype [addrtypelen]
 * addrlen [2]
 * addr [addrlen]
 */

static atomic_t packets_in_workqueue = ATOMIC_INIT(0);

DEFINE_MUTEX(announce_rcv_lock);
DEFINE_SPINLOCK(announce_snd_lock);
DEFINE_SPINLOCK(neighbor_list_lock);

char *addrtype = "id";
char *addr;
int addrlen;


LIST_HEAD(nb_list);
struct kmem_cache *nb_slab;

LIST_HEAD(announce_out_list);

struct notifier_block netdev_notify;


#define ADDRTYPE_UNKNOWN 0
#define ADDRTYPE_ID 1

static int get_addrtype(__u32 addrtypelen, char *addrtype)
{
	if (addrtypelen == 2 &&
			(addrtype[0] == 'i' || addrtype[0] == 'I') &&
			(addrtype[1] == 'd' || addrtype[1] == 'D'))
		return ADDRTYPE_ID;

	return ADDRTYPE_UNKNOWN;
}

void neighbor_free(struct kref *ref)
{
	struct neighbor *nb = container_of(ref, struct neighbor, ref);
	printk(KERN_ERR "neighbor free");
	BUG_ON(nb->nb_list.next != LIST_POISON1);
	BUG_ON(nb->nb_list.prev != LIST_POISON2);
	if (nb->addr != 0)
		kfree(nb->addr);
	nb->addr = 0;
	if (nb->dev != 0)
		dev_put(nb->dev);
	nb->dev = 0;
	if (nb->queue != 0)
		kref_put(&(nb->queue->ref), free_qos);
	nb->queue = 0;
	kmem_cache_free(nb_slab, nb);
}

static struct neighbor *alloc_neighbor(gfp_t allocflags)
{
	struct neighbor *nb = kmem_cache_alloc(nb_slab, allocflags);
	__u32 seqno;

	if (unlikely(nb == 0))
		return 0;

	memset(nb, 0, sizeof(struct neighbor));

	kref_init(&(nb->ref));
	init_timer(&(nb->cmsg_timer));
	nb->cmsg_timer.function = controlmsg_timerfunc;
	nb->cmsg_timer.data = (unsigned long) nb;
	tasklet_init(&(nb->cmsg_task), controlmsg_taskfunc, (unsigned long) nb);
	atomic_set(&(nb->cmsg_task_scheduled), 0);
	atomic_set(&(nb->cmsg_timer_running), 0);
	spin_lock_init(&(nb->cmsg_lock));
	spin_lock_init(&(nb->send_cmsg_lock));
	INIT_LIST_HEAD(&(nb->control_msgs_out));
	INIT_LIST_HEAD(&(nb->ucontrol_msgs_out));
	nb->last_ping_time = jiffies;
	nb->cmsg_interval = 1000000;
	spin_lock_init(&(nb->credits_lock));
	nb->ktime_credit_update = ktime_get();
	nb->ktime_credit_decay = nb->ktime_credit_decay;
	spin_lock_init(&(nb->busytill_lock));
	nb->busy_till = jiffies;
	atomic_set(&(nb->latency), 1000000);
	atomic_set(&(nb->max_remote_cmsg_delay), 1000000);
	spin_lock_init(&(nb->state_lock));
	get_random_bytes((char *) &seqno, sizeof(seqno));
	spin_lock_init(&(nb->connid_reuse_lock));
	INIT_LIST_HEAD(&(nb->connid_reuse_list));
	nb->connid_reuse_pingcnt = 0;
	atomic_set(&(nb->kpacket_seqno), seqno);
	spin_lock_init(&(nb->conn_list_lock));
	INIT_LIST_HEAD(&(nb->rcv_conn_list));
	spin_lock_init(&(nb->retrans_lock));
	INIT_LIST_HEAD(&(nb->retrans_list));
	INIT_LIST_HEAD(&(nb->retrans_list_conn));

	return nb;
}

int is_from_nb(struct sk_buff *skb, struct neighbor *nb)
{
	int rc;

	char source_hw[MAX_ADDR_LEN];
	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

	rc = (skb->dev == nb->dev && memcmp(nb->mac, source_hw,
			MAX_ADDR_LEN) == 0);
	return rc;
}

struct neighbor *get_neigh_by_mac(struct sk_buff *skb)
{
	struct list_head *currlh;
	struct neighbor *ret = 0;


	char source_hw[MAX_ADDR_LEN];
	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

	spin_lock_bh(&(neighbor_list_lock));

	currlh = nb_list.next;

	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);

		if (skb->dev == curr->dev && memcmp(curr->mac, source_hw,
				MAX_ADDR_LEN) == 0) {
			ret = curr;
			kref_get(&(ret->ref));
			break;
		}

		currlh = currlh->next;
	}

	spin_unlock_bh(&(neighbor_list_lock));

	return ret;
}

int addrtype_known(__u16 addrtypelen, __u8 *addrtype)
{
	if (get_addrtype(addrtypelen, addrtype) == ADDRTYPE_ID)
		return 1;
	return 0;
}

struct neighbor *find_neigh(__u16 addrtypelen, __u8 *addrtype,
		__u16 addrlen, __u8 *addr)
{
	struct list_head *currlh;
	struct neighbor *ret = 0;

	if (get_addrtype(addrtypelen, addrtype) != ADDRTYPE_ID)
		return 0;

	spin_lock_bh(&(neighbor_list_lock));

	currlh = nb_list.next;

	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);

		if (curr->addrlen == addrlen && memcmp(curr->addr, addr,
				addrlen) == 0) {
			ret = curr;
			kref_get(&(ret->ref));

			goto out;
		}

		currlh = currlh->next;
	}

out:
	spin_unlock_bh(&(neighbor_list_lock));

	return ret;
}

/*
 * TODO:
 *
 * address flags
 * credit exchange factor + unstable flag
 * throughput bound conns: throughput,credits/msecs
 * latency bound conns: latency (ms), credits/byte
 */
#warning todo extend
#warning todo pregenerate (lift response size limit)
__u32 generate_neigh_list(char *buf, __u32 buflen, __u32 limit, __u32 offset)
{
	struct list_head *currlh;

	int bufferfull = 0;

	__u32 total = 0;
	__u32 cnt = 0;

	__u32 buf_offset = 8;
	__u32 headoffset = 0;

	int rc;

	/*
	 * The variable length headers rowcount and fieldlength need to be
	 * generated after the data. This is done by reserving the maximum space
	 * they could take. If they end up being smaller, the data is moved so
	 * that there is no gap.
	 */

	BUG_ON(buf == 0);
	BUG_ON(buflen < buf_offset);

	/* num_fields */
	rc = encode_len(buf + buf_offset, buflen - buf_offset, 2);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	/* addr field */
	BUG_ON(buflen < buf_offset + 2);
	put_u16(buf + buf_offset, LIST_NEIGH_FIELD_ADDR, 1);
	buf_offset += 2;

	rc = encode_len(buf + buf_offset, buflen - buf_offset, 0);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	/* latency field */
	BUG_ON(buflen < buf_offset + 2);
	put_u16(buf + buf_offset, LIST_NEIGH_FIELD_LATENCY, 1);
	buf_offset += 2;

	rc = encode_len(buf + buf_offset, buflen - buf_offset, 1);
	BUG_ON(rc <= 0);
	buf_offset += rc;

	spin_lock_bh(&(neighbor_list_lock));

	currlh = nb_list.next;

	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);
		__u8 state;
		unsigned long iflags;

		__u32 addroffset = buf_offset;

		/* get_neigh_state not used here because it would deadlock */
		spin_lock_irqsave(&(curr->state_lock), iflags);
		state = curr->state;
		spin_unlock_irqrestore(&(curr->state_lock), iflags);

		if (state != NEIGHBOR_STATE_ACTIVE)
			goto cont2;

		if (total < offset)
			goto cont;

		if (unlikely(buflen < buf_offset + 4+ 4 + 4 + 4 + 2 +
				curr->addrlen + 1))
			bufferfull = 1;

		if (bufferfull)
			goto cont;

		buf_offset += 4; /* reserve bufferspace for fieldlen */
		/* numaddr */
		rc = encode_len(buf + buf_offset, buflen - buf_offset, 1);
		BUG_ON(rc <= 0);
		buf_offset += rc;

		/* addrtypelen */
		rc = encode_len(buf + buf_offset, buflen - buf_offset, 2);
		BUG_ON(rc <= 0);
		buf_offset += rc;

		/* addrlen */
		rc = encode_len(buf + buf_offset, buflen - buf_offset,
				curr->addrlen);
		BUG_ON(rc <= 0);
		buf_offset += rc;

		buf[buf_offset] = 'i'; /* addrtype */
		buf_offset += 1;
		buf[buf_offset] = 'd';
		buf_offset += 1;
		BUG_ON(curr->addrlen > buflen - buf_offset);
		memcpy(buf + buf_offset, curr->addr, curr->addrlen); /* addr */
		buf_offset += curr->addrlen;

		/* fieldlen */
		rc = encode_len(buf + addroffset, 4, buf_offset - addroffset -
				4);
		BUG_ON(rc <= 0);
		BUG_ON(rc > 4);
		if (likely(rc < 4))
			memmove(buf+addroffset+rc, buf+addroffset + 4,
					buf_offset - addroffset - 4);
		buf_offset -= (4-rc);

		buf[buf_offset] = enc_log_64_11(atomic_read(&(curr->latency)));
		buf_offset += 1;

		BUG_ON(buf_offset > buflen);

		cnt++;

cont:
		total++;
cont2:
		currlh = currlh->next;
	}

	spin_unlock_bh(&(neighbor_list_lock));

	rc = encode_len(buf, 4, total);
	BUG_ON(rc <= 0);
	BUG_ON(rc > 4);
	headoffset += rc;

	rc = encode_len(buf + headoffset, 4, cnt);
	BUG_ON(rc <= 0);
	BUG_ON(rc > 4);
	headoffset += rc;

	if (likely(headoffset < 8))
		memmove(buf+headoffset, buf+8, buf_offset);

	return buf_offset + headoffset - 8;
}

static void _refresh_initial_debitsrate(struct net_device *dev,
		__u32 debitsrate)
{
	ktime_t now;
	__u32 neighbors = 0;
	struct list_head *currlh;

	currlh = nb_list.next;
	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);

		if (curr->dev == dev)
			neighbors++;

		currlh = currlh->next;
	}

	now = ktime_get();

	currlh = nb_list.next;
	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);

		if (curr->dev == dev)
			set_creditrate_initial(curr,
					debitsrate/neighbors, now);

		currlh = currlh->next;
	}
}

/* neighbor list lock has to be held while calling this */
static void refresh_initial_debitsrate(void)
{
	struct list_head *currlh1;
	__u32 ifcnt = 0;
	__u32 creditrate;

	currlh1 = nb_list.next;

	while (currlh1 != &nb_list) {
		struct neighbor *curr1 = container_of(currlh1, struct neighbor,
				nb_list);

		struct list_head *currlh2;
		currlh2 = nb_list.next;
		while (currlh2 != currlh1) {
			struct neighbor *curr2 = container_of(currlh2,
					struct neighbor, nb_list);
			if (curr1->dev == curr2->dev)
				goto present1;
		}

		ifcnt++;

present1:

		currlh1 = currlh1->next;
	}

	creditrate = creditrate_initial();

	currlh1 = nb_list.next;

	while (currlh1 != &nb_list) {
		struct neighbor *curr1 = container_of(currlh1, struct neighbor,
				nb_list);

		struct list_head *currlh2;
		currlh2 = nb_list.next;
		while (currlh2 != currlh1) {
			struct neighbor *curr2 = container_of(currlh2,
					struct neighbor, nb_list);
			if (curr1->dev == curr2->dev)
				goto present2;
		}

		_refresh_initial_debitsrate(curr1->dev, creditrate/ifcnt);

present2:

		currlh1 = currlh1->next;
	}
}

static void stall_timer(struct work_struct *work);

static void reset_all_conns(struct neighbor *nb)
{
	while (1) {
		unsigned long iflags;
		struct conn *src_in;
		int rc;

		spin_lock_irqsave(&(nb->conn_list_lock), iflags);

		if (list_empty(&(nb->rcv_conn_list))) {
			spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);
			break;
		}

		src_in = container_of(nb->rcv_conn_list.next, struct conn,
				source.in.nb_list);
		kref_get(&(src_in->ref));

		spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);

		if (src_in->is_client) {
			spin_lock_bh(&(src_in->rcv_lock));
			spin_lock_bh(&(src_in->reversedir->rcv_lock));
		} else {
			spin_lock_bh(&(src_in->reversedir->rcv_lock));
			spin_lock_bh(&(src_in->rcv_lock));
		}

		if (unlikely(unlikely(src_in->sourcetype != SOURCE_IN) ||
				unlikely(src_in->source.in.nb != nb))) {
			rc = 1;
			goto unlock;
		}

		rc = send_reset_conn(nb, src_in->reversedir->target.out.conn_id,
				src_in->source.in.conn_id, 1);

		if (unlikely(rc != 0))
			goto unlock;

		if (src_in->reversedir->isreset == 0)
			src_in->reversedir->isreset = 1;

unlock:
		if (src_in->is_client) {
			spin_unlock_bh(&(src_in->rcv_lock));
			spin_unlock_bh(&(src_in->reversedir->rcv_lock));
		} else {
			spin_unlock_bh(&(src_in->reversedir->rcv_lock));
			spin_unlock_bh(&(src_in->rcv_lock));
		}

		if (rc == 0) {
			reset_conn(src_in);
			kref_put(&(src_in->ref), free_conn);
		} else {
			kref_put(&(src_in->ref), free_conn);
			kref_get(&(nb->ref));
			INIT_DELAYED_WORK(&(nb->stalltimeout_timer),
					stall_timer);
			schedule_delayed_work(&(nb->stalltimeout_timer), 100);
			break;
		}
	}
}

static void reset_neighbor(struct neighbor *nb)
{
	int removenblist;
	unsigned long iflags;

	spin_lock_irqsave(&(nb->state_lock), iflags);
	removenblist = (nb->state != NEIGHBOR_STATE_KILLED);
	nb->state = NEIGHBOR_STATE_KILLED;
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	if (removenblist)
		printk(KERN_ERR "reset_neighbor");

	reset_all_conns(nb);

	if (removenblist) {
		spin_lock_bh(&neighbor_list_lock);
		list_del(&(nb->nb_list));
		refresh_initial_debitsrate();
		spin_unlock_bh(&neighbor_list_lock);

		#warning todo empty control_msg list

		kref_put(&(nb->ref), neighbor_free); /* nb_list */
	}
}

static void reset_neighbor_dev(struct net_device *dev)
{
	struct list_head *currlh;

restart:
	spin_lock_bh(&neighbor_list_lock);

	currlh = nb_list.next;

	while (currlh != &nb_list) {
		unsigned long iflags;
		struct neighbor *currnb = container_of(currlh, struct neighbor,
				nb_list);
		__u8 state;

		if (currnb->dev != dev)
			goto cont;

		spin_lock_irqsave(&(currnb->state_lock), iflags);
		state = currnb->state;
		spin_unlock_irqrestore(&(currnb->state_lock), iflags);

		if (state != NEIGHBOR_STATE_KILLED) {
			spin_unlock_bh(&neighbor_list_lock);
			reset_neighbor(currnb);
			goto restart;
		}

cont:
		currlh = currlh->next;
	}

	spin_unlock_bh(&neighbor_list_lock);
}

static void stall_timer(struct work_struct *work)
{
	struct neighbor *nb = container_of(to_delayed_work(work),
			struct neighbor, stalltimeout_timer);

	int stall_time_ms;
	__u8 nbstate;

	unsigned long iflags;

	spin_lock_irqsave(&(nb->state_lock), iflags);
	nbstate = nb->state;
	if (unlikely(nbstate != NEIGHBOR_STATE_STALLED))
		nb->str_timer_pending = 0;

	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	if (unlikely(nbstate == NEIGHBOR_STATE_ACTIVE))
		goto kref;

	stall_time_ms = ktime_to_ms(ktime_get()) -
			ktime_to_ms(nb->state_time.last_roundtrip);

	if (nbstate == NEIGHBOR_STATE_STALLED &&
			stall_time_ms < NB_KILL_TIME_MS) {
		INIT_DELAYED_WORK(&(nb->stalltimeout_timer), stall_timer);
		schedule_delayed_work(&(nb->stalltimeout_timer),
				msecs_to_jiffies(NB_KILL_TIME_MS -
				stall_time_ms));
		return;
	}

	reset_neighbor(nb);

kref:
	kref_put(&(nb->ref), neighbor_free); /* stall_timer */
}

int get_neigh_state(struct neighbor *nb)
{
	int ret;
	unsigned long iflags;
	int starttimer = 0;
	int stall_time_ms;

	BUG_ON(nb == 0);

	spin_lock_irqsave(&(nb->state_lock), iflags);

	stall_time_ms = ktime_to_ms(ktime_get()) -
			ktime_to_ms(nb->state_time.last_roundtrip);

	if (unlikely(likely(nb->state == NEIGHBOR_STATE_ACTIVE) && unlikely(
			stall_time_ms > NB_STALL_TIME_MS && (
			nb->ping_intransit >= NB_STALL_MINPINGS ||
			nb->ping_intransit >= PING_COOKIES_PER_NEIGH)))) {
		nb->state = NEIGHBOR_STATE_STALLED;
		starttimer = (nb->str_timer_pending == 0);

		nb->str_timer_pending = 1;
		printk(KERN_ERR "switched to stalled");
		BUG_ON(nb->ping_intransit > PING_COOKIES_PER_NEIGH);
	}

	ret = nb->state;

	spin_unlock_irqrestore(&(nb->state_lock), iflags);


	if (unlikely(starttimer)) {
		kref_get(&(nb->ref));
		INIT_DELAYED_WORK(&(nb->stalltimeout_timer),
				stall_timer);
		schedule_delayed_work(&(nb->stalltimeout_timer),
				NB_KILL_TIME_MS - stall_time_ms);
	}

	return ret;
}

static struct ping_cookie *find_cookie(struct neighbor *nb, __u32 cookie)
{
	int i;

	for(i=0;i<PING_COOKIES_PER_NEIGH;i++) {
		if (nb->cookies[i].cookie == cookie)
			return &(nb->cookies[i]);
	}
	return 0;
}

void ping_resp(struct neighbor *nb, __u32 cookie, __u32 respdelay)
{
	unsigned long iflags;

	struct ping_cookie *c;
	int i;

	__s64 oldlatency;
	__s64 pinglatency;
	__s64 newlatency;

	ktime_t now;
	int call_connidreuse = 0;

	spin_lock_irqsave(&(nb->state_lock), iflags);

	c = find_cookie(nb, cookie);

	if (unlikely(c == 0))
		goto out;

	for(i=0;i<PING_COOKIES_PER_NEIGH;i++) {
		if (nb->cookies[i].cookie != 0 &&
				ktime_before(nb->cookies[i].time, c->time)) {
			nb->cookies[i].pongs++;
			if (nb->cookies[i].pongs >= PING_PONGLIMIT) {
				nb->cookies[i].cookie = 0;
				nb->cookies[i].pongs = 0;
				nb->ping_intransit--;
			}
		}
	}

	c->cookie = 0;
	nb->ping_intransit--;

	call_connidreuse = ktime_before_eq(nb->last_roundtrip_end, c->time);
	now = ktime_get();
	nb->last_roundtrip_end = now;

	oldlatency = ((__s64) ((__u32)atomic_read(&(nb->latency)))) * 1000;
	pinglatency = ktime_to_ns(now) - ktime_to_ns(c->time) -
			respdelay * 1000LL;
	if (unlikely(unlikely(nb->state == NEIGHBOR_STATE_INITIAL) &&
			nb->ping_success < 16))
		newlatency = (oldlatency * nb->ping_success + pinglatency) /
				(nb->ping_success + 1);
	else
		newlatency = (oldlatency * 15 + pinglatency) / 16;

	newlatency = (newlatency + 500) / 1000;

	if (unlikely(newlatency < 0))
		newlatency = 0;
	if (unlikely(newlatency >= (1LL << 32)))
		newlatency = (1LL << 32) - 1;

	atomic_set(&(nb->latency), (__u32) newlatency);

	if (unlikely(nb->state == NEIGHBOR_STATE_INITIAL ||
			nb->state == NEIGHBOR_STATE_STALLED)) {
		nb->ping_success++;
		call_connidreuse = 0;

		if (nb->state == NEIGHBOR_STATE_INITIAL) {
			__u64 jiffies64 = get_jiffies_64();
			if (nb->state_time.last_state_change == 0)
				nb->state_time.last_state_change = jiffies64;
			if (jiffies64 <= (nb->state_time.last_state_change +
					msecs_to_jiffies(INITIAL_TIME_MS)))
				goto out;
		}

		if (nb->ping_success >= PING_SUCCESS_CNT) {
			/*if (nb->state == NEIGHBOR_STATE_INITIAL)
				printk(KERN_ERR "switched from initial to active");
			else
				printk(KERN_ERR "switched from stalled to active");
			*/

			if (nb->state == NEIGHBOR_STATE_INITIAL)
				set_busy_till(nb, 0);

			nb->state = NEIGHBOR_STATE_ACTIVE;
			nb->ping_success = 0;
			nb->state_time.last_roundtrip = c->time;
		}
	} else {
		nb->state_time.last_roundtrip = c->time;
	}

out:
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	if (call_connidreuse)
		connid_used_pingsuccess(nb);
}

__u32 add_ping_req(struct neighbor *nb, unsigned long *last_ping_time)
{
	unsigned long iflags;
	struct ping_cookie *c;
	__u32 i;

	__u32 cookie;

	spin_lock_irqsave(&(nb->state_lock), iflags);

	for (i=0;i<PING_COOKIES_PER_NEIGH;i++) {
		if (nb->cookies[i].cookie == 0)
			goto found;
	}

	get_random_bytes((char *) &i, sizeof(i));
	i = (i % (PING_COOKIES_PER_NEIGH - PING_COOKIES_FIFO)) +
			PING_COOKIES_FIFO;

found:
	c = &(nb->cookies[i]);
	c->time = ktime_get();
	c->pongs = 0;
	nb->lastcookie++;
	if (unlikely(nb->lastcookie == 0))
		nb->lastcookie++;
	c->cookie = nb->lastcookie;

	nb->ping_intransit++;

	cookie = c->cookie;

	*last_ping_time = nb->last_ping_time;
	nb->last_ping_time = jiffies;

	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	return cookie;
}

void unadd_ping_req(struct neighbor *nb, __u32 cookie,
		unsigned long last_ping_time)
{
	unsigned long iflags;
	int i;

	if (cookie == 0)
		return;

	spin_lock_irqsave(&(nb->state_lock), iflags);

	for (i=0;i<PING_COOKIES_PER_NEIGH;i++) {
		if (nb->cookies[i].cookie == cookie) {
			nb->cookies[i].cookie = 0;
			nb->ping_intransit--;
			break;
		}
	}

	nb->last_ping_time = last_ping_time;

	spin_unlock_irqrestore(&(nb->state_lock), iflags);
}

void set_busy_till(struct neighbor *nb, int initial)
{
	unsigned long iflags;
	unsigned long newval;

	/* improve latency measurement and make traffic analysis harder */

	spin_lock_irqsave(&(nb->busytill_lock), iflags);
	if (unlikely(initial)) {
		newval = jiffies + msecs_to_jiffies(ACTIVEDELAY_INITIAL_MS);
	} else {
		__u32 rand;

		get_random_bytes((char *) &rand, 4);

		newval = jiffies + msecs_to_jiffies(ACTIVEDELAY_NOCONN_MIN_MS +
				(1LL << 32) * (ACTIVEDELAY_NOCONN_MAX_MS -
				ACTIVEDELAY_NOCONN_MIN_MS) / rand);
	}

	if (time_after(newval, nb->busy_till))
		nb->busy_till = newval;

	spin_unlock_irqrestore(&(nb->busytill_lock), iflags);
}

static int get_ping_forcetime(struct neighbor *nb)
{
	unsigned long iflags;
	int state = get_neigh_state(nb);
	int idle = 1;

	spin_lock_irqsave(&(nb->busytill_lock), iflags);
	if (time_after_eq(nb->busy_till, jiffies))
		idle = 0;
	else
		nb->busy_till = jiffies;
	spin_unlock_irqrestore(&(nb->busytill_lock), iflags);

	if (unlikely(state != NEIGHBOR_STATE_ACTIVE))
		return PING_FORCETIME_MS;

	if (idle) {
		spin_lock_irqsave(&(nb->conn_list_lock), iflags);
		idle = list_empty(&(nb->rcv_conn_list));
		spin_unlock_irqrestore(&(nb->conn_list_lock), iflags);
	}

	if (idle)
		return PING_FORCETIME_ACTIVEIDLE_MS;
	else
		return PING_FORCETIME_ACTIVE_MS;
}

/**
 * Check additional to the checks and timings already done in kpacket_gen.c
 * This is primarily to make sure that we do not invalidate other ping cookies
 * which might still receive responses. It does this by requiring a certain
 * mimimum delay between pings, depending on how many pings are already in
 * transit.
 */
int time_to_send_ping(struct neighbor *nb)
{
	unsigned long iflags;
	int rc = 1;

	__u32 forcetime = get_ping_forcetime(nb);

	spin_lock_irqsave(&(nb->state_lock), iflags);
	if (nb->ping_intransit >= PING_COOKIES_NOTHROTTLE) {
		__u32 mindelay = (( ((__u32) atomic_read(&(nb->latency))) +
				((__u32) atomic_read(
				&(nb->max_remote_cmsg_delay))) )/1000) <<
				(nb->ping_intransit + 1 -
				PING_COOKIES_NOTHROTTLE);

		if (mindelay > PING_THROTTLE_LIMIT_MS)
			mindelay = PING_THROTTLE_LIMIT_MS;

		if (jiffies_to_msecs(jiffies - nb->last_ping_time) <  mindelay)
			rc = 0;
	}

	if (jiffies_to_msecs(jiffies - nb->last_ping_time) < (forcetime/2))
		rc = 0;
	else if (jiffies_to_msecs(jiffies - nb->last_ping_time) >= forcetime &&
			rc != 0)
		rc = 2;

	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	return rc;
}

int get_next_ping_time(struct neighbor *nb)
{
	unsigned long iflags;
	unsigned long ret;
	__u32 forcetime = get_ping_forcetime(nb);

	spin_lock_irqsave(&(nb->state_lock), iflags);
	ret = round_jiffies_up(nb->last_ping_time +
			msecs_to_jiffies(forcetime));
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	return ret;
}

static void add_neighbor(struct neighbor *nb)
{
	struct list_head *currlh;

	spin_lock_bh(&neighbor_list_lock);

	currlh = nb_list.next;

	BUG_ON((nb->addr == 0) != (nb->addrlen == 0));

	while (currlh != &nb_list) {
		struct neighbor *curr = container_of(currlh, struct neighbor,
				nb_list);

		if (curr->addrlen == nb->addrlen && memcmp(curr->addr, nb->addr,
				curr->addrlen) == 0)
			goto already_present;

		currlh = currlh->next;
	}

	/* kref_get not needed here, because the caller leaves its ref to us */
	printk(KERN_ERR "add_neigh");

	init_timer(&(nb->retrans_timer));
	nb->retrans_timer.function = retransmit_timerfunc;
	nb->retrans_timer.data = (unsigned long) nb;
	tasklet_init(&(nb->retrans_task), retransmit_conn_taskfunc,
			(unsigned long) nb);

	init_timer(&(nb->retrans_timer_conn));
	nb->retrans_timer_conn.function = retransmit_conn_timerfunc;
	nb->retrans_timer_conn.data = (unsigned long) nb;
	tasklet_init(&(nb->retrans_task_conn), retransmit_conn_taskfunc,
			(unsigned long) nb);

	spin_lock_bh(&(nb->cmsg_lock));
	nb->last_ping_time = jiffies;
	nb->cmsg_interval = 1000000;
	schedule_controlmsg_timer(nb);
	spin_unlock_bh(&(nb->cmsg_lock));

	list_add_tail(&(nb->nb_list), &nb_list);

	refresh_initial_debitsrate();

	if (0) {
already_present:
		kmem_cache_free(nb_slab, nb);
	}

	spin_unlock_bh(&neighbor_list_lock);
}

static __u32 pull_u32(struct sk_buff *skb, int convbo)
{
	char *ptr = cor_pull_skb(skb, 4);

	__u32 ret = 0;

	BUG_ON(0 == ptr);

	((char *)&ret)[0] = ptr[0];
	((char *)&ret)[1] = ptr[1];
	((char *)&ret)[2] = ptr[2];
	((char *)&ret)[3] = ptr[3];

	if (convbo)
		return be32_to_cpu(ret);
	return ret;
}

static int apply_announce_addaddr(struct neighbor *nb, __u32 cmd, __u32 len,
		char *cmddata)
{
	__u16 addrtypelen;
	char *addrtype;
	__u16 addrlen;
	char *addr;

	BUG_ON((nb->addr == 0) != (nb->addrlen == 0));

	if (nb->addr != 0)
		return 0;

	if (len < 4)
		return 0;

	addrtypelen = be16_to_cpu(*((__u16 *) cmddata));
	cmddata += 2;
	len -= 2;

	if (len < 2)
		return 0;

	addrlen = be16_to_cpu(*((__u16 *) cmddata));
	cmddata += 2;
	len -= 2;

	addrtype = cmddata;
	cmddata += addrtypelen;
	len -= addrtypelen;

	addr = cmddata;
	cmddata += addrlen;
	len -= addrlen;

	if (len < 0)
		return 0;

	if (get_addrtype(addrtypelen, addrtype) != ADDRTYPE_ID)
		return 0;

	nb->addr = kmalloc(addrlen, GFP_KERNEL);
	if (unlikely(nb->addr == 0))
		return 1;

	memcpy(nb->addr, addr, addrlen);
	nb->addrlen = addrlen;

	return 0;
}

static void apply_announce_cmd(struct neighbor *nb, __u32 cmd, __u32 len,
		char *cmddata)
{
	if (cmd == NEIGHCMD_ADDADDR) {
		apply_announce_addaddr(nb, cmd, len, cmddata);
	} else {
		/* ignore unknown cmds */
	}
}

static void apply_announce_cmds(char *msg, __u32 len, struct net_device *dev,
		char *source_hw)
{
	struct neighbor *nb = alloc_neighbor(GFP_KERNEL);

	if (unlikely(nb == 0))
		return;

	nb->queue = get_queue(dev);
	if (nb->queue == 0) {
		kmem_cache_free(nb_slab, nb);
		return;
	}

	dev_hold(dev);
	nb->dev = dev;

	memcpy(nb->mac, source_hw, MAX_ADDR_LEN);

	while (len >= 8) {
		__u32 cmd;
		__u32 cmdlen;

		cmd = be32_to_cpu(*((__u32 *) msg));
		msg += 4;
		len -= 4;
		cmdlen = be32_to_cpu(*((__u32 *) msg));
		msg += 4;
		len -= 4;

		BUG_ON(cmdlen > len);

		apply_announce_cmd(nb, cmd, cmdlen, msg);

		msg += cmdlen;
		len -= cmdlen;
	}

	BUG_ON(len != 0);

	add_neighbor(nb);
}

static int check_announce_cmds(char *msg, __u32 len)
{
	while (len >= 8) {
		__u32 cmd;
		__u32 cmdlen;

		cmd = be32_to_cpu(*((__u32 *) msg));
		msg += 4;
		len -= 4;
		cmdlen = be32_to_cpu(*((__u32 *) msg));
		msg += 4;
		len -= 4;

		/* malformated packet */
		if (unlikely(cmdlen > len))
			return 1;

		msg += cmdlen;
		len -= cmdlen;
	}

	if (unlikely(len != 0))
		return 1;

	return 0;
}

static void parse_announce(char *msg, __u32 len, struct net_device *dev,
		char *source_hw)
{
	__u32 min_announce_version;
	__u32 max_announce_version;
	__u32 min_cor_version;
	__u32 max_cor_version;

	if (unlikely(len < 16))
		return;

	min_announce_version = be32_to_cpu(*((__u32 *) msg));
	msg += 4;
	len -= 4;
	max_announce_version = be32_to_cpu(*((__u32 *) msg));
	msg += 4;
	len -= 4;
	min_cor_version = be32_to_cpu(*((__u32 *) msg));
	msg += 4;
	len -= 4;
	max_cor_version = be32_to_cpu(*((__u32 *) msg));
	msg += 4;
	len -= 4;

	if (min_announce_version != 0)
		return;
	if (min_cor_version != 0)
		return;
	if (check_announce_cmds(msg, len)) {
		return;
	}
	apply_announce_cmds(msg, len, dev, source_hw);
}

struct announce_in {
	/* lh has to be first */
	struct list_head lh;
	struct sk_buff_head skbs; /* sorted by offset */
	struct net_device *dev;
	char source_hw[MAX_ADDR_LEN];
	__u32 announce_proto_version;
	__u32 packet_version;
	__u32 total_size;
	__u32 received_size;
	__u64 last_received_packet;
};

LIST_HEAD(announce_list);

struct kmem_cache *announce_in_slab;

static void merge_announce(struct announce_in *ann)
{
	char *msg = kmalloc(ann->total_size, GFP_KERNEL);
	__u32 copy = 0;

	if (msg == 0) {
		/* try again when next packet arrives */
		return;
	}

	while (copy != ann->total_size) {
		__u32 currcpy;
		__u32 offset = 0;
		struct sk_buff *skb;
		struct skb_procstate *ps;

		if (unlikely(skb_queue_empty(&(ann->skbs)))) {
			printk(KERN_ERR "net/cor/neighbor.c: sk_head ran "
					"empty while merging packets\n");
			goto free;
		}

		skb = skb_dequeue(&(ann->skbs));
		ps = skb_pstate(skb);

		currcpy = skb->len;
		if (unlikely(ps->funcstate.announce2.offset > copy)) {
			printk(KERN_ERR "net/cor/neighbor.c: invalid offset"
					"value found\n");
			goto free;
		}

		if (unlikely(ps->funcstate.announce2.offset < copy)) {
			offset = copy - ps->funcstate.announce2.offset;
			currcpy -= offset;
		}

		if (unlikely(currcpy + copy > ann->total_size))
			goto free;

		memcpy(msg + copy, skb->data + offset, currcpy);
		copy += currcpy;
		kfree_skb(skb);
	}

	parse_announce(msg, ann->total_size, ann->dev, ann->source_hw);

free:
	if (msg != 0)
		kfree(msg);

	dev_put(ann->dev);
	list_del(&(ann->lh));
	kmem_cache_free(announce_in_slab, ann);
}

static int __rcv_announce(struct sk_buff *skb, struct announce_in *ann)
{
	struct skb_procstate *ps = skb_pstate(skb);

	__u32 offset = ps->funcstate.announce2.offset;
	__u32 len = skb->len;

	__u32 curroffset = 0;
	__u32 prevoffset = 0;
	__u32 prevlen = 0;

	struct sk_buff *curr = ann->skbs.next;

	if (unlikely(len + offset > ann->total_size)) {
		/* invalid header */
		kfree_skb(skb);
		return 0;
	}

	/**
	 * Try to find the right place to insert in the sorted list. This
	 * means to process the list until we find a skb which has a greater
	 * offset, so we can insert before it to keep the sort order. However,
	 * this is complicated by the fact that the new skb must not be inserted
	 * between 2 skbs if there is no data missing in between. So the loop
	 * runs has to keep running until there is either a gap to insert or
	 * we see that this data has already been received.
	 */
	while ((void *) curr != (void *) &(ann->skbs)) {
		struct skb_procstate *currps = skb_pstate(skb);

		curroffset = currps->funcstate.announce2.offset;

		if (curroffset > offset && (prevoffset + prevlen) < curroffset)
			break;

		prevoffset = curroffset;
		prevlen = curr->len;
		curr = curr->next;

		if ((offset+len) <= (prevoffset+prevlen)) {
			/* we already have this data */
			kfree_skb(skb);
			return 0;
		}
	}

	/**
	 * Calculate how much data was really received, by substracting
	 * the bytes we already have.
	 */
	if (unlikely(prevoffset + prevlen > offset)) {
		len -= (prevoffset + prevlen) - offset;
		offset = prevoffset + prevlen;
	}

	if (unlikely((void *) curr != (void *) &(ann->skbs) &&
			(offset + len) > curroffset))
		len = curroffset - offset;

	ann->received_size += len;
	BUG_ON(ann->received_size > ann->total_size);
	__skb_queue_before(&(ann->skbs), curr, skb);
	ann->last_received_packet = get_jiffies_64();

	if (ann->received_size == ann->total_size)
		merge_announce(ann);
	else if (unlikely(ann->skbs.qlen >= 16))
		return 1;

	return 0;
}

static void _rcv_announce(struct work_struct *work)
{
	struct skb_procstate *ps = container_of(work,
			struct skb_procstate, funcstate.announce1.work);
	struct sk_buff *skb = skb_from_pstate(ps);

	struct announce_in *curr = 0;
	struct announce_in *leastactive = 0;
	__u32 list_size = 0;

	__u32 announce_proto_version = pull_u32(skb, 1);
	__u32 packet_version = pull_u32(skb, 1);
 	__u32 total_size = pull_u32(skb, 1);

	char source_hw[MAX_ADDR_LEN];
	memset(source_hw, 0, MAX_ADDR_LEN);
	if (skb->dev->header_ops != 0 &&
			skb->dev->header_ops->parse != 0)
		skb->dev->header_ops->parse(skb, source_hw);

 	ps->funcstate.announce2.offset = pull_u32(skb, 1);

 	if (total_size > 8192)
 		goto discard;

 	mutex_lock(&(announce_rcv_lock));

 	if (announce_proto_version != 0)
 		goto discard;

	curr = (struct announce_in *) announce_list.next;

	while (((struct list_head *) curr) != &(announce_list)) {
		list_size++;
		if (curr->dev == skb->dev && memcmp(curr->source_hw, source_hw,
				MAX_ADDR_LEN) == 0 &&
				curr->announce_proto_version ==
				announce_proto_version &&
				curr->packet_version == packet_version &&
				curr->total_size == total_size)
			goto found;

		if (leastactive == 0 || curr->last_received_packet <
				leastactive->last_received_packet)
			leastactive = curr;

		curr = (struct announce_in *) curr->lh.next;
	}

	if (list_size >= 128) {
		BUG_ON(leastactive == 0);
		curr = leastactive;

		curr->last_received_packet = get_jiffies_64();

		while (!skb_queue_empty(&(curr->skbs))) {
			struct sk_buff *skb2 = skb_dequeue(&(curr->skbs));
			kfree_skb(skb2);
		}

		dev_put(curr->dev);
	} else {
		curr = kmem_cache_alloc(announce_in_slab, GFP_KERNEL);
		if (curr == 0)
			goto discard;

		skb_queue_head_init(&(curr->skbs));
		list_add_tail((struct list_head *) curr, &announce_list);
	}

	curr->packet_version = packet_version;
	curr->total_size = total_size;
	curr->received_size = 0;
	curr->announce_proto_version = announce_proto_version;
	curr->dev = skb->dev;
	dev_hold(curr->dev);
	memcpy(curr->source_hw, source_hw, MAX_ADDR_LEN);

 found:
 	if (__rcv_announce(skb, curr)) {
		list_del((struct list_head *) curr);
		dev_put(curr->dev);
		kmem_cache_free(announce_in_slab, curr);
	}

 	if (0) {
 discard:
 		kfree_skb(skb);
	}

	mutex_unlock(&(announce_rcv_lock));

	atomic_dec(&packets_in_workqueue);
}

int rcv_announce(struct sk_buff *skb)
{
	struct skb_procstate *ps = skb_pstate(skb);
	long queuelen;

	queuelen = atomic_inc_return(&packets_in_workqueue);

	BUG_ON(queuelen <= 0);

	if (queuelen > MAX_PACKETS_IN_RCVQUEUE) {
		atomic_dec(&packets_in_workqueue);
		kfree_skb(skb);
		return NET_RX_SUCCESS;
	}

	INIT_WORK(&(ps->funcstate.announce1.work), _rcv_announce);
	schedule_work(&(ps->funcstate.announce1.work));
	return NET_RX_SUCCESS;
}

struct announce{
	struct kref ref;

	__u32 packet_version;
	char *announce_msg;
	__u32 announce_msg_len;
};

struct announce *last_announce;

static int send_announce_chunk(struct announce_data *ann)
{
	struct sk_buff *skb;
	__u32 packet_size = 256;
	__u32 remainingdata = ann->ann->announce_msg_len -
			ann->curr_announce_msg_offset;
	__u32 headroom = LL_ALLOCATED_SPACE(ann->dev);
	__u32 overhead = 17 + headroom;
	char *header;
	char *ptr;
	int rc = 0;

	if (remainingdata < packet_size)
		packet_size = remainingdata;

	skb = alloc_skb(packet_size + overhead, GFP_ATOMIC);
	if (unlikely(skb == 0))
		return 0;

	skb->protocol = htons(ETH_P_COR);
	skb->dev = ann->dev;
	skb_reserve(skb, headroom);

	if(unlikely(dev_hard_header(skb, ann->dev, ETH_P_COR,
			ann->dev->broadcast, ann->dev->dev_addr, skb->len) < 0))
		goto out_err;

	skb_reset_network_header(skb);

	header = skb_put(skb, 17);
	if (unlikely(header == 0))
		goto out_err;

	header[0] = PACKET_TYPE_ANNOUNCE;

	put_u32(header + 1, 0, 1); /* announce proto version */
	put_u32(header + 5, ann->ann->packet_version, 1); /* packet version */
	put_u32(header + 9, ann->ann->announce_msg_len, 1); /* total size */
	put_u32(header + 13, ann->curr_announce_msg_offset, 1); /* offset */

	ptr = skb_put(skb, packet_size);
	if (unlikely(ptr == 0))
		goto out_err;

	memcpy(ptr, ann->ann->announce_msg + ann->curr_announce_msg_offset,
			packet_size);

	rc = dev_queue_xmit(skb);

	if (rc == 0) {
		ann->curr_announce_msg_offset += packet_size;

		if (ann->curr_announce_msg_offset == ann->ann->announce_msg_len)
			ann->curr_announce_msg_offset = 0;
	}

	if (0) {
out_err:
		if (skb != 0)
			kfree_skb(skb);
	}

	return rc;
}

int send_announce_qos(struct announce_data *ann)
{
	int rc;
	spin_lock_bh(&(announce_snd_lock));
	rc = send_announce_chunk(ann);
	spin_unlock_bh(&(announce_snd_lock));
	return rc;
}

static void announce_free(struct kref *ref)
{
	struct announce *ann = container_of(ref, struct announce, ref);
	kfree(&(ann->announce_msg));
	kfree(ann);
}

void announce_data_free(struct kref *ref)
{
	struct announce_data *ann = container_of(ref, struct announce_data,
			ref);
	if (ann->ann != 0)
		kref_put(&(ann->ann->ref), announce_free);
	kfree(ann);
}

static void send_announce(struct work_struct *work)
{
	struct announce_data *ann = container_of(to_delayed_work(work),
			struct announce_data, announce_work);
	int reschedule = 0;
	int rc = 0;

	spin_lock_bh(&(announce_snd_lock));

	if (unlikely(ann->dev == 0))
		goto out;
	reschedule = 1;

	if (unlikely(ann->ann == 0 && last_announce == 0))
		goto out;
	if (ann->curr_announce_msg_offset == 0 &&
			unlikely(ann->ann != last_announce)) {
		if (ann->ann != 0)
			kref_put(&(ann->ann->ref), announce_free);
		ann->ann = last_announce;
		kref_get(&(ann->ann->ref));
	}

	rc = send_announce_chunk(ann);

out:
	spin_unlock_bh(&(announce_snd_lock));

	if (rc != 0) {
		struct qos_queue *q = get_queue(ann->dev);
		if (q != 0) {
			qos_enqueue(q, &(ann->rb), QOS_CALLER_ANNOUNCE);
			kref_put(&(q->ref), free_qos);
		}
	}

	if (unlikely(reschedule == 0)) {
		kref_put(&(ann->ref), announce_data_free);
	} else {
		__u64 jiffies = get_jiffies_64();
		int delay;

		ann->scheduled_announce_timer += msecs_to_jiffies(
				ANNOUNCE_SEND_PACKETINTELVAL_MS);

		delay = ann->scheduled_announce_timer - jiffies;
		if (delay < 0)
			delay = 1;

		INIT_DELAYED_WORK(&(ann->announce_work), send_announce);
		schedule_delayed_work(&(ann->announce_work), delay);
	}
}

static struct announce_data *get_announce_by_netdev(struct net_device *dev)
{
	struct list_head *lh = announce_out_list.next;

	while (lh != &announce_out_list) {
		struct announce_data *curr = (struct announce_data *)(
				((char *) lh) -
				offsetof(struct announce_data, lh));

		if (curr->dev == dev)
			return curr;
	}

	return 0;
}

static void announce_send_adddev(struct net_device *dev)
{
	struct announce_data *ann;

	ann = kmalloc(sizeof(struct announce_data), GFP_KERNEL);

	if (unlikely(ann == 0)) {
		printk(KERN_ERR "cor cannot allocate memory for sending "
				"announces");
		return;
	}

	memset(ann, 0, sizeof(struct announce_data));

	kref_init(&(ann->ref));

	dev_hold(dev);
	ann->dev = dev;

	spin_lock_bh(&(announce_snd_lock));
	list_add_tail(&(ann->lh), &announce_out_list);
	spin_unlock_bh(&(announce_snd_lock));

	ann->scheduled_announce_timer = get_jiffies_64();
	INIT_DELAYED_WORK(&(ann->announce_work), send_announce);
	schedule_delayed_work(&(ann->announce_work), 1);
}

static void announce_send_rmdev(struct net_device *dev)
{
	struct announce_data *ann;

	spin_lock_bh(&(announce_snd_lock));

	ann = get_announce_by_netdev(dev);

	if (ann == 0)
		goto out;

	dev_put(ann->dev);
	ann->dev = 0;

out:
	spin_unlock_bh(&(announce_snd_lock));
}

int netdev_notify_func(struct notifier_block *not, unsigned long event,
		void *ptr)
{
	struct net_device *dev = (struct net_device *) ptr;
	int rc;

	switch(event){
	case NETDEV_UP:
		rc = create_queue(dev);
		if (rc == 1)
			return 1;
		announce_send_adddev(dev);
		break;
	case NETDEV_DOWN:
		announce_send_rmdev(dev);
		reset_neighbor_dev(dev);
		destroy_queue(dev);
		break;
	case NETDEV_REBOOT:
	case NETDEV_CHANGE:
	case NETDEV_REGISTER:
	case NETDEV_UNREGISTER:
	case NETDEV_CHANGEMTU:
	case NETDEV_CHANGEADDR:
	case NETDEV_GOING_DOWN:
	case NETDEV_CHANGENAME:
	case NETDEV_FEAT_CHANGE:
	case NETDEV_BONDING_FAILOVER:
		break;
	default:
		return 1;
	}

	return 0;
}

static int set_announce(char *msg, __u32 len)
{
	struct announce *ann = kmalloc(sizeof(struct announce), GFP_KERNEL);

	if (unlikely(ann == 0)) {
		kfree(msg);
		return 1;
	}

	memset(ann, 0, sizeof(struct announce));

	ann->announce_msg = msg;
	ann->announce_msg_len = len;

	kref_init(&(ann->ref));

	spin_lock_bh(&(announce_snd_lock));

	if (last_announce != 0) {
		ann->packet_version = last_announce->packet_version + 1;
		kref_put(&(last_announce->ref), announce_free);
	}

	last_announce = ann;

	spin_unlock_bh(&(announce_snd_lock));

	return 0;
}

static int generate_announce(void)
{
	__u32 addrtypelen = strlen(addrtype);

	__u32 hdr_len = 16;
	__u32 cmd_hdr_len = 8;
	__u32 cmd_len = 2 + 2 + addrtypelen + addrlen;

	__u32 len = hdr_len + cmd_hdr_len + cmd_len;
	__u32 offset = 0;

	char *msg = kmalloc(len, GFP_KERNEL);
	if (unlikely(msg == 0))
		return 1;

	put_u32(msg + offset, 0, 1); /* min_announce_proto_version */
	offset += 4;
	put_u32(msg + offset, 0, 1); /* max_announce_proto_version */
	offset += 4;
	put_u32(msg + offset, 0, 1); /* min_cor_proto_version */
	offset += 4;
	put_u32(msg + offset, 0, 1); /* max_cor_proto_version */
	offset += 4;


	put_u32(msg + offset, NEIGHCMD_ADDADDR, 1); /* command */
	offset += 4;
	put_u32(msg + offset, cmd_len, 1); /* command length */
	offset += 4;

	/* addrtypelen, addrlen */
	put_u16(msg + offset, addrtypelen, 1);
	offset += 2;
	put_u16(msg + offset, addrlen, 1);
	offset += 2;

	/* addrtype, addr */
	memcpy(msg + offset, addrtype, addrtypelen);
	offset += addrtypelen;
	memcpy(msg + offset, addr, addrlen);
	offset += addrlen;

	BUG_ON(offset != len);

	return set_announce(msg, len);
}

int __init cor_neighbor_init(void)
{
	addrlen = 16;

	addr = kmalloc(addrlen, GFP_KERNEL);
	if (unlikely(addr == 0))
		goto error_free2;

	get_random_bytes(addr, addrlen);

	nb_slab = kmem_cache_create("cor_neighbor", sizeof(struct neighbor), 8,
			0, 0);
	announce_in_slab = kmem_cache_create("cor_announce_in",
			sizeof(struct announce_in), 8, 0, 0);

	if (unlikely(generate_announce()))
		goto error_free1;

	memset(&netdev_notify, 0, sizeof(netdev_notify));
	netdev_notify.notifier_call = netdev_notify_func;
	register_netdevice_notifier(&netdev_notify);

	return 0;

error_free1:
	kfree(addr);

error_free2:
	return -ENOMEM;
}

MODULE_LICENSE("GPL");
