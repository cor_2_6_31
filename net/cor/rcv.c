/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/in.h>


#include "cor.h"

static struct work_struct outofbufferspace_work;
DEFINE_SPINLOCK(oobs_sched_lock);
static int outofbufferspace_scheduled;

DEFINE_MUTEX(oobs_lock);

/**
 * buffering space is divided in 4 areas:
 *
 * buffer_init:
 * distributed equally among all conns; shrinks and grows immediately when there
 * are new connections or connections are reset
 *
 * buffer_speed:
 * distributed proportional to speed; shrinks and grows constantly
 *
 * buffer_ata:
 * used to make noise to make traffic analysis harder; may only shrink if data
 * is either send on or "stuck" for a long time
 *
 * buffer_reserve:
 * reserve in case where sudden shrinking causes some connections to contain
 * more old data than allowed by the first 3 buffers. If this area is full, the
 * out-of-memory conn-resetter is triggered
 *
 * Each area commits a certain amount of space to each connection. This is the
 * maximum buffer space a connection is allowed to use. The space of a specific
 * connection is first accounted to buffer_ata. If the buffer space allowed to
 * use is exceeded, the rest is accounted to buffer_speed and then buffer_init.
 * The reserve area will be used last. This should only be the case, if the
 * assigned buffer space of the first 3 areas shrink suddenly. If this area is
 * also used up, connections will be reset.
 */

DEFINE_SPINLOCK(bufferlimits_lock);

int oobs_listlock;
/**
 * If oobs_listlock, buffer_conn_list must not be touched by anything
 * execept outofbufferspace. In this time new conns are inserted to
 * buffer_conn_tmp_list and later moved to buffer_conn_list.
 */
LIST_HEAD(buffer_conn_list);
LIST_HEAD(buffer_conn_tmp_list);

static __u64 bufferassigned_init;
static __u64 bufferassigned_speed;
static __u64 bufferassigned_ata;

static __u64 bufferusage_init;
static __u64 bufferusage_speed;
static __u64 bufferusage_ata;
static __u64 bufferusage_reserve;

DEFINE_SPINLOCK(st_lock);
static struct speedtracker st;

static __u64 desired_bufferusage(__u64 assigned, __u64 usage, __u64 assignlimit,
		__u64 usagelimit)
{
	__u64 ret;
	__u64 load;

	if (unlikely(assignlimit < usagelimit))
		assignlimit = usagelimit;

	if (multiply_div(usage, 9, 10) > usagelimit)
		return multiply_div(usagelimit, 9, 10);

	load = multiply_div(usage, 192, usagelimit);

	/* slow limit increase, fast decrease */
	if (load == 128) {
		ret = assigned;
	} else if (load < 128) {
		if (load == 0)
			return multiply_div(usagelimit, 9, 10);

		if (load < 96)
			load = 96;
		ret = multiply_div(assigned, 128, load);
	} else {
		ret = multiply_div(assigned, 128, load + load - 128);
	}

	if (ret > assignlimit)
		return assignlimit;
	return ret;
}

/* return 65536 == 1byte/HZ */
static __u64 get_speed(struct speedtracker *st, unsigned long jiffies_tmp)
{
	if (unlikely(time_after(st->jiffies_last_update, jiffies_tmp) ||
			time_before(st->jiffies_last_update + HZ*10,
			jiffies_tmp))) {
		st->jiffies_last_update = jiffies_tmp;
		st->speed = 0;
		st->bytes_curr = 0;
		return 0;
	}

	for (;time_before(st->jiffies_last_update, jiffies_tmp);
			st->jiffies_last_update++) {
		st->speed = ( st->speed * (HZ*9-1) +
				(((__u64)st->bytes_curr)<<16) ) /
				(HZ*10);
		st->bytes_curr = 0;
	}

	return ( st->speed * (HZ*9 - 1) + (((__u64)st->bytes_curr) << 17) ) /
			(HZ*10);
}

/**
 * val1[0], val2[2], res[0] ... least significant
 * val1[val1len-1], val2[val2len-1], res[reslen-1] ... most significant
 */
static void mul(__u32 *val1, unsigned int val1len, __u32 *val2,
		unsigned int val2len, __u32 *res, int reslen)
{
	int digits = val1len + val2len;
	__u64 overflow = 0;
	int i;

	BUG_ON(val1len > 0 && val2len > 0 && reslen < digits);

	memset(res, 0, reslen);

	if (val1len == 0 || val2len == 0)
		return;

	for(i=0;i<digits;i++) {
		int idx1;
		res[i] = (__u32) overflow;
		overflow = overflow >> 32;
		for(idx1=0;idx1<val1len && idx1<=i;idx1++) {
			int idx2 = i - idx1;
			__u64 tmpres;

			if (idx2 >= val2len)
				continue;

			tmpres = ((__u64) (val1[idx1])) *
					((__u64) (val2[idx2]));
			overflow += tmpres >> 32;
			tmpres = (tmpres << 32) >> 32;
			if (res[i] + tmpres < res[i])
				overflow++;
			res[i] += tmpres;
		}
	}
	BUG_ON(overflow != 0);
}

/**
 * return values:
 * 1 == usage1/speed1 offends more
 * 0 == both offend the same
 * -1 == usage2/speed2 offends more
 */
static int compare_scores(__u32 usage1, __u64 speed1, __u32 usage2,
		__u64 speed2)
{
	int i;

	__u32 speed1squared[4];
	__u32 speed2squared[4];

	__u32 speed1squared_usage2[5];
	__u32 speed2squared_usage1[5];


	__u32 speed1_tmp[2];
	__u32 speed2_tmp[2];

	speed1_tmp[0] = (speed1 << 32) >> 32;
	speed1_tmp[1] = (speed1 >> 32);
	speed2_tmp[0] = (speed2 << 32) >> 32;
	speed2_tmp[1] = (speed2 << 32);

	mul(speed1_tmp, 2, speed1_tmp, 2, speed1squared,4);
	mul(speed2_tmp, 2, speed2_tmp, 2, speed2squared, 4);

	mul(speed1squared, 4, &usage2, 1, speed1squared_usage2, 5);
	mul(speed2squared, 4, &usage2, 1, speed2squared_usage1, 5);

	for(i=4;i>=0;i++) {
		if (speed1squared_usage2[i] > speed2squared_usage1[i])
			return -1;
		if (speed1squared_usage2[i] < speed2squared_usage1[i])
			return 1;
	}

	return 0;
}

#define OOBS_SIZE 10
static void _outofbufferspace(void)
{
	int i;

	struct list_head *curr;
	struct conn *offendingconns[OOBS_SIZE];
	__u32 offendingusage[OOBS_SIZE];
	__u64 offendingspeed[OOBS_SIZE];

	memset(&offendingconns, 0, sizeof(offendingconns));

	spin_lock_bh(&bufferlimits_lock);
	BUG_ON(oobs_listlock == 1);
	oobs_listlock = 1;
	spin_unlock_bh(&bufferlimits_lock);

	curr = buffer_conn_list.next;
	while (curr != &buffer_conn_list) {
		unsigned long iflags;

		struct conn *src_in_o = container_of(curr, struct conn,
				source.in.buffer_list);

		__u32 usage;
		__u64 speed;

		int i;

		curr = curr->next;

		spin_lock_bh(&(src_in_o->rcv_lock));

		BUG_ON(src_in_o->sourcetype != SOURCE_IN);

		usage = src_in_o->source.in.usage_reserve;

		spin_lock_irqsave(&st_lock, iflags);
		speed = get_speed(&(src_in_o->source.in.st), jiffies);
		spin_unlock_irqrestore(&st_lock, iflags);

		spin_unlock_bh(&(src_in_o->rcv_lock));

		if (offendingconns[OOBS_SIZE-1] != 0 &&
				compare_scores(
				offendingusage[OOBS_SIZE-1],
				offendingspeed[OOBS_SIZE-1],
				usage, speed) >= 0)
			continue;

		offendingconns[OOBS_SIZE-1] = src_in_o;
		offendingusage[OOBS_SIZE-1] = usage;
		offendingspeed[OOBS_SIZE-1] = speed;

		for (i=OOBS_SIZE-2;i>=0;i++) {
			struct conn *tmpconn;
			__u32 usage_tmp;
			__u64 speed_tmp;

			if (offendingconns[i] != 0 && compare_scores(
					offendingusage[i], offendingspeed[i],
					offendingusage[i+1],
					offendingspeed[i+1]) >= 0)
				break;

			tmpconn = offendingconns[i];
			usage_tmp = offendingusage[i];
			speed_tmp = offendingspeed[i];

			offendingconns[i] = offendingconns[i+1];
			offendingusage[i] = offendingusage[i+1];
			offendingspeed[i] = offendingspeed[i+1];

			offendingconns[i+1] = tmpconn;
			offendingusage[i+1] = usage_tmp;
			offendingspeed[i+1] = speed_tmp;
		}
	}

	for (i=0;i<OOBS_SIZE;i++) {
		if (offendingconns[i] == 0)
			break;
		kref_get(&(offendingconns[i]->ref));
	}

	spin_lock_bh(&bufferlimits_lock);
	BUG_ON(oobs_listlock == 0);
	oobs_listlock = 0;
	spin_unlock_bh(&bufferlimits_lock);

	for (i=0;i<OOBS_SIZE;i++) {
		int resetneeded;

		if (offendingconns[i] == 0)
			break;

		spin_lock_bh(&bufferlimits_lock);
		resetneeded = ((bufferusage_reserve*4)/3 > BUFFERSPACE_RESERVE);
		spin_unlock_bh(&bufferlimits_lock);

		if (resetneeded)
			reset_conn(offendingconns[i]);
		kref_put(&(offendingconns[i]->ref), free_conn);
	}

	spin_lock_bh(&bufferlimits_lock);
	BUG_ON(oobs_listlock == 1);
	for (i=0;i<1000 && list_empty(&buffer_conn_tmp_list) == 0;i++) {
		curr = buffer_conn_tmp_list.next;
		list_del(curr);
		list_add(curr, &buffer_conn_list);
	}
	spin_unlock_bh(&bufferlimits_lock);
}

static void outofbufferspace(struct work_struct *work)
{
	mutex_lock(&oobs_lock);
	while (1) {
		unsigned long iflags;
		int resetneeded;

		spin_lock_bh(&bufferlimits_lock);
		spin_lock_irqsave(&oobs_sched_lock, iflags);
		resetneeded = (bufferusage_reserve > BUFFERSPACE_RESERVE);

		if (resetneeded == 0)
			outofbufferspace_scheduled = 0;

		spin_unlock_irqrestore(&oobs_sched_lock, iflags);
		spin_unlock_bh(&bufferlimits_lock);

		if (resetneeded == 0)
			break;

		_outofbufferspace();
	}
	mutex_unlock(&oobs_lock);
}

static void refresh_bufferusage(struct conn *src_in_l)
{
	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	bufferusage_init -= src_in_l->source.in.usage_init;
	bufferusage_speed -= src_in_l->source.in.usage_speed;
	bufferusage_ata -= src_in_l->source.in.usage_ata;
	bufferusage_reserve -= src_in_l->source.in.usage_reserve;

	src_in_l->source.in.usage_ata = src_in_l->data_buf.totalsize;
	if (src_in_l->source.in.usage_ata > src_in_l->source.in.buffer_ata)
		src_in_l->source.in.usage_ata = src_in_l->source.in.buffer_ata;


	if (src_in_l->source.in.usage_ata == src_in_l->data_buf.totalsize)
		src_in_l->source.in.usage_speed = 0;
	else
		src_in_l->source.in.usage_speed = src_in_l->data_buf.totalsize -
				src_in_l->source.in.usage_ata;

	if (src_in_l->source.in.usage_speed > src_in_l->source.in.buffer_speed)
		src_in_l->source.in.usage_speed =
				src_in_l->source.in.buffer_speed;


	if ((src_in_l->source.in.usage_ata + src_in_l->source.in.usage_speed) ==
			src_in_l->data_buf.totalsize)
		src_in_l->source.in.usage_init = 0;
	else
		src_in_l->source.in.usage_init = src_in_l->data_buf.totalsize -
				src_in_l->source.in.usage_ata -
				src_in_l->source.in.usage_speed;

	if (src_in_l->source.in.usage_init > src_in_l->source.in.buffer_init)
		src_in_l->source.in.usage_init =
				src_in_l->source.in.buffer_init;


	if ((src_in_l->source.in.usage_ata + src_in_l->source.in.usage_speed +
			src_in_l->source.in.usage_init) ==
			src_in_l->data_buf.totalsize)
		src_in_l->source.in.usage_reserve = 0;
	else
		src_in_l->source.in.usage_reserve =
				src_in_l->data_buf.totalsize -
				src_in_l->source.in.usage_ata -
				src_in_l->source.in.usage_speed -
				src_in_l->source.in.usage_init;

	bufferusage_init += src_in_l->source.in.usage_init;
	bufferusage_speed += src_in_l->source.in.usage_speed;
	bufferusage_ata += src_in_l->source.in.usage_ata;
	bufferusage_reserve += src_in_l->source.in.usage_reserve;

	if (bufferusage_reserve > BUFFERSPACE_RESERVE) {
		unsigned long iflags;
		spin_lock_irqsave(&oobs_sched_lock, iflags);
		if (outofbufferspace_scheduled == 0) {
			schedule_work(&outofbufferspace_work);
			outofbufferspace_scheduled = 1;
		}

		spin_unlock_irqrestore(&oobs_sched_lock, iflags);
	}
}

static __u8 __get_window(struct conn *src_in_l)
{
	__u64 window = 0;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	if (src_in_l->source.in.usage_reserve != 0)
		return 0;

	BUG_ON(src_in_l->source.in.usage_init >
			src_in_l->source.in.buffer_init);
	BUG_ON(src_in_l->source.in.usage_speed >
			src_in_l->source.in.buffer_speed);
	BUG_ON(src_in_l->source.in.usage_ata > src_in_l->source.in.buffer_ata);

	window += src_in_l->source.in.buffer_init;
	window += src_in_l->source.in.buffer_speed;
	window += src_in_l->source.in.buffer_ata;

	window -= src_in_l->source.in.usage_init;
	window -= src_in_l->source.in.usage_speed;
	window -= src_in_l->source.in.usage_ata;

	if (window > MAX_ANNOUNCE_WINDOW)
		window = MAX_ANNOUNCE_WINDOW;

	return enc_log_64_11(window);
}

#warning todo upper buffer limits
static __u8 _get_window(struct conn *cn, struct neighbor *expectedsender,
		__u32 expected_connid, int from_acksend)
{
	unsigned long iflags;

	unsigned long jiffies_tmp;

	__u8 window = 0;

	__s32 conns;
	__u64 bufferlimit_init;
	__u64 connlimit_init;

	__u64 totalspeed;
	__u64 bufferlimit_speed;
	__u64 connlimit_speed;

	spin_lock_bh(&(cn->rcv_lock));

	BUG_ON(expectedsender == 0 && cn->sourcetype != SOURCE_IN);

	if (unlikely(unlikely(cn->sourcetype != SOURCE_IN) ||
			unlikely(expectedsender != 0 && (cn->source.in.nb !=
			expectedsender || cn->reversedir->target.out.conn_id !=
			expected_connid))))
		goto out;

	if (unlikely(cn->isreset != 0)) {
		if (oobs_listlock == 0 && (
				cn->source.in.buffer_list.next != 0 ||
				cn->source.in.buffer_list.prev != 0)) {
			list_del(&(cn->source.in.buffer_list));
			cn->source.in.buffer_list.next = 0;
			cn->source.in.buffer_list.prev = 0;
			kref_put(&(cn->ref), free_conn);
		}
		goto out;
	}

	if (oobs_listlock == 0){
		if (cn->source.in.buffer_list.next != 0 ||
				cn->source.in.buffer_list.prev != 0) {
			list_del(&(cn->source.in.buffer_list));
		} else {
			kref_get(&(cn->ref));
		}
		list_add_tail(&(cn->source.in.buffer_list),
				&buffer_conn_list);
	} else  if (cn->source.in.buffer_list.next == 0 &&
			cn->source.in.buffer_list.prev == 0) {
		kref_get(&(cn->ref));
		list_add_tail(&(cn->source.in.buffer_list),
				&buffer_conn_tmp_list);
	}


	conns = atomic_read(&num_conns);
	BUG_ON(conns < 0);
	bufferlimit_init = desired_bufferusage(bufferassigned_init,
			bufferusage_init, BUFFERASSIGN_INIT, BUFFERSPACE_INIT);
	connlimit_init = (bufferlimit_init + conns - 1) / conns;

	bufferassigned_init -= cn->source.in.buffer_init;
	if (((__u32) connlimit_init) != connlimit_init)
		cn->source.in.buffer_init = -1;
	else
		cn->source.in.buffer_init = (__u32) connlimit_init;
	bufferassigned_init += cn->source.in.buffer_init;

	spin_lock_irqsave(&st_lock, iflags);
	jiffies_tmp = jiffies;
	totalspeed = get_speed(&st, jiffies_tmp);
	bufferlimit_speed = desired_bufferusage(bufferassigned_speed,
			bufferusage_speed, BUFFERASSIGN_SPEED,
			BUFFERSPACE_SPEED);
	connlimit_speed = multiply_div(bufferlimit_speed,
			get_speed(&(cn->source.in.st), jiffies_tmp),
			totalspeed);
	spin_unlock_irqrestore(&st_lock, iflags);

	bufferassigned_speed -= cn->source.in.buffer_speed;
	if (((__u32) connlimit_speed) != connlimit_speed)
		cn->source.in.buffer_speed = -1;
	else
		cn->source.in.buffer_speed = (__u32) connlimit_speed;
	bufferassigned_speed += cn->source.in.buffer_speed;

	refresh_bufferusage(cn);

	window = __get_window(cn);

	cn->source.in.window_seqnolimit = cn->source.in.next_seqno +
			dec_log_64_11(window);

	if (from_acksend)
		cn->source.in.window_seqnolimit_remote =
				cn->source.in.window_seqnolimit;
	else
		send_ack_conn_ifneeded(cn);

out:
	spin_unlock_bh(&(cn->rcv_lock));

	return window;
}

__u8 get_window(struct conn *cn, struct neighbor *expectedsender,
		__u32 expected_connid, int from_acksend)
{
	struct conn *cn2;

	__u8 window;

	spin_lock_bh(&bufferlimits_lock);

	window = _get_window(cn, expectedsender, expected_connid, from_acksend);

	if (oobs_listlock == 0) {
		/**
		 * refresh window of idle conns as well to keep global counters
		 * accurate
		 */

		cn2 = container_of(buffer_conn_list.next, struct conn,
				source.in.buffer_list);

		if (list_empty(&buffer_conn_list) == 0 && cn2 != cn)
			_get_window(cn2, 0, 0, 0);


		if (list_empty(&buffer_conn_tmp_list) == 0) {
			cn2 = container_of(buffer_conn_tmp_list.next,
					struct conn, source.in.buffer_list);
			BUG_ON(cn2 == cn);
			_get_window(cn2, 0, 0, 0);
		}
	}

	spin_unlock_bh(&bufferlimits_lock);

	return window;
}

void reset_bufferusage(struct conn *cn)
{
	spin_lock_bh(&bufferlimits_lock);
	spin_lock_bh(&(cn->rcv_lock));

	if (cn->sourcetype != SOURCE_IN)
		goto out;

	bufferusage_init -= cn->source.in.usage_init;
	bufferusage_speed -= cn->source.in.usage_speed;
	bufferusage_ata -= cn->source.in.usage_ata;
	bufferusage_reserve -= cn->source.in.usage_reserve;

	bufferassigned_init -= cn->source.in.buffer_init;
	bufferassigned_speed -= cn->source.in.buffer_speed;
	bufferassigned_ata -= cn->source.in.buffer_ata;

	if (oobs_listlock == 0 && (cn->source.in.buffer_list.next != 0 ||
			cn->source.in.buffer_list.prev != 0)) {
		list_del(&(cn->source.in.buffer_list));
		cn->source.in.buffer_list.next = 0;
		cn->source.in.buffer_list.prev = 0;
		kref_put(&(cn->ref), free_conn);
	}

out:
	spin_unlock_bh(&(cn->rcv_lock));
	spin_unlock_bh(&bufferlimits_lock);
}

void refresh_speedstat(struct conn *src_in_l, __u32 written)
{
	unsigned long iflags;
	unsigned long jiffies_tmp;

	spin_lock_irqsave(&st_lock, iflags);

	jiffies_tmp = jiffies;

	if (src_in_l->source.in.st.jiffies_last_update != jiffies_tmp)
		get_speed(&(src_in_l->source.in.st), jiffies_tmp);
	if (src_in_l->source.in.st.bytes_curr + written < written)
		src_in_l->source.in.st.bytes_curr = -1;
	else
		src_in_l->source.in.st.bytes_curr += written;

	if (st.jiffies_last_update != jiffies_tmp)
		get_speed(&st, jiffies_tmp);
	if (st.bytes_curr + written < written)
		st.bytes_curr = -1;
	else
		st.bytes_curr += written;

	spin_unlock_irqrestore(&st_lock, iflags);
}

void reset_ooo_queue(struct conn *src_in_l)
{
	struct sk_buff *skb;

	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	skb = src_in_l->source.in.reorder_queue.next;

	while ((void *) skb != (void *) &(src_in_l->source.in.reorder_queue)) {
		struct skb_procstate *ps = skb_pstate(skb);
		int drop;

		if (src_in_l->source.in.next_seqno != ps->funcstate.rcv.seqno)
			break;

		drop = receive_skb(src_in_l, skb);
		if (drop)
			break;

		skb_unlink(skb, &(src_in_l->source.in.reorder_queue));
		src_in_l->source.in.next_seqno += skb->len;
	}
}

#warning todo overlapping seqno rcv
void drain_ooo_queue(struct conn *src_in_l)
{
	BUG_ON(src_in_l->sourcetype != SOURCE_IN);

	while (skb_queue_empty(&(src_in_l->source.in.reorder_queue)) == 0) {
		struct sk_buff *skb = src_in_l->source.in.reorder_queue.next;
		skb_unlink(skb, &(src_in_l->source.in.reorder_queue));
		kfree_skb(skb);
	}
}

static int _conn_rcv_ooo(struct conn *src_in_l, struct sk_buff *skb)
{
	struct skb_procstate *ps = skb_pstate(skb);
	struct sk_buff_head *reorder_queue =
			&(src_in_l->source.in.reorder_queue);
	struct sk_buff *curr = reorder_queue->next;

	#warning todo min rcv size
	while (1) {
		struct skb_procstate *ps2 = skb_pstate(curr);

		if ((void *) curr == (void *) reorder_queue) {
			skb_queue_tail(reorder_queue, skb);
			break;
		}

		if (ps->funcstate.rcv.seqno > ps2->funcstate.rcv.seqno) {
			skb_insert(curr, skb, reorder_queue);
			break;
		}

		curr = curr->next;
	}

	return 0;
}

static void _conn_rcv(struct neighbor *nb, struct conn *src_in,
		struct sk_buff *skb, __u32 conn_id)
{
	struct skb_procstate *ps = skb_pstate(skb);

	int in_order;
	int drop = 1;

	__u32 len = skb->len;

	spin_lock_bh(&(src_in->rcv_lock));

	if (unlikely(unlikely(src_in->isreset != 0) ||
			unlikely(src_in->sourcetype != SOURCE_IN) ||
			unlikely(src_in->source.in.conn_id != conn_id)))
		goto out;

	if (nb == 0) {
		if (unlikely(is_from_nb(skb, src_in->source.in.nb) == 0))
			goto out;
	} else {
		if (unlikely(src_in->source.in.nb != nb))
			goto out;
	}

	set_last_act(src_in);

	if (((__s32) (ps->funcstate.rcv.seqno + skb->len -
			src_in->source.in.window_seqnolimit)) > 0)
		goto out;

	in_order = (src_in->source.in.next_seqno == ps->funcstate.rcv.seqno);
	if (in_order == 0) {
		drop = _conn_rcv_ooo(src_in, skb);
	} else {
		src_in->source.in.next_seqno += skb->len;
		drop = receive_skb(src_in, skb);
	}

	if (drop)
		goto out;

	if (in_order == 0) {
		struct control_msg_out *cm =
				alloc_control_msg(src_in->source.in.nb,
				ACM_PRIORITY_LOW);
		send_ack_conn_ooo(cm, src_in,
				src_in->reversedir->target.out.conn_id,
				ps->funcstate.rcv.seqno, len);
	} else {
		drain_ooo_queue(src_in);
		src_in->source.in.inorder_ack_needed = 1;
	}

out:
	send_ack_conn_ifneeded(src_in);

	spin_unlock_bh(&(src_in->rcv_lock));

	if (unlikely(drop)) {
		kfree_skb(skb);
	} else if (in_order == 1) {
		flush_buf(src_in);
	}
}

static void conn_rcv(struct neighbor *nb, struct sk_buff *skb, __u32 conn_id,
		__u32 seqno)
{
	struct conn *cn_src_in;
	struct skb_procstate *ps = skb_pstate(skb);

	memset(ps, 0, sizeof(struct skb_procstate));

	ps->funcstate.rcv.seqno = seqno;

	cn_src_in = get_conn(conn_id);

	if (unlikely(cn_src_in == 0)) {
		int nbput = 0;
		printk(KERN_DEBUG "unknown conn_id when receiving: %d",
				conn_id);

		if (nb == 0) {
			nb = get_neigh_by_mac(skb);
			nbput = 1;
		}

		kfree_skb(skb);
		if (likely(nb != 0)) {
			send_connid_unknown(nb, conn_id);
			if (nbput) {
				kref_put(&(nb->ref), neighbor_free);
			}
		}
		return;
	}
	_conn_rcv(nb, cn_src_in, skb, conn_id);
	kref_put(&(cn_src_in->ref), free_conn);
}

void conn_rcv_buildskb(struct neighbor *nb, char *data, __u32 datalen,
		__u32 conn_id, __u32 seqno)
{
	struct sk_buff *skb = alloc_skb(datalen, GFP_ATOMIC);
	char *dst = skb_put(skb, datalen);
	memcpy(dst, data, datalen);
	conn_rcv(nb, skb, conn_id, seqno);
}

static void rcv_data(struct sk_buff *skb)
{
	__u32 conn_id;
	__u32 seqno;

	char *connid_p = cor_pull_skb(skb, 4);
	char *seqno_p = cor_pull_skb(skb, 4);

	/* __u8 rand; */

	((char *)&conn_id)[0] = connid_p[0];
	((char *)&conn_id)[1] = connid_p[1];
	((char *)&conn_id)[2] = connid_p[2];
	((char *)&conn_id)[3] = connid_p[3];

	((char *)&seqno)[0] = seqno_p[0];
	((char *)&seqno)[1] = seqno_p[1];
	((char *)&seqno)[2] = seqno_p[2];
	((char *)&seqno)[3] = seqno_p[3];

	conn_id = be32_to_cpu(conn_id);
	seqno = be32_to_cpu(seqno);

	/* get_random_bytes(&rand, 1);

	if (rand < 64) {
		printk(KERN_ERR "drop %d %d %d %d %d", conn_id, seqno_p[0],
				seqno_p[1], seqno_p[2], seqno_p[3]);
		goto drop;
	} */

	if (conn_id == 0) {
		struct neighbor *nb = get_neigh_by_mac(skb);
		if (unlikely(nb == 0))
			goto drop;
		kernel_packet(nb, skb, seqno);
		kref_put(&(nb->ref), neighbor_free);
	} else {
		conn_rcv(0, skb, conn_id, seqno);
	}

	if (0) {
drop:
		kfree_skb(skb);
	}
}

static int rcv(struct sk_buff *skb, struct net_device *dev,
		struct packet_type *pt, struct net_device *orig_dev)
{
	__u8 packet_type;
	char *packet_type_p;

	if (skb->pkt_type == PACKET_OTHERHOST)
 		goto drop;

	packet_type_p = cor_pull_skb(skb, 1);

	if (unlikely(packet_type_p == 0))
		goto drop;

	packet_type = *packet_type_p;

	if (unlikely(packet_type == PACKET_TYPE_ANNOUNCE)) {
		rcv_announce(skb);
		return NET_RX_SUCCESS;
	}

	if (likely(packet_type == PACKET_TYPE_DATA)) {
		rcv_data(skb);
		return NET_RX_SUCCESS;
	}

	kfree_skb(skb);
	return NET_RX_SUCCESS;

drop:
	kfree_skb(skb);
	return NET_RX_DROP;
}

static struct packet_type ptype_cor = {
	.type = htons(ETH_P_COR),
	.dev = 0,
	.func = rcv
};

int __init cor_rcv_init(void)
{
	oobs_listlock = 0;

	bufferassigned_init = 0;
	bufferassigned_speed = 0;
	bufferassigned_ata = 0;

	bufferusage_init = 0;
	bufferusage_speed = 0;
	bufferusage_ata = 0;
	bufferusage_reserve = 0;

	memset(&st, 0, sizeof(struct speedtracker));

	BUG_ON(sizeof(struct skb_procstate) > 48);
	INIT_WORK(&outofbufferspace_work, outofbufferspace);
	outofbufferspace_scheduled = 0;

	dev_add_pack(&ptype_cor);
	return 0;
}

MODULE_LICENSE("GPL");
