#define MAX_PACKETS_IN_RCVQUEUE 32

#define MAX_TOTAL_OOO_PACKETS 128
#define MAX_TOTAL_OOO_PER_NEIGH 32
#define MAX_TOTAL_OOO_PER_CONN 12

#define MAX_TOTAL_BUFFER_SIZE 256

#define MAX_CMSGS 2048
#define MAX_CMSGS_PER_NEIGH 256
#define GUARANTEED_CMSGS_PER_NEIGH 32

#define MAX_URGENT_CMSGS_PER_NEIGH 8
#define MAX_URGENT_CMSGS_PER_NEIGH_RETRANSALLOW 6

#define CMSG_INTERVAL_MS 100

#define ANNOUNCE_SEND_PACKETINTELVAL_MS 2000

#define PING_ALL_CONNS_TIMEOUT 5000

#define PING_COOKIES_PER_NEIGH 16
#define PING_COOKIES_NOTHROTTLE 3
#define PING_COOKIES_FIFO 5
#define PING_PONGLIMIT 2
#define PING_THROTTLE_LIMIT_MS 1000
#define PING_FORCETIME_MS 100
#define PING_FORCETIME_ACTIVE_MS 5000
#define PING_FORCETIME_ACTIVEIDLE_MS 30000

#define ACTIVEDELAY_INITIAL_MS 300000
#define ACTIVEDELAY_NOCONN_MIN_MS 30000
#define ACTIVEDELAY_NOCONN_MAX_MS 120000

#define INITIAL_TIME_MS 1000
#define PING_SUCCESS_CNT 10

#define NB_STALL_MINPINGS 10
#define NB_STALL_TIME_MS 10000
#define NB_KILL_TIME_MS 30000

#define CREDIT_REFRESHINTERVAL_SEC 30
/* time until half the credits are gone */
/* minimum == 2 h, otherwise int overflow when adding initial_credits */
#define CREDIT_DECAYTIME_HOURS 600
#define CREDIT_EXCHANGETAX_PERMILLE 25

#define BUFFERASSIGN_INIT 4194304
#define BUFFERASSIGN_SPEED 2097152

#define BUFFERSPACE_INIT 1048576
#define BUFFERSPACE_SPEED 1048576
#define BUFFERSPACE_ATA 1048576
#define BUFFERSPACE_RESERVE 1048576

#define MAX_ANNOUNCE_WINDOW 262144

#define LISTNEIGH_RESP_MAXSIZE 2048

#define BUFFERLIMIT_CONNDATA_GLOBAL 65536
#define BUFFERLIMIT_CONNDATA_PERCONN 4096

#define BUFFERLIMIT_SOCK_GLOBAL 262144
#define BUFFERLIMIT_SOCK_USER 131072
#define BUFFERLIMIT_SOCK_SOCK 65536

#define CONN_ACTIVITY_UPDATEINTERVAL_SEC 60
#define CONN_INACTIVITY_TIMEOUT_SEC 3600

#define CONNID_REUSE_RTTS 8
