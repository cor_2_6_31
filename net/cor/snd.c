/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/gfp.h>
#include <linux/jiffies.h>
#include <linux/slab.h>

#include "cor.h"

struct kmem_cache *connretrans_slab;

struct conn_retrans {
	/* timeout_list and conn_list share a single ref */
	struct kref ref;
	struct list_head timeout_list;
	struct list_head conn_list;
	struct htab_entry htab_entry;
	struct conn *trgt_out_o;
	__u32 seqno;
	__u32 length;
	__u8 ackrcvd;
	unsigned long timeout;
};

static void free_connretrans(struct kref *ref)
{
	struct conn_retrans *cr = container_of(ref, struct conn_retrans, ref);
	kmem_cache_free(connretrans_slab, cr);
	kref_put(&(cr->trgt_out_o->ref), free_conn);
}

DEFINE_SPINLOCK(queues_lock);
LIST_HEAD(queues);
struct timer_list qos_resume_timer;
struct tasklet_struct qos_resume_task;
int qos_resume_scheduled;

void free_qos(struct kref *ref)
{
	struct qos_queue *q = container_of(ref, struct qos_queue, ref);
	kfree(q);
}

/* Highest bidder "pays" the credits the second has bid */
static int _resume_conns(struct qos_queue *q)
{
	struct conn *best = 0;
	__u64 bestcredit = 0;
	__u64 secondcredit = 0;

	int rc;

	struct list_head *lh = q->conns_waiting.next;

	while (lh != &(q->conns_waiting)) {
		struct conn *trgt_out_o = container_of(lh, struct conn,
				target.out.rb.lh);
		__u64 credits;

		lh = lh->next;

		refresh_conn_credits(trgt_out_o, 0, 0);

		spin_lock_bh(&(trgt_out_o->rcv_lock));

		BUG_ON(trgt_out_o->targettype != TARGET_OUT);

		if (unlikely(trgt_out_o->isreset != 0)) {
			trgt_out_o->target.out.rb.in_queue = 0;
			list_del(&(trgt_out_o->target.out.rb.lh));
			spin_unlock_bh(&(trgt_out_o->rcv_lock));
			kref_put(&(trgt_out_o->ref), free_conn);

			continue;
		}

		BUG_ON(trgt_out_o->data_buf.read_remaining == 0);

		if (may_alloc_control_msg(trgt_out_o->target.out.nb,
				ACM_PRIORITY_LOW) == 0)
			continue;

		if (trgt_out_o->credits <= 0)
			credits = 0;
		else
			credits = multiply_div(trgt_out_o->credits, 1LL << 24,
					trgt_out_o->data_buf.read_remaining);
		spin_unlock_bh(&(trgt_out_o->rcv_lock));

		if (best == 0 || bestcredit < credits) {
			secondcredit = bestcredit;
			best = trgt_out_o;
			bestcredit = credits;
		} else if (secondcredit < credits) {
			secondcredit = credits;
		}
	}

	if (best == 0)
		return RC_FLUSH_CONN_OUT_OK;

	spin_lock_bh(&(best->rcv_lock));
	rc = flush_out(best, 1, (__u32) (secondcredit >> 32));

	if (rc == RC_FLUSH_CONN_OUT_OK || rc == RC_FLUSH_CONN_OUT_OK_SENT) {
		best->target.out.rb.in_queue = 0;
		list_del(&(best->target.out.rb.lh));
	}
	spin_unlock_bh(&(best->rcv_lock));

	refresh_conn_credits(best, 0, 0);

	if (rc == RC_FLUSH_CONN_OUT_OK_SENT)
		wake_sender(best);

	if (rc == RC_FLUSH_CONN_OUT_OK || rc == RC_FLUSH_CONN_OUT_OK_SENT)
		kref_put(&(best->ref), free_conn);

	return rc;
}

static int resume_conns(struct qos_queue *q)
{
	while (list_empty(&(q->conns_waiting)) == 0) {
		int rc = _resume_conns(q);
		if (rc != RC_FLUSH_CONN_OUT_OK &&
				rc != RC_FLUSH_CONN_OUT_OK_SENT)
			return 1;
	}
	return 0;
}

static int send_retrans(struct neighbor *nb, int fromqos);

static int __qos_resume(struct qos_queue *q, int caller)
{
	unsigned long iflags;
	int rc = 0;
	struct list_head *lh;

	spin_lock_irqsave(&(q->qlock), iflags);

	if (caller == QOS_CALLER_KPACKET)
		lh = &(q->conn_retrans_waiting);
	else if (caller == QOS_CALLER_CONN_RETRANS)
		lh = &(q->kpackets_waiting);
	else if (caller == QOS_CALLER_ANNOUNCE)
		lh = &(q->announce_waiting);
	else
		BUG();

	while (list_empty(lh) == 0) {
		struct list_head *curr = lh->next;
		struct resume_block *rb = container_of(curr,
				struct resume_block, lh);
		rb->in_queue = 0;
		list_del(curr);

		if (caller == QOS_CALLER_KPACKET) {
			struct neighbor *nb = container_of(rb, struct neighbor,
					rb_kp);
			kref_put(&(nb->ref), neighbor_free);
			spin_unlock_irqrestore(&(q->qlock), iflags);
			rc = send_messages(nb, 1);
			kref_put(&(nb->ref), neighbor_free);
			spin_lock_irqsave(&(q->qlock), iflags);
		} else if (caller == QOS_CALLER_CONN_RETRANS) {
			struct neighbor *nb = container_of(rb, struct neighbor,
					rb_cr);
			#warning todo do not send if neighbor is stalled
			kref_put(&(nb->ref), neighbor_free);
			spin_unlock_irqrestore(&(q->qlock), iflags);
			rc = send_retrans(nb, 1);
			kref_put(&(nb->ref), neighbor_free);
			spin_lock_irqsave(&(q->qlock), iflags);
		 } else if (caller == QOS_CALLER_ANNOUNCE) {
			struct announce_data *ann = container_of(rb,
					struct announce_data, rb);
			kref_get(&(ann->ref));
			spin_unlock_irqrestore(&(q->qlock), iflags);
			rc = send_announce_qos(ann);
			kref_put(&(ann->ref), announce_data_free);
			spin_lock_irqsave(&(q->qlock), iflags);
		} else {
			BUG();
		}

		if (rc != 0) {
			if (rb->in_queue == 0) {
				rb->in_queue = 1;
				list_add(curr, lh);
			}
			break;
		}

		if (caller == QOS_CALLER_KPACKET) {
			kref_put(&(container_of(rb, struct neighbor,
					rb_kp)->ref), neighbor_free);
		} else if (caller == QOS_CALLER_CONN_RETRANS) {
			kref_put(&(container_of(rb, struct neighbor,
					rb_cr)->ref), neighbor_free);
		} else if (caller == QOS_CALLER_ANNOUNCE) {
			kref_put(&(container_of(rb,
					struct announce_data, rb)->ref),
					announce_data_free);
		} else {
			BUG();
		}
	}

	spin_unlock_irqrestore(&(q->qlock), iflags);

	return rc;
}

static int _qos_resume(struct qos_queue *q)
{
	int rc = 0;
	unsigned long iflags;
	int i;

	spin_lock_irqsave(&(q->qlock), iflags);

	for (i=0;i<4 && rc == 0;i++) {
		struct list_head *lh;
		int rc;

		if (i == QOS_CALLER_KPACKET)
			lh = &(q->conn_retrans_waiting);
		else if (i == QOS_CALLER_CONN_RETRANS)
			lh = &(q->kpackets_waiting);
		else if (i == QOS_CALLER_ANNOUNCE)
			lh = &(q->announce_waiting);
		else if (i == QOS_CALLER_CONN)
			lh = &(q->conns_waiting);
		else
			BUG();

		if (list_empty(lh))
			continue;

		spin_unlock_irqrestore(&(q->qlock), iflags);
		if (i == QOS_CALLER_CONN)
			rc = resume_conns(q);
		else
			rc = __qos_resume(q, i);
		spin_lock_irqsave(&(q->qlock), iflags);

		i = 0;
	}

	spin_unlock_irqrestore(&(q->qlock), iflags);

	return rc;
}

void qos_resume_taskfunc(unsigned long arg)
{
	struct list_head *curr;

	int congested = 0;

	spin_lock_bh(&(queues_lock));

	curr = queues.next;
	while (curr != (&queues)) {
		struct qos_queue *q = container_of(curr,
				struct qos_queue, queue_list);

		if (_qos_resume(q))
			congested = 1;

		curr = curr->next;
	}

	if (congested) {
		mod_timer(&(qos_resume_timer), jiffies + 1);
	} else {
		qos_resume_scheduled = 0;
	}

	spin_unlock_bh(&(queues_lock));
}

void qos_resume_timerfunc(unsigned long arg)
{
	tasklet_schedule(&qos_resume_task);
}

struct qos_queue *get_queue(struct net_device *dev)
{
	struct qos_queue *ret = 0;
	struct list_head *curr;

	spin_lock_bh(&(queues_lock));
	curr = queues.next;
	while (curr != (&queues)) {
		struct qos_queue *q = container_of(curr,
				struct qos_queue, queue_list);
		if (q->dev == dev) {
			ret = q;
			break;
		}
	}
	spin_unlock_bh(&(queues_lock));
	return ret;
}

static void _destroy_queue(struct qos_queue *q, int caller)
{
	struct list_head *lh;

	if (caller == QOS_CALLER_KPACKET)
		lh = &(q->conn_retrans_waiting);
	else if (caller == QOS_CALLER_CONN_RETRANS)
		lh = &(q->kpackets_waiting);
	else if (caller == QOS_CALLER_ANNOUNCE)
		lh = &(q->announce_waiting);
	else
		BUG();

	while (list_empty(lh) == 0) {
		struct list_head *curr = lh->next;
		struct resume_block *rb = container_of(curr,
				struct resume_block, lh);
		rb->in_queue = 0;
		list_del(curr);

		if (caller == QOS_CALLER_KPACKET) {
			kref_put(&(container_of(rb, struct neighbor,
					rb_kp)->ref), neighbor_free);
		} else if (caller == QOS_CALLER_CONN_RETRANS) {
			kref_put(&(container_of(rb, struct neighbor,
					rb_cr)->ref), neighbor_free);
		} else if (caller == QOS_CALLER_ANNOUNCE) {
			kref_put(&(container_of(rb,
					struct announce_data, rb)->ref),
					announce_data_free);
		} else {
			BUG();
		}
	}
}

int destroy_queue(struct net_device *dev)
{
	int unlink;
	unsigned long iflags;
	struct qos_queue *q = get_queue(dev);
	if (q == 0)
		return 1;

	spin_lock_irqsave(&(q->qlock), iflags);
	unlink = (q->dev != 0);
	q->dev = 0;
	_destroy_queue(q, QOS_CALLER_KPACKET);
	_destroy_queue(q, QOS_CALLER_CONN_RETRANS);
	_destroy_queue(q, QOS_CALLER_ANNOUNCE);
	spin_unlock_irqrestore(&(q->qlock), iflags);

	if (unlink) {
		dev_put(dev);
		spin_lock_bh(&(queues_lock));
		list_del(&(q->queue_list));
		spin_unlock_bh(&(queues_lock));
		kref_put(&(q->ref), free_qos);
	}
	kref_put(&(q->ref), free_qos);

	return 0;
}

int create_queue(struct net_device *dev)
{
	struct qos_queue *q = kmalloc(sizeof(struct qos_queue), GFP_KERNEL);

	if (q == 0) {
		printk(KERN_ERR "cor: unable to allocate memory for device "
				"queue, not enabling device");
		return 1;
	}

	spin_lock_init(&(q->qlock));

	q->dev = dev;
	dev_hold(dev);

	INIT_LIST_HEAD(&(q->kpackets_waiting));
	INIT_LIST_HEAD(&(q->conn_retrans_waiting));
	INIT_LIST_HEAD(&(q->announce_waiting));
	INIT_LIST_HEAD(&(q->conns_waiting));

	spin_lock_bh(&(queues_lock));
	list_add(&(q->queue_list), &queues);
	spin_unlock_bh(&(queues_lock));

	return 0;
}

void qos_enqueue(struct qos_queue *q, struct resume_block *rb, int caller)
{
	unsigned long iflags;

	spin_lock_irqsave(&(q->qlock), iflags);

	if (rb->in_queue)
		goto out;

	rb->in_queue = 1;

	if (caller == QOS_CALLER_KPACKET) {
		list_add(&(rb->lh) , &(q->conn_retrans_waiting));
		kref_get(&(container_of(rb, struct neighbor, rb_kp)->ref));
	} else if (caller == QOS_CALLER_CONN_RETRANS) {
		list_add(&(rb->lh), &(q->kpackets_waiting));
		kref_get(&(container_of(rb, struct neighbor, rb_cr)->ref));
	} else if (caller == QOS_CALLER_ANNOUNCE) {
		list_add(&(rb->lh), &(q->announce_waiting));
		kref_get(&(container_of(rb, struct announce_data, rb)->ref));
	} else if (caller == QOS_CALLER_CONN) {
		list_add(&(rb->lh), &(q->conns_waiting));
		kref_get(&(container_of(rb, struct conn, target.out.rb)->ref));
	} else {
		BUG();
	}

	if (qos_resume_scheduled == 0) {
		mod_timer(&(qos_resume_timer), jiffies + 1);
		qos_resume_scheduled = 1;
	}

out:
	spin_unlock_irqrestore(&(q->qlock), iflags);
}

void qos_remove_conn(struct conn *trgt_out_l)
{
	unsigned long iflags;
	struct qos_queue *q;

	BUG_ON(trgt_out_l->targettype != TARGET_OUT);

	if (trgt_out_l->target.out.rb.in_queue == 0)
		return;

	q = trgt_out_l->target.out.nb->queue;
	BUG_ON(q == 0);

	trgt_out_l->target.out.rb.in_queue = 0;
	spin_lock_irqsave(&(q->qlock), iflags);
	list_del(&(trgt_out_l->target.out.rb.lh));
	spin_unlock_irqrestore(&(q->qlock), iflags);

	kref_put(&(trgt_out_l->ref), free_conn);
}

static void qos_enqueue_conn(struct conn *trgt_out_l)
{
	qos_enqueue(trgt_out_l->target.out.nb->queue,
			&(trgt_out_l->target.out.rb), QOS_CALLER_CONN);
}

static int may_send_conn_retrans(struct neighbor *nb)
{
	unsigned long iflags;
	int rc;

	BUG_ON(nb->queue == 0);

	spin_lock_irqsave(&(nb->queue->qlock), iflags);
	rc = (list_empty(&(nb->queue->kpackets_waiting)));
	spin_unlock_irqrestore(&(nb->queue->qlock), iflags);

	return rc;
}

static int may_send_conn(struct conn *trgt_out_l)
{
	unsigned long iflags;
	struct qos_queue *q = trgt_out_l->target.out.nb->queue;
	int rc;

	BUG_ON(q == 0);

	spin_lock_irqsave(&(q->qlock), iflags);
	rc = (list_empty(&(q->kpackets_waiting)) &&
			list_empty(&(q->conn_retrans_waiting)) &&
			list_empty(&(q->announce_waiting)) &&
			list_empty(&(q->conns_waiting)));
	spin_unlock_irqrestore(&(q->qlock), iflags);

	return rc;
}


struct sk_buff *create_packet(struct neighbor *nb, int size,
		gfp_t alloc_flags, __u32 conn_id, __u32 seqno)
{
	struct sk_buff *ret;
	char *dest;

	ret = alloc_skb(size + 9 + LL_ALLOCATED_SPACE(nb->dev), alloc_flags);
	if (unlikely(0 == ret))
		return 0;

	ret->protocol = htons(ETH_P_COR);
	ret->dev = nb->dev;

	skb_reserve(ret, LL_RESERVED_SPACE(nb->dev));
	if(unlikely(dev_hard_header(ret, nb->dev, ETH_P_COR, nb->mac,
			nb->dev->dev_addr, ret->len) < 0))
		return 0;
	skb_reset_network_header(ret);

	dest = skb_put(ret, 9);
	BUG_ON(0 == dest);

	dest[0] = PACKET_TYPE_DATA;
	dest += 1;

	put_u32(dest, conn_id, 1);
	dest += 4;
	put_u32(dest, seqno, 1);
	dest += 4;

	return ret;
}

static void set_conn_retrans_timeout(struct conn_retrans *cr)
{
	struct neighbor *nb = cr->trgt_out_o->target.out.nb;
	cr->timeout = jiffies + usecs_to_jiffies(100000 +
			((__u32) atomic_read(&(nb->latency))) +
			((__u32) atomic_read(&(nb->max_remote_cmsg_delay))));
}

static struct conn_retrans *readd_conn_retrans(struct conn_retrans *cr,
		struct neighbor *nb, __u32 length, int *dontsend)
{
	struct conn_retrans *ret = 0;

	spin_lock_bh(&(nb->retrans_lock));

	if (unlikely(cr->ackrcvd)) {
		*dontsend = 1;
		goto out;
	} else
		*dontsend = 0;

	if (unlikely(cr->length > length)) {
		ret = kmem_cache_alloc(connretrans_slab, GFP_ATOMIC);
		if (unlikely(ret == 0)) {
			cr->timeout = jiffies + 1;
			goto out;
		}

		memset(ret, 0, sizeof (struct conn_retrans));
		ret->trgt_out_o = cr->trgt_out_o;
		kref_get(&(cr->trgt_out_o->ref));
		ret->seqno = cr->seqno + length;
		ret->length = cr->length - length;
		kref_init(&(ret->ref));

		list_add(&(ret->timeout_list), &(nb->retrans_list_conn));
		list_add(&(ret->conn_list), &(cr->conn_list));

		cr->length = length;
	} else {
		list_del(&(cr->timeout_list));
		list_add_tail(&(cr->timeout_list), &(nb->retrans_list_conn));
		set_conn_retrans_timeout(cr);

		BUG_ON(cr->length != length);
	}

out:
	spin_unlock_bh(&(nb->retrans_lock));

	return ret;
}

void cancel_retrans(struct conn *trgt_out_l)
{
	struct neighbor *nb = trgt_out_l->target.out.nb;

	spin_lock_bh(&(nb->retrans_lock));

	while (list_empty(&(trgt_out_l->target.out.retrans_list)) == 0) {
		struct conn_retrans *cr = container_of(
				trgt_out_l->target.out.retrans_list.next,
				struct conn_retrans, conn_list);
		BUG_ON(cr->trgt_out_o != trgt_out_l);

		list_del(&(cr->timeout_list));
		list_del(&(cr->conn_list));
		cr->ackrcvd = 1;
		kref_put(&(cr->ref), free_connretrans);
	}
	#warning reschedule timer

	spin_unlock_bh(&(nb->retrans_lock));
}

static int _send_retrans(struct neighbor *nb, struct conn_retrans *cr)
{
	int targetmss = mss(nb);
	int dontsend;
	int queuefull = 0;

	spin_lock_bh(&(cr->trgt_out_o->rcv_lock));

	BUG_ON(cr->trgt_out_o->targettype != TARGET_OUT);
	BUG_ON(cr->trgt_out_o->target.out.nb != nb);

	kref_get(&(cr->trgt_out_o->ref));

	if (unlikely(cr->trgt_out_o->isreset != 0)) {
		cancel_retrans(cr->trgt_out_o);
		goto out;
	}

	while (cr->length >= targetmss) {
		struct sk_buff *skb;
		char *dst;
		struct conn_retrans *cr2;
		int rc;

		if (may_send_conn_retrans(nb) == 0)
			goto qos_enqueue;

		skb = create_packet(nb, targetmss, GFP_ATOMIC,
				cr->trgt_out_o->target.out.conn_id, cr->seqno);
		if (unlikely(skb == 0)) {
			cr->timeout = jiffies + 1;
			goto out;
		}

		cr2 = readd_conn_retrans(cr, nb, targetmss, &dontsend);
		if (unlikely(unlikely(dontsend) || unlikely(cr2 == 0 &&
				unlikely(cr->length > targetmss)))) {
			kfree_skb(skb);
			goto out;
		}

		dst = skb_put(skb, targetmss);

		databuf_pullold(cr->trgt_out_o, cr->seqno, dst, targetmss);
		rc = dev_queue_xmit(skb);

		if (rc != 0) {
			spin_lock_bh(&(nb->retrans_lock));
			if (unlikely(cr->ackrcvd)) {
				dontsend = 1;
			} else {
				list_del(&(cr->timeout_list));
				list_add(&(cr->timeout_list),
						&(nb->retrans_list_conn));
			}
			spin_unlock_bh(&(nb->retrans_lock));
			if (dontsend == 0)
				goto qos_enqueue;
		}

		cr = cr2;

		if (likely(cr == 0))
			goto out;
	}

	if (unlikely(cr->length <= 0)) {
		BUG();
	} else {
		struct control_msg_out *cm;
		char *buf = kmalloc(cr->length, GFP_ATOMIC);

		if (unlikely(buf == 0)) {
			cr->timeout = jiffies + 1;
			goto out;
		}

		cm = alloc_control_msg(nb, ACM_PRIORITY_LOW);
		if (unlikely(cm == 0)) {
			cr->timeout = jiffies + 1;
			kfree(buf);
			goto out;
		}

		databuf_pullold(cr->trgt_out_o, cr->seqno, buf, cr->length);

		if (unlikely(readd_conn_retrans(cr, nb, cr->length, &dontsend)
				!= 0))
			BUG();

		if (likely(dontsend == 0)) {
			send_conndata(cm, cr->trgt_out_o->target.out.conn_id,
					cr->seqno, buf, buf, cr->length);
		}
	}

	if (0) {
qos_enqueue:
		queuefull = 1;
	}
out:
	spin_unlock_bh(&(cr->trgt_out_o->rcv_lock));

	kref_put(&(cr->trgt_out_o->ref), free_conn);

	return queuefull;
}

static int send_retrans(struct neighbor *nb, int fromqos)
{
	unsigned long iflags;

	struct conn_retrans *cr = 0;

	int nbstate;
	int rescheduled = 0;
	int queuefull = 0;

	spin_lock_irqsave(&(nb->state_lock), iflags);
	nbstate = nb->state;
	spin_unlock_irqrestore(&(nb->state_lock), iflags);

	while (1) {
		spin_lock_bh(&(nb->retrans_lock));

		if (list_empty(&(nb->retrans_list_conn))) {
			nb->retrans_timer_conn_running = 0;
			spin_unlock_bh(&(nb->retrans_lock));
			break;
		}

		cr = container_of(nb->retrans_list_conn.next,
				struct conn_retrans, timeout_list);

		if (unlikely(nbstate == NEIGHBOR_STATE_KILLED)) {
			list_del(&(cr->timeout_list));
			list_del(&(cr->conn_list));
			spin_unlock_bh(&(nb->retrans_lock));

			kref_put(&(cr->ref), free_connretrans);
			continue;
		}

		#warning todo check window limit

		if (time_after(cr->timeout, jiffies)) {
			mod_timer(&(nb->retrans_timer_conn), cr->timeout);
			if (fromqos)
				kref_get(&(nb->ref));
			rescheduled = 1;
			spin_unlock_bh(&(nb->retrans_lock));
			break;
		}

		kref_get(&(cr->ref));
		spin_unlock_bh(&(nb->retrans_lock));
		queuefull = _send_retrans(nb, cr);
		kref_put(&(cr->ref), free_connretrans);
		if (queuefull) {
			if (fromqos == 0)
				qos_enqueue(nb->queue, &(nb->rb_cr),
						QOS_CALLER_CONN_RETRANS);
			break;
		}
	}

	if (rescheduled == 0 && fromqos == 0)
		kref_put(&(nb->ref), neighbor_free);

	return queuefull;
}

void retransmit_conn_taskfunc(unsigned long arg)
{
	struct neighbor *nb = (struct neighbor *) arg;
	send_retrans(nb, 0);
}

void retransmit_conn_timerfunc(unsigned long arg)
{
	struct neighbor *nb = (struct neighbor *) arg;
	tasklet_schedule(&(nb->retrans_task_conn));
}

void conn_ack_ooo_rcvd(struct neighbor *nb, __u32 conn_id,
		struct conn *trgt_out, __u32 seqno_ooo, __u32 length)
{
	struct list_head *curr;

	if (unlikely(length == 0))
		return;

	spin_lock_bh(&(trgt_out->rcv_lock));

	if (unlikely(trgt_out->targettype != TARGET_OUT))
		goto out;
	if (unlikely(trgt_out->target.out.nb != nb))
		goto out;
	if (unlikely(trgt_out->target.out.conn_id != conn_id))
		goto out;

	spin_lock_bh(&(nb->retrans_lock));

	curr = trgt_out->target.out.retrans_list.next;

	while (curr != &(trgt_out->target.out.retrans_list)) {
		struct conn_retrans *cr = container_of(curr,
				struct conn_retrans, conn_list);

		if (((__s32)(cr->seqno + cr->length - seqno_ooo)) > 0)
			goto cont;

		if (((__s32)(cr->seqno + cr->length - seqno_ooo - length)) >0) {
			if (((__s32)(cr->seqno - seqno_ooo - length)) < 0) {
				__u32 newseqno = seqno_ooo + length;
				cr->length -= (newseqno - cr->seqno);
				cr->seqno = newseqno;
			}

			break;
		}

		if (((__s32)(cr->seqno - seqno_ooo)) < 0 &&
				((__s32)(cr->seqno + cr->length - seqno_ooo -
				length)) <= 0) {
			__u32 diff = seqno_ooo + length - cr->seqno -
					cr->length;
			cr->seqno += diff;
			cr->length -= diff;
		} else {
			list_del(&(cr->timeout_list));
			list_del(&(cr->conn_list));
			cr->ackrcvd = 1;
			kref_put(&(cr->ref), free_connretrans);
		}

cont:
		curr = curr->next;
	}

	if (unlikely(list_empty(&(trgt_out->target.out.retrans_list))) == 0) {
		struct conn_retrans *cr = container_of(
				trgt_out->target.out.retrans_list.next,
				struct conn_retrans, conn_list);
		if (unlikely(((__s32) (cr->seqno -
				trgt_out->target.out.seqno_acked)) > 0))
			trgt_out->target.out.seqno_acked = cr->seqno;
	}

	spin_unlock_bh(&(nb->retrans_lock));

out:
	spin_unlock_bh(&(trgt_out->rcv_lock));
}

void conn_ack_rcvd(struct neighbor *nb, __u32 conn_id, struct conn *trgt_out,
		__u32 seqno, int setwindow, __u8 window)
{
	int flush = 0;

	spin_lock_bh(&(trgt_out->rcv_lock));

	if (unlikely(trgt_out->isreset != 0))
		goto out;
	if (unlikely(trgt_out->targettype != TARGET_OUT))
		goto out;
	if (unlikely(trgt_out->target.out.nb != nb))
		goto out;
	if (unlikely(trgt_out->reversedir->source.in.conn_id != conn_id))
		goto out;

	if (unlikely(((__s32)(seqno - trgt_out->target.out.seqno_nextsend)) > 0)
			||
			((__s32)(seqno - trgt_out->target.out.seqno_acked)) < 0)
		goto out;

	if (setwindow) {
		__u32 windowdec = dec_log_64_11(window);
		if (unlikely(seqno == trgt_out->target.out.seqno_acked &&
				((__s32) (seqno + windowdec -
				trgt_out->target.out.seqno_windowlimit )) <= 0))
			goto skipwindow;

		trgt_out->target.out.seqno_windowlimit = seqno + windowdec;
		flush = 1;
	}

skipwindow:
	if (seqno == trgt_out->target.out.seqno_acked)
		goto out;

	spin_lock_bh(&(nb->retrans_lock));

	trgt_out->target.out.seqno_acked = seqno;

	while (list_empty(&(trgt_out->target.out.retrans_list)) == 0) {
		struct conn_retrans *cr = container_of(
				trgt_out->target.out.retrans_list.next,
				struct conn_retrans, conn_list);

		if (((__s32)(cr->seqno + cr->length - seqno)) > 0) {
			if (((__s32)(cr->seqno - seqno)) < 0) {
				cr->length -= (seqno - cr->seqno);
				cr->seqno = seqno;
			}
			break;
		}

		list_del(&(cr->timeout_list));
		list_del(&(cr->conn_list));
		cr->ackrcvd = 1;
		kref_put(&(cr->ref), free_connretrans);
	}

	spin_unlock_bh(&(nb->retrans_lock));
	databuf_ack(trgt_out, trgt_out->target.out.seqno_acked);

out:
	spin_unlock_bh(&(trgt_out->rcv_lock));

	if (flush)
		flush_buf(trgt_out);
}

static void schedule_retransmit_conn(struct conn_retrans *cr,
		struct conn *trgt_out_l, __u32 seqno, __u32 len)
{
	struct neighbor *nb = trgt_out_l->target.out.nb;

	int first;

	BUG_ON(trgt_out_l->targettype != TARGET_OUT);

	memset(cr, 0, sizeof (struct conn_retrans));
	cr->trgt_out_o = trgt_out_l;
	kref_get(&(trgt_out_l->ref));
	cr->seqno = seqno;
	cr->length = len;
	kref_init(&(cr->ref));
	set_conn_retrans_timeout(cr);

	spin_lock_bh(&(nb->retrans_lock));

	first = unlikely(list_empty(&(nb->retrans_list_conn)));
	list_add_tail(&(cr->timeout_list), &(nb->retrans_list_conn));

	list_add_tail(&(cr->conn_list), &(trgt_out_l->target.out.retrans_list));

	if (unlikely(unlikely(first) &&
			unlikely(nb->retrans_timer_conn_running == 0))) {
		mod_timer(&(nb->retrans_timer_conn), cr->timeout);
		nb->retrans_timer_conn_running = 1;
		kref_get(&(nb->ref));
	}

	spin_unlock_bh(&(nb->retrans_lock));
}

static __u32 get_windowlimit(struct conn *trgt_out_l)
{
	__s32 windowlimit = (__s32)(trgt_out_l->target.out.seqno_windowlimit -
			trgt_out_l->target.out.seqno_nextsend);
	if (unlikely(windowlimit < 0))
		return 0;
	return windowlimit;
}

#warning todo reset connections which are SOURCE_NONE and are stuck for too long
int flush_out(struct conn *trgt_out_l, int fromqos, __u32 creditsperbyte)
{
	int targetmss;
	__u32 seqno;
	int sent = 0;

	BUG_ON(trgt_out_l->targettype != TARGET_OUT);

	targetmss = mss(trgt_out_l->target.out.nb);

	if (unlikely(trgt_out_l->target.out.conn_id == 0))
		return RC_FLUSH_CONN_OUT_OK;

	if (unlikely(trgt_out_l->isreset != 0))
		return RC_FLUSH_CONN_OUT_OK;

	if (unlikely(trgt_out_l->sourcetype == SOURCE_SOCK &&
			trgt_out_l->source.sock.delay_flush != 0))
		return RC_FLUSH_CONN_OUT_OK;

	if (fromqos == 0 && may_send_conn(trgt_out_l) == 0) {
		qos_enqueue_conn(trgt_out_l);
		return RC_FLUSH_CONN_OUT_CONG;
	}

	while (trgt_out_l->data_buf.read_remaining >= targetmss &&
			get_windowlimit(trgt_out_l) >= targetmss) {
		struct conn_retrans *cr;
		struct sk_buff *skb;
		char *dst;
		int rc;

		if (unlikely(creditsperbyte * targetmss >
				trgt_out_l->credits))
			return RC_FLUSH_CONN_OUT_CREDITS;

		seqno = trgt_out_l->target.out.seqno_nextsend;
		skb = create_packet(trgt_out_l->target.out.nb, targetmss,
				GFP_ATOMIC, trgt_out_l->target.out.conn_id,
				seqno);
		if (unlikely(skb == 0)) {
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_OOM;
		}

		cr = kmem_cache_alloc(connretrans_slab, GFP_ATOMIC);
		if (unlikely(cr == 0)) {
			kfree_skb(skb);
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_OOM;
		}

		dst = skb_put(skb, targetmss);

		databuf_pull(trgt_out_l, dst, targetmss);

		rc = dev_queue_xmit(skb);
		if (rc != 0) {
			databuf_unpull(trgt_out_l, targetmss);
			kmem_cache_free(connretrans_slab, cr);
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_CONG;
		}

		trgt_out_l->credits -= creditsperbyte * targetmss;
		trgt_out_l->target.out.seqno_nextsend += targetmss;
		schedule_retransmit_conn(cr, trgt_out_l, seqno, targetmss);
		sent = 1;
	}

	if (trgt_out_l->data_buf.read_remaining > 0 && (trgt_out_l->tos ==
			TOS_LATENCY || trgt_out_l->target.out.seqno_nextsend ==
			trgt_out_l->target.out.seqno_acked)) {
		struct control_msg_out *cm;
		struct conn_retrans *cr;
		__u32 len = trgt_out_l->data_buf.read_remaining;
		__s32 windowlimit = get_windowlimit(trgt_out_l);
		char *buf;

		if (windowlimit == 0)
			goto out;

		if (windowlimit < len/2 &&
				trgt_out_l->target.out.seqno_nextsend !=
				trgt_out_l->target.out.seqno_acked)
			goto out;

		if (len > windowlimit)
			len = windowlimit;

		buf = kmalloc(len, GFP_ATOMIC);

		if (unlikely(creditsperbyte * len > trgt_out_l->credits))
			return RC_FLUSH_CONN_OUT_CREDITS;

		if (unlikely(buf == 0)) {
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_OOM;
		}

		cm = alloc_control_msg(trgt_out_l->target.out.nb,
				ACM_PRIORITY_LOW);
		if (unlikely(cm == 0)) {
			kfree(buf);
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_OOM;
		}

		cr = kmem_cache_alloc(connretrans_slab, GFP_ATOMIC);
		if (unlikely(cr == 0)) {
			kfree(buf);
			free_control_msg(cm);
			qos_enqueue_conn(trgt_out_l);
			return RC_FLUSH_CONN_OUT_CONG;
		}

		databuf_pull(trgt_out_l, buf, len);

		seqno = trgt_out_l->target.out.seqno_nextsend;
		trgt_out_l->credits -= creditsperbyte * len;
		trgt_out_l->target.out.seqno_nextsend += len;

		schedule_retransmit_conn(cr, trgt_out_l, seqno, len);

		send_conndata(cm, trgt_out_l->target.out.conn_id, seqno, buf,
				buf, len);
		sent = 1;
	}

out:
	if (sent)
		return RC_FLUSH_CONN_OUT_OK_SENT;

	return RC_FLUSH_CONN_OUT_OK;
}

int __init cor_snd_init(void)
{
	connretrans_slab = kmem_cache_create("cor_connretrans",
			sizeof(struct conn_retrans), 8, 0, 0);

	if (unlikely(connretrans_slab == 0))
		return 1;

	init_timer(&qos_resume_timer);
	qos_resume_timer.function = qos_resume_timerfunc;
	qos_resume_timer.data = 0;
	tasklet_init(&qos_resume_task, qos_resume_taskfunc, 0);

	qos_resume_scheduled = 0;

	return 0;
}

MODULE_LICENSE("GPL");
