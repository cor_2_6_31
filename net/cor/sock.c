/**
 *	Connection oriented routing
 *	Copyright (C) 2007-2011 Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <net/sock.h>
#include <linux/net.h>
#include <asm/uaccess.h>

#include "cor.h"

struct kmem_cache *sock_slab;

/**
 * sock_bt_wait_list and waiting_conns are ordered by min amount first, the
 * order in which resuming will happen
 */
DEFINE_SPINLOCK(sock_bufferlimits_lock);
LIST_HEAD(sock_bt_list);
LIST_HEAD(sock_bt_wait_list);
static __u64 sock_bufferusage;

static struct work_struct outofsockbufferspace_work;
static int outofsockbufferspace_scheduled;

static void free_sbt(struct kref *ref)
{
	struct sock_buffertracker *sbt = container_of(ref,
			struct sock_buffertracker, ref);

	BUG_ON(sbt->usage != 0);
	BUG_ON(list_empty(&(sbt->waiting_conns)) == 0);

	list_del(&(sbt->lh));
	kfree(sbt);
}

static struct sock_buffertracker *get_sock_buffertracker(uid_t uid)
{
	struct sock_buffertracker *sbt;
	struct list_head *curr;

	curr = sock_bt_list.next;
	while (curr != &sock_bt_list) {
		sbt = container_of(curr, struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(sbt->waiting_conns)) == 0);
		if (sbt->uid == uid)
			goto found;
		curr = curr->next;
	}

	curr = sock_bt_wait_list.next;
	while (curr != &sock_bt_wait_list) {
		sbt = container_of(curr, struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(sbt->waiting_conns)));
		if (sbt->uid == uid)
			goto found;
		curr = curr->next;
	}

	sbt = kmalloc(sizeof(struct sock_buffertracker), GFP_ATOMIC);
	if (sbt != 0) {
		memset(sbt, 0, sizeof(struct sock_buffertracker));
		sbt->uid = uid;
		list_add_tail(&(sbt->lh), &sock_bt_list);
		INIT_LIST_HEAD(&(sbt->delflush_conns));
		INIT_LIST_HEAD(&(sbt->waiting_conns));
		kref_init(&(sbt->ref));
	}

	if (0) {
found:
		kref_get(&(sbt->ref));
	}
	return sbt;
}

static void _reserve_sock_buffer_reord_bt(struct sock_buffertracker *sbt,
		int waitingconnremoved)
{
	if (waitingconnremoved && list_empty(&(sbt->waiting_conns))) {
		list_del(&(sbt->lh));
		list_add_tail(&(sbt->lh), &sock_bt_list);
		return;
	}

	if (list_empty(&(sbt->waiting_conns)))
		return;

	while(sbt->lh.next != &sock_bt_wait_list) {
		struct sock_buffertracker *next = container_of(sbt->lh.next,
				struct sock_buffertracker, lh);

		BUG_ON(sbt->lh.next == &sock_bt_list);

		if (sbt->usage <= next->usage)
			break;

		list_del(&(sbt->lh));
		list_add(&(sbt->lh), &(next->lh));
	}
}

static int oosbs_resumesbt(struct sock_buffertracker *sbt)
{
	int restart = 0;
	struct list_head *curr = sbt->delflush_conns.next;

	while (curr != &(sbt->delflush_conns)) {
		struct conn *src_in_o = container_of(curr, struct conn,
				source.sock.delflush_list);
		int flush = 0;

		spin_lock_bh(&(src_in_o->rcv_lock));

		BUG_ON(src_in_o->sourcetype != SOURCE_SOCK);

		BUG_ON(src_in_o->source.sock.delay_flush == 0);

		if (src_in_o->data_buf.read_remaining != 0) {
			src_in_o->source.sock.delay_flush = 0;
			list_del(&(src_in_o->source.sock.delflush_list));
			flush = 1;
		}

		spin_unlock_bh(&(src_in_o->rcv_lock));

		if (flush) {
			if (restart == 0) {
				restart = 1;
				kref_get(&(sbt->ref));
				spin_unlock_bh(&sock_bufferlimits_lock);
			}
			flush_buf(src_in_o);
		}

		curr = curr->next;
	}

	if (restart)
		kref_put(&(sbt->ref), free_sbt);

	return restart;
}

static void oosbs_global(void)
{
	struct list_head *curr;

	if (0) {
restart:
		spin_lock_bh(&sock_bufferlimits_lock);
	}

	curr = sock_bt_list.prev;
	while (curr != &sock_bt_list) {
		struct sock_buffertracker *sbt = container_of(curr,
				struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(sbt->waiting_conns)) == 0);
		if (oosbs_resumesbt(sbt))
			goto restart;
		curr = curr->prev;
	}

	curr = sock_bt_wait_list.prev;
	while (curr != &sock_bt_wait_list) {
		struct sock_buffertracker *sbt = container_of(curr,
				struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(sbt->waiting_conns)));
		if (oosbs_resumesbt(sbt))
			goto restart;
		curr = curr->prev;
	}
}

static void oosbs_user(void)
{
	struct list_head *curr;

	if (0) {
restart:
		spin_lock_bh(&sock_bufferlimits_lock);
	}

	curr = sock_bt_wait_list.prev;
	while (curr != &sock_bt_wait_list) {
		struct sock_buffertracker *sbt = container_of(curr,
				struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(sbt->waiting_conns)));

		if (sbt->usage < (BUFFERLIMIT_SOCK_USER * 3 / 4))
			break;

		if (oosbs_resumesbt(sbt))
			goto restart;
		curr = curr->prev;
	}
}

static void outofsockbufferspace(struct work_struct *work)
{
	spin_lock_bh(&sock_bufferlimits_lock);
	if (sock_bufferusage < (BUFFERLIMIT_SOCK_GLOBAL * 3 / 4)) {
		oosbs_user();
		if (sock_bufferusage >= (BUFFERLIMIT_SOCK_GLOBAL * 3 / 4))
			goto global;
	} else {
global:
		oosbs_global();
	}
	outofsockbufferspace_scheduled = 0;
	spin_unlock_bh(&sock_bufferlimits_lock);
}

static void _reserve_sock_buffer_inswl(struct conn *src_in_l)
{
	struct sock_buffertracker *sbt = src_in_l->source.sock.sbt;
	struct list_head *curr;

	BUG_ON(sbt == 0);

	if (list_empty(&(sbt->waiting_conns)) == 0)
		goto wlinserted;

	list_del(&(sbt->lh));

	curr = sock_bt_wait_list.next;
	while (curr != &sock_bt_wait_list) {
		struct sock_buffertracker *currsbt = container_of(curr,
				struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(currsbt->waiting_conns)));
		if (sbt->usage < currsbt->usage) {
			list_add(&(sbt->lh), curr);
			goto wlinserted;
		}
		curr = curr->next;
	}

	list_add_tail(&(sbt->lh), &sock_bt_wait_list);

wlinserted:
	curr = sbt->waiting_conns.next;
	while (curr != &(sbt->waiting_conns)) {
		struct conn *currrconn = container_of(curr, struct conn,
				source.sock.alwait_list);
		BUG_ON(currrconn->sourcetype != SOURCE_SOCK);
		if (src_in_l->source.sock.alloclimit <
				currrconn->source.sock.alloclimit) {
			list_add(&(src_in_l->source.sock.alwait_list), curr);
			goto wcinserted;
		}
		curr = curr->next;
	}

	list_add_tail(&(src_in_l->source.sock.alwait_list),
			&(sbt->waiting_conns));

wcinserted:
	src_in_l->source.sock.in_alwait_list = 1;

	if (outofsockbufferspace_scheduled == 0) {
		schedule_work(&outofsockbufferspace_work);
		outofsockbufferspace_scheduled = 1;
	}
}

static void reserve_sock_buffer(struct conn *src_in_l, __u64 amount)
{
	struct sock_buffertracker *sbt = src_in_l->source.sock.sbt;
	struct sock_buffertracker *first_wait_sbt = list_empty(
			&sock_bt_wait_list) ? 0 : container_of(
			sock_bt_wait_list.next, struct sock_buffertracker, lh);

	__u32 max = (1 << 30) - 1;

	BUG_ON(sbt == 0);

	if (unlikely(amount > max))
		amount = max;

	amount += src_in_l->data_buf.totalsize + src_in_l->data_buf.overhead -
			src_in_l->data_buf.cpacket_buffer;

	if (unlikely(amount > max))
		amount = max;

	if (amount > BUFFERLIMIT_SOCK_SOCK)
		amount = BUFFERLIMIT_SOCK_SOCK;

	if (amount <= src_in_l->source.sock.alloclimit)
		return;

	if ((list_empty(&sock_bt_wait_list) == 0 && first_wait_sbt != 0 &&
			first_wait_sbt != sbt &&
			first_wait_sbt->usage <= sbt->usage) ||
			amount - src_in_l->source.sock.alloclimit >
			BUFFERLIMIT_SOCK_USER - sbt->usage ||
			amount - src_in_l->source.sock.alloclimit >
			BUFFERLIMIT_SOCK_GLOBAL - sock_bufferusage) {
		_reserve_sock_buffer_inswl(src_in_l);
	} else {
		int waitingconnremoved = 0;
		sbt->usage += amount - src_in_l->source.sock.alloclimit;
		sock_bufferusage += amount - src_in_l->source.sock.alloclimit;
		src_in_l->source.sock.alloclimit = amount;

		if (src_in_l->source.sock.in_alwait_list){
			list_del(&(src_in_l->source.sock.alwait_list));
			src_in_l->source.sock.in_alwait_list = 0;
			waitingconnremoved = 1;
		}
		_reserve_sock_buffer_reord_bt(sbt, waitingconnremoved);
	}
}

static int _resume_bufferwaiting_socks(struct sock_buffertracker *sbt)
{
	int failed = 0;

	while (list_empty(&(sbt->waiting_conns)) && failed == 0) {
		struct conn *src_in_o = container_of(sbt->waiting_conns.next,
				struct conn, source.sock.alwait_list);
		spin_lock_bh(&(src_in_o->rcv_lock));

		BUG_ON(src_in_o->sourcetype == SOURCE_SOCK);
		BUG_ON(src_in_o->source.sock.in_alwait_list == 0);
		BUG_ON(src_in_o->source.sock.wait_len == 0);

		reserve_sock_buffer(src_in_o, src_in_o->source.sock.wait_len);

		if (src_in_o->source.sock.alloclimit +
				src_in_o->data_buf.cpacket_buffer <=
				src_in_o->data_buf.totalsize +
				src_in_o->data_buf.overhead) {
			failed = 1;
			goto out;
		}

		wake_up_interruptible(&(src_in_o->source.sock.wait));

out:
		spin_unlock_bh(&(src_in_o->rcv_lock));
	}

	return failed;
}

static void resume_bufferwaiting_socks(void)
{
	struct list_head *curr = sock_bt_wait_list.next;

	while (curr != &sock_bt_wait_list) {
		struct sock_buffertracker *currsbt = container_of(curr,
				struct sock_buffertracker, lh);
		BUG_ON(list_empty(&(currsbt->waiting_conns)));
		curr = curr->next;

		if (_resume_bufferwaiting_socks(currsbt))
			return;
	}
}

static void reorder_sock_bt_wait_list(struct sock_buffertracker *sbt)
{
	if (list_empty(&(sbt->waiting_conns)))
		return;

	while (sbt->lh.prev != &sock_bt_wait_list) {
		struct sock_buffertracker *prevsbt = container_of(sbt->lh.prev,
				struct sock_buffertracker, lh);

		BUG_ON(sbt->lh.next == &sock_bt_list);

		if (prevsbt->usage <= sbt->usage)
			break;

		list_del(&(sbt->lh));
		list_add_tail(&(sbt->lh), &(prevsbt->lh));
	}
}

void connreset_sbt(struct conn *cn)
{
	struct sock_buffertracker *sbt;

	spin_lock_bh(&sock_bufferlimits_lock);
	spin_lock_bh(&(cn->rcv_lock));

	if (cn->sourcetype != SOURCE_SOCK)
		goto out;

	sbt = cn->source.sock.sbt;
	BUG_ON(sbt == 0);

	if (cn->source.sock.in_alwait_list) {
		list_del(&(cn->source.sock.alwait_list));
		cn->source.sock.in_alwait_list = 0;

		if (list_empty(&(sbt->waiting_conns))) {
			list_del(&(sbt->lh));
			list_add_tail(&(sbt->lh), &sock_bt_list);
		}

		reorder_sock_bt_wait_list(sbt);
	}

	sbt->usage -= cn->source.sock.alloclimit;
	if (cn->source.sock.delay_flush) {
		cn->source.sock.delay_flush = 0;
		list_del(&(cn->source.sock.delflush_list));
	}
	kref_put(&(sbt->ref), free_sbt);
	cn->source.sock.sbt = 0;

out:
	spin_unlock_bh(&(cn->rcv_lock));
	spin_unlock_bh(&sock_bufferlimits_lock);
}

void unreserve_sock_buffer(struct conn *cn)
{
	int freed = 0;
	struct sock_buffertracker *sbt;

	spin_lock_bh(&sock_bufferlimits_lock);
	spin_lock_bh(&(cn->rcv_lock));

	if (cn->sourcetype != SOURCE_SOCK)
		goto out;

	if (unlikely(cn->isreset != 0))
		goto out;

	sbt = cn->source.sock.sbt;
	BUG_ON(sbt == 0);

	if (cn->data_buf.totalsize + cn->data_buf.overhead <=
			cn->source.sock.alloclimit +
			cn->data_buf.cpacket_buffer)
		goto out;

	freed = 1;

	BUG_ON(cn->source.sock.alloclimit > sbt->usage);
	BUG_ON(cn->source.sock.alloclimit > sock_bufferusage);
	BUG_ON(cn->data_buf.cpacket_buffer > cn->data_buf.totalsize +
			cn->data_buf.overhead);

	sbt->usage -= cn->source.sock.alloclimit;
	sbt->usage += cn->data_buf.totalsize;
	sbt->usage += cn->data_buf.overhead;
	sbt->usage -= cn->data_buf.cpacket_buffer;

	sock_bufferusage -= cn->source.sock.alloclimit;
	sock_bufferusage += cn->data_buf.totalsize;
	sock_bufferusage += cn->data_buf.overhead;
	sock_bufferusage -= cn->data_buf.cpacket_buffer;

	cn->source.sock.alloclimit = cn->data_buf.totalsize +
			cn->data_buf.overhead - cn->data_buf.cpacket_buffer;

	if (cn->source.sock.alloclimit == 0 &&
			cn->source.sock.in_alwait_list) {
		list_del(&(cn->source.sock.alwait_list));
		cn->source.sock.in_alwait_list = 0;

		if (list_empty(&(sbt->waiting_conns))) {
			list_del(&(sbt->lh));
			list_add_tail(&(sbt->lh), &sock_bt_list);
		}
	}

	reorder_sock_bt_wait_list(sbt);

out:
	spin_unlock_bh(&(cn->rcv_lock));

	if (freed)
		resume_bufferwaiting_socks();

	spin_unlock_bh(&sock_bufferlimits_lock);
}


int cor_socket_release(struct socket *sock)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	if (cs->type == SOCKTYPE_UNCONNECTED) {
	} else if (cs->type == SOCKTYPE_LISTENER) {
		close_port(cs);
	} else if (cs->type == SOCKTYPE_CONN) {
		reset_conn(cs->data.conn.src_sock);
		kref_put(&(cs->data.conn.src_sock->ref), free_conn);
		kref_put(&(cs->data.conn.trgt_sock->ref), free_conn);

		if (cs->data.conn.rcvitem != 0) {
			databuf_item_free(cs->data.conn.rcvitem);
			cs->data.conn.rcvitem = 0;
		}
	} else {
		BUG();
	}

	kmem_cache_free(sock_slab, cs);

	return 0;
}

int cor_socket_bind(struct socket *sock, struct sockaddr *myaddr,
		int sockaddr_len)
{
	int rc = 0;
	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	struct cor_sockaddr *addr = (struct cor_sockaddr *) myaddr;

	if (unlikely(sockaddr_len < sizeof(struct cor_sockaddr)))
		return -EINVAL;

	if (unlikely(addr->type != SOCKADDRTYPE_PORT))
		return -EINVAL;

	spin_lock_bh(&(cs->lock));

	if (unlikely(cs->type != SOCKTYPE_UNCONNECTED)) {
		rc = -EINVAL;
		goto out;
	}

	rc = open_port(cs, addr->addr.port);

out:
	spin_unlock_bh(&(cs->lock));

	return rc;
}

int cor_socket_connect(struct socket *sock, struct sockaddr *vaddr,
		int sockaddr_len, int flags)
{
	struct sock_buffertracker *sbt;

	struct conn *src_sock;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	src_sock = alloc_conn(GFP_KERNEL);

	if (unlikely(src_sock == 0))
		return -ENOMEM;

	src_sock->is_client = 1;

	spin_lock_bh(&sock_bufferlimits_lock);
	sbt = get_sock_buffertracker(current_uid());
	spin_unlock_bh(&sock_bufferlimits_lock);

	if (unlikely(sbt == 0)) {
		reset_conn(src_sock);
		return -ENOMEM;
	}

	spin_lock_bh(&(src_sock->rcv_lock));
	spin_lock_bh(&(src_sock->reversedir->rcv_lock));
	spin_lock_bh(&(cs->lock));
	if (cs->type != SOCKTYPE_UNCONNECTED) {
		spin_unlock_bh(&(cs->lock));
		spin_unlock_bh(&(src_sock->reversedir->rcv_lock));
		spin_unlock_bh(&(src_sock->rcv_lock));
		reset_conn(src_sock);
		kref_put(&(sbt->ref), free_sbt);
		return -EISCONN;
	}

	conn_init_sock_source(src_sock);
	src_sock->source.sock.sbt = sbt;
	conn_init_sock_target(src_sock->reversedir);

	memset(&(cs->data), 0, sizeof(cs->data));
	cs->type = SOCKTYPE_CONN;
	cs->data.conn.src_sock = src_sock;
	cs->data.conn.trgt_sock = src_sock->reversedir;
	mutex_init(&(cs->data.conn.rcvbuf_lock));
	kref_get(&(src_sock->ref));
	kref_get(&(src_sock->reversedir->ref));

	spin_unlock_bh(&(cs->lock));
	spin_unlock_bh(&(src_sock->reversedir->rcv_lock));
	spin_unlock_bh(&(src_sock->rcv_lock));

	sock->state = SS_CONNECTED;

	return 0;
}

static int cor_rdytoaccept(struct cor_sock *cs)
{
	int rc;
	spin_lock_bh(&(cs->lock));
	BUG_ON(cs->type != SOCKTYPE_LISTENER);
	rc = (list_empty(&(cs->data.listener.conn_queue)) == 0);
	spin_unlock_bh(&(cs->lock));
	return rc;
}

const struct proto_ops cor_proto_ops;

int cor_socket_accept(struct socket *sock, struct socket *newsock, int flags)
{
	struct sock_buffertracker *sbt;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	struct conn *src_sock_o;

	struct cor_sock *newcs;


	newcs = kmem_cache_alloc(sock_slab, GFP_KERNEL);
	if (unlikely(newcs == 0))
		return -ENOMEM;
	memset(newcs, 0, sizeof(struct cor_sock));
	newcs->type = SOCKTYPE_CONN;
	spin_lock_init(&(newcs->lock));

	spin_lock_bh(&sock_bufferlimits_lock);
	sbt = get_sock_buffertracker(current_uid());
	spin_unlock_bh(&sock_bufferlimits_lock);

	if (unlikely(sbt == 0)) {
		kmem_cache_free(sock_slab, newcs);
		return -ENOMEM;
	}

restart:
	spin_lock_bh(&(cs->lock));

	BUG_ON(cs->type != SOCKTYPE_UNCONNECTED &&
			cs->type != SOCKTYPE_LISTENER &&
			cs->type != SOCKTYPE_CONN);

	if (unlikely(cs->type != SOCKTYPE_LISTENER)) {
		spin_unlock_bh(&(cs->lock));
		kref_put(&(sbt->ref), free_sbt);
		kmem_cache_free(sock_slab, newcs);
		return -EINVAL;
	}

	if (unlikely(cs->data.listener.queue_maxlen <= 0)) {
		spin_unlock_bh(&(cs->lock));
		kref_put(&(sbt->ref), free_sbt);
		kmem_cache_free(sock_slab, newcs);
		return -EINVAL;
	}

	while (list_empty(&(cs->data.listener.conn_queue))) {
		spin_unlock_bh(&(cs->lock));
		if (wait_event_interruptible(cs->data.listener.wait,
				cor_rdytoaccept(cs))) {
			kref_put(&(sbt->ref), free_sbt);
			kmem_cache_free(sock_slab, newcs);
			return -ERESTARTSYS;
		}
		spin_lock_bh(&(cs->lock));
	}

	src_sock_o = container_of(cs->data.listener.conn_queue.next,
			struct conn, source.sock.cl_list);

	BUG_ON(src_sock_o->sourcetype != SOURCE_SOCK);

	list_del(cs->data.listener.conn_queue.next);

	cs->data.listener.queue_len--;

	spin_unlock_bh(&(cs->lock));

	spin_lock_bh(&(src_sock_o->rcv_lock));
	if (unlikely(src_sock_o->isreset != 0)) {
		spin_unlock_bh(&(src_sock_o->rcv_lock));
		kref_put(&(src_sock_o->ref), free_conn);
		goto restart;
	}
	src_sock_o->source.sock.sbt = sbt;
	spin_unlock_bh(&(src_sock_o->rcv_lock));

	newcs->data.conn.src_sock = src_sock_o;
	newcs->data.conn.trgt_sock = src_sock_o->reversedir;
	kref_get(&(src_sock_o->reversedir->ref));

	newsock->ops = &cor_proto_ops;
	newsock->sk = (struct sock *) newcs;
	newsock->state = SS_CONNECTED;

	return 0;
}

int cor_socket_listen(struct socket *sock, int len)
{
	struct cor_sock *cs = (struct cor_sock *) sock->sk;

	spin_lock_bh(&(cs->lock));

	BUG_ON(cs->type != SOCKTYPE_UNCONNECTED &&
			cs->type != SOCKTYPE_LISTENER &&
			cs->type != SOCKTYPE_CONN);

	if (unlikely(cs->type != SOCKTYPE_LISTENER)) {
		spin_unlock_bh(&(cs->lock));
		return -EOPNOTSUPP;
	}

	cs->data.listener.queue_maxlen = len;

	spin_unlock_bh(&(cs->lock));

	return 0;
}

int cor_socket_shutdown(struct socket *sock, int flags)
{
	return -ENOTSUPP;
}

int cor_ioctl(struct socket *sock, unsigned int cmd, unsigned long arg)
{
	return -ENOIOCTLCMD;
}

static int sendmsg_maypush(struct cor_sock *cs, struct conn *src_sock)
{
	int ret = 0;

	spin_lock_bh(&sock_bufferlimits_lock);
	spin_lock_bh(&(src_sock->rcv_lock));
	spin_lock_bh(&(cs->lock));

	if (unlikely(unlikely(src_sock != cs->data.conn.src_sock))) {
		ret = 1;
	} else if (unlikely(src_sock->isreset != 0)) {
		ret = 1;
	} else if (src_sock->source.sock.wait_len == 0) {
		ret = 1;
	} else if (src_sock->source.sock.alloclimit +
			src_sock->data_buf.cpacket_buffer >
			src_sock->data_buf.totalsize +
			src_sock->data_buf.overhead) {
		ret = 1;
	} else {
		reserve_sock_buffer(src_sock,
				src_sock->source.sock.wait_len);
		if (src_sock->source.sock.alloclimit +
				src_sock->data_buf.cpacket_buffer >
				src_sock->data_buf.totalsize +
				src_sock->data_buf.overhead)
			ret = 1;
	}

	spin_unlock_bh(&(cs->lock));
	spin_unlock_bh(&(src_sock->rcv_lock));
	spin_unlock_bh(&sock_bufferlimits_lock);

	return ret;
}

__s32 ___cor_sendmsg(char *buf, __u32 bufread, __u32 buflen,
		__u32 totalremaining, struct cor_sock *cs,
		struct conn *src_sock, struct conn *trgt_sock)
{
	__s32 rc = 0;
	__s64 bufferfree;

	spin_lock_bh(&sock_bufferlimits_lock);
	spin_lock_bh(&(src_sock->rcv_lock));
	spin_lock_bh(&(cs->lock));

	if (unlikely(unlikely(src_sock != cs->data.conn.src_sock) ||
			unlikely(trgt_sock != cs->data.conn.trgt_sock) ||
			unlikely(src_sock->isreset != 0))) {
		spin_unlock_bh(&(cs->lock));
		spin_unlock_bh(&(src_sock->rcv_lock));
		spin_unlock_bh(&sock_bufferlimits_lock);
		return -EPIPE;
	}

	spin_unlock_bh(&(cs->lock));

	reserve_sock_buffer(src_sock, (__u32) (buflen +
			sizeof(struct data_buf_item)));

	bufferfree = (__s64) src_sock->source.sock.alloclimit +
			(__s64) src_sock->data_buf.cpacket_buffer -
			(__s64) src_sock->data_buf.totalsize -
			(__s64) src_sock->data_buf.overhead;

	spin_unlock_bh(&sock_bufferlimits_lock);

	if (bufferfree < (buflen + sizeof(struct data_buf_item))) {
		kfree(buf);
		rc = -EAGAIN;
		printk(KERN_ERR "2");
		goto out;
	}

	rc = receive_buf(src_sock, buf, bufread, buflen, 0);

out:

	if (rc == -EAGAIN)
		src_sock->source.sock.wait_len = totalremaining +
				sizeof(struct data_buf_item);
	else
		src_sock->source.sock.wait_len = 0;

	spin_unlock_bh(&(src_sock->rcv_lock));

	return rc;
}

__s32 __cor_sendmsg(struct msghdr *msg, __u32 totallen, int *iovidx,
		int *iovread, struct cor_sock *cs, struct conn *src_sock,
		struct conn *trgt_sock)
{
	char *buf = 0;
	__u32 bufread = 0;
	__u32 buflen = buf_optlen(totallen);

	buf = kmalloc(buflen, GFP_KERNEL);
	if (unlikely(buf == 0))
		return -ENOMEM;

	memset(buf, 0, buflen);

	while (bufread < buflen && bufread < totallen) {
		struct iovec *iov = msg->msg_iov + *iovidx;
		__user char *userbuf = iov->iov_base + *iovread;
		__u32 len = iov->iov_len - *iovread;
		int notcopied;

		if (len == 0) {
			(*iovidx)++;
			(*iovread) = 0;
			BUG_ON(*iovidx >= msg->msg_iovlen);
			continue;
		}

		if (len > (buflen - bufread))
			len = buflen - bufread;
		if (len > (totallen - bufread))
			len = totallen - bufread;

		notcopied = copy_from_user(buf + bufread, userbuf, len);

		bufread += len - notcopied;
		(*iovread) += len - notcopied;

		if (unlikely(notcopied == buflen) && bufread == 0) {
			kfree(buf);
			return -EFAULT;
		}

		if (unlikely(notcopied > 0))
			break;
	}

	return ___cor_sendmsg(buf, bufread, buflen, totallen, cs, src_sock,
			trgt_sock);
}

__s32 _cor_sendmsg(struct msghdr *msg, size_t total_len, struct cor_sock *cs,
		struct conn *src_sock, struct conn *trgt_sock)
{
	int flush = (msg->msg_flags & MSG_MORE) == 0;

	__s32 copied = 0;
	__s32 rc = 0;
	__u64 max = (1LL << 31) - 1;
	__u32 totallen = (total_len > max ? max : total_len);

	int iovidx = 0;
	int iovread = 0;

	while (rc >= 0 && copied < totallen) {
		rc = __cor_sendmsg(msg, totallen - copied, &iovidx, &iovread,
				cs, src_sock, trgt_sock);

		if (rc > 0 || copied == 0)
			copied += rc;
	}

	unreserve_sock_buffer(src_sock);

	spin_lock_bh(&sock_bufferlimits_lock);
	spin_lock_bh(&(src_sock->rcv_lock));

	if (unlikely(src_sock->isreset != 0)) {
		spin_unlock_bh(&(src_sock->rcv_lock));
		spin_unlock_bh(&sock_bufferlimits_lock);
		return -EPIPE;
	}

	if (flush == 0 && copied > 0 && copied == total_len &&
			src_sock->data_buf.totalsize +
			src_sock->data_buf.overhead -
			src_sock->data_buf.cpacket_buffer <
			(BUFFERLIMIT_SOCK_SOCK*3)/4) {
		if (src_sock->source.sock.delay_flush == 0) {
			struct sock_buffertracker *sbt =
					src_sock->source.sock.sbt;
			BUG_ON(sbt == 0);
			list_add_tail(&(src_sock->source.sock.delflush_list),
					&(sbt->delflush_conns));
		}
		src_sock->source.sock.delay_flush = 1;
	} else {
		if (src_sock->source.sock.delay_flush) {
			list_del(&(src_sock->source.sock.delflush_list));
		}
		src_sock->source.sock.delay_flush = 0;
	}

	spin_unlock_bh(&(src_sock->rcv_lock));
	spin_unlock_bh(&sock_bufferlimits_lock);

	return copied;
}

int cor_sendmsg(struct kiocb *iocb, struct socket *sock, struct msghdr *msg,
		size_t total_len)
{
	__s32 rc = 0;

	int blocking = (msg->msg_flags & MSG_DONTWAIT) == 0;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	struct conn *src_sock;
	struct conn *trgt_sock;

	spin_lock_bh(&(cs->lock));

	BUG_ON(cs->type != SOCKTYPE_UNCONNECTED &&
			cs->type != SOCKTYPE_LISTENER &&
			cs->type != SOCKTYPE_CONN);

	if (unlikely(cs->type != SOCKTYPE_CONN)) {
		spin_unlock_bh(&(cs->lock));
		return -EBADF;
	}

	src_sock = cs->data.conn.src_sock;
	trgt_sock = cs->data.conn.trgt_sock;

	kref_get(&(src_sock->ref));
	kref_get(&(trgt_sock->ref));

	spin_unlock_bh(&(cs->lock));

	BUG_ON(src_sock == 0);
	BUG_ON(trgt_sock == 0);

send:
	rc = _cor_sendmsg(msg, total_len, cs, src_sock, trgt_sock);

	if (likely(rc > 0 || rc == -EAGAIN))
		flush_buf(src_sock);

	if (rc == -EAGAIN && blocking) {
		#warning todo move waitqueue to cor_sock
		if (wait_event_interruptible(src_sock->source.sock.wait,
				sendmsg_maypush(cs, src_sock)) == 0)
			goto send;
		rc = -ERESTARTSYS;
	}

	BUG_ON(rc > total_len);
	return rc;
}

static int cor_readytoread(struct conn *trgt_sock_o)
{
	int rc = 0;
	spin_lock_bh(&(trgt_sock_o->rcv_lock));
	rc = (trgt_sock_o->data_buf.read_remaining != 0) ||
			unlikely(trgt_sock_o->isreset != 0);
	spin_unlock_bh(&(trgt_sock_o->rcv_lock));
	return rc;
}

static int __cor_recvmsg(struct msghdr *msg, __u32 totallen,
		int *iovidx, int *iovwritten,
		struct cor_sock *cs, struct conn *trgt_sock)
{
	struct data_buf_item *dbi = cs->data.conn.rcvitem;
	int written = 0;

	while (written < totallen && dbi != 0) {
		struct iovec *iov = msg->msg_iov + *iovidx;
		__user char *userbuf = iov->iov_base + *iovwritten;
		__u32 len = iov->iov_len - *iovwritten;
		int notcopied;

		if (len == 0) {
			(*iovidx)++;
			(*iovwritten) = 0;
			BUG_ON(*iovidx >= msg->msg_iovlen);
			continue;
		}

		if (dbi->datalen == cs->data.conn.rcvoffset) {
			databuf_item_free(cs->data.conn.rcvitem);
			cs->data.conn.rcvitem = 0;
			cs->data.conn.rcvoffset = 0;
			break;
		}

		if (len > (dbi->datalen - cs->data.conn.rcvoffset))
			len = dbi->datalen - cs->data.conn.rcvoffset;
		if (len > (totallen - written))
			len = totallen - written;

		notcopied = copy_to_user(userbuf, dbi->buf +
				cs->data.conn.rcvoffset, len);

		written += (len - notcopied);
		(*iovwritten) += (len - notcopied);
		cs->data.conn.rcvoffset += (len - notcopied);

		if (unlikely(notcopied == len) && written == 0)
			return -EFAULT;

		if (notcopied > 0)
			break;
	}

	if (written == 0)
		return -EAGAIN;
	return written;
}

static int _cor_recvmsg(struct msghdr *msg, size_t total_len,
		struct cor_sock *cs, struct conn *trgt_sock)
{
	int copied = 0;
	int rc = 0;

	__u64 max = (1LL << 31) - 1;
	__u32 totallen = (total_len > max ? max : total_len);

	int iovidx = 0;
	int iovwritten = 0;

	mutex_lock(&(cs->data.conn.rcvbuf_lock));

	while (rc >= 0 && copied < totallen) {
		spin_lock_bh(&(trgt_sock->rcv_lock));
		spin_lock_bh(&(cs->lock));
		if (unlikely(unlikely(trgt_sock != cs->data.conn.trgt_sock)||
				unlikely(trgt_sock->isreset != 0))) {
			spin_unlock_bh(&(cs->lock));
			spin_unlock_bh(&(trgt_sock->rcv_lock));
			mutex_unlock(&(cs->data.conn.rcvbuf_lock));
			return -EPIPE;
		}

		spin_unlock_bh(&(cs->lock));

		if (cs->data.conn.rcvitem == 0)
			databuf_pull_dbi(cs, trgt_sock);

		spin_unlock_bh(&(trgt_sock->rcv_lock));

		rc = __cor_recvmsg(msg, totallen - copied, &iovidx, &iovwritten,
				cs, trgt_sock);

		if (rc > 0 || copied == 0)
			copied += rc;
	}

	mutex_unlock(&(cs->data.conn.rcvbuf_lock));

	return copied;
}

int cor_recvmsg(struct kiocb *iocb, struct socket *sock, struct msghdr *msg,
		size_t total_len, int flags)
{
	size_t copied = 0;
	int blocking = (flags & MSG_DONTWAIT) == 0;

	struct cor_sock *cs = (struct cor_sock *) sock->sk;
	struct conn *src_sock;
	struct conn *trgt_sock;

	spin_lock_bh(&(cs->lock));

	BUG_ON(cs->type != SOCKTYPE_UNCONNECTED &&
			cs->type != SOCKTYPE_LISTENER &&
			cs->type != SOCKTYPE_CONN);

	if (unlikely(cs->type != SOCKTYPE_CONN)) {
		spin_unlock_bh(&(cs->lock));
		return -EBADF;
	}

	src_sock = cs->data.conn.src_sock;
	trgt_sock = cs->data.conn.trgt_sock;

	BUG_ON(src_sock == 0);
	BUG_ON(trgt_sock == 0);

	kref_get(&(src_sock->ref));
	kref_get(&(trgt_sock->ref));

	spin_unlock_bh(&(cs->lock));

recv:
	copied = _cor_recvmsg(msg, total_len, cs, trgt_sock);

	if (likely(copied > 0)) {
		refresh_conn_credits(trgt_sock, 0, 0);
		wake_sender(trgt_sock);
	}

	if (copied == -EAGAIN && blocking) {
		if (wait_event_interruptible(trgt_sock->target.sock.wait,
				cor_readytoread(trgt_sock)) == 0)
			goto recv;
		copied = -ERESTARTSYS;
	}

	kref_put(&(src_sock->ref), free_conn);
	kref_put(&(trgt_sock->ref), free_conn);

	return copied;
}

const struct proto_ops cor_proto_ops = {
		.family = PF_COR,
		.owner = THIS_MODULE,
		.release = cor_socket_release,
		.bind = cor_socket_bind,
		.connect = cor_socket_connect,
		.accept = cor_socket_accept,
		.listen = cor_socket_listen,
		.shutdown = cor_socket_shutdown,
		.ioctl = cor_ioctl,
		.sendmsg = cor_sendmsg,
		.recvmsg = cor_recvmsg

	/*socketpair
	getname
	poll
	compat_ioctl
	setsockopt
	getsockopt
	compat_setsockopt
	compat_getsockopt
	mmap
	sendpage
	splice_read*/
};

int cor_createsock(struct net *net, struct socket *sock, int protocol)
{
	struct cor_sock *cs;

	if (unlikely(protocol != 0))
		return -EPROTONOSUPPORT;

	cs = kmem_cache_alloc(sock_slab, GFP_KERNEL);
	if (unlikely(cs == 0))
		return -ENOMEM;
	memset(cs, 0, sizeof(struct cor_sock));

	cs->type = SOCKTYPE_UNCONNECTED;
	spin_lock_init(&(cs->lock));

	sock->state = SS_UNCONNECTED;
	sock->ops = &cor_proto_ops;
	sock->sk = (struct sock *) cs;

	return 0;
}

static struct net_proto_family cor_net_proto_family = {
	.family = PF_COR,
	.create = cor_createsock,
	.owner = THIS_MODULE
};

static int __init cor_sock_init(void)
{
	sock_slab = kmem_cache_create("cor_sock",
			sizeof(struct cor_sock), 8, 0, 0);

	INIT_WORK(&outofsockbufferspace_work, outofsockbufferspace);
	outofsockbufferspace_scheduled = 0;

	sock_register(&cor_net_proto_family);
	sock_bufferusage = 0;
	return 0;
}

module_init(cor_sock_init);

MODULE_LICENSE("GPL");

